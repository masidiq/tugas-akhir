<?php
$app->get('/dosen/jadwal_mengajar', function ($request, $response) {
    // Set Fakultas
    if(!setFakultasFromHeader($request)){
        return error400($response,"Header fakultas harus diikutsertakan");
    }
    try {

        $user = getUserByToken($request);
        if (!$user) {
            return errorAuth($response);
        }
        if (!is_dosen($user)) {
            return errorAuth($response, "Akses ditolak"); 
        }
        $userId = $user['id'];

        // get jadwal mengajar
        $query = "SELECT * FROM jadwal_mengajar where id_dosen = ".$userId ;
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        // get kehadiran tiap jadwal
        foreach ($result as $row){
            $q2 = "SELECT tanggal, status, mulai, selesai FROM kehadiran_dosen where id_jadwal = ". $row->id ." ORDER BY tanggal DESC";
            $stmt2 = getDB()->query($q2); 
            $result2 = $stmt2->fetchAll(PDO::FETCH_OBJ);
            $row->kehadiran = $result2;
        }
        return ok($response,$result);
    } catch(PDOException $e) {
       return error400($response, $e->$e->getMessage());
    }
});

$app->get('/dosen/hasil_kuisioner', function ($request, $response) {
    // Set Fakultas
    if(!setFakultasFromHeader($request)){
        return error400($response,"Header fakultas harus diikutsertakan");
    }

    try {

        $user = getUserByToken($request);
        if (!$user) {
            return errorAuth($response);
        }
        if (!is_dosen($user)) {
            return errorAuth($response, "Akses ditolak"); 
        }
        $userId = $user['id'];

        $query = "SELECT * FROM hasil_kuisioner where id_dosen = ".$userId;
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($result as $row){
            $q2 = "SELECT pertanyaan, nilai FROM hasil_kuisioner_detail where id_hasil_kuisioner = ". $row->id;
            $stmt2 = getDB()->query($q2); 
            $result2 = $stmt2->fetchAll(PDO::FETCH_OBJ);
            $row->list_pertanyaan = $result2;
        }
        return ok($response,$result);
    } catch(PDOException $e) {
       return error400($response, $e->$e->getMessage());
    }
});

$app->get('/dosen/status_perwalian', function ($request, $response) {
    // Set Fakultas
    if(!setFakultasFromHeader($request)){
        return error400($response,"Header fakultas harus diikutsertakan");
    }

    try {
        $user = getUserByToken($request);
        if (!$user) {
            return errorAuth($response);
        }
        if (!is_dosen($user)) {
            return errorAuth($response, "Akses ditolak"); 
        }
        $userId = $user['id'];

        $query = "SELECT * FROM status_perwalian WHERE id_dosen = " .$userId ." ORDER BY nama_mhs";
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return ok($response,$result);
    } catch(PDOException $e) {
        return error400($response, $e->$e->getMessage());
    }
});


function is_dosen($user){
    if($user['type_user'] == "DOSEN")
        return true;
    else
        return false;
}
?>