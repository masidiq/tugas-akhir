<?php
$app->get('/mahasiswa/jadwal_kuliah', function ($request, $response) {

    // Set Fakultas
    if(!setFakultasFromHeader($request)){
       return error400($response,"Header fakultas harus diikutsertakan");
    }

    try {
        $user = getUserByToken($request);
        if (!$user) {
            return errorAuth($response);
        }
    
        // jika type ORTU, get info user mahasiswa nya
        $typeUserOrigin = $user['type_user'];
        if ($typeUserOrigin == "ORTU") {
            $usernameMhs = convert_ortu_to_mhs($user['username']);
            $query = "SELECT id FROM user where username = '".$usernameMhs ."' LIMIT 1";    
            $stmt = getDB()->query($query); 
            $user = $stmt->fetch();

            // jika user tidak ditemukan
            if(!$user){
                return error401($response, "Mahasiswa dengan nrp ". $usernameMhs ." tidak terdaftar");
            }
        }

        $userId = $user['id'];
        // get jadwal kul
        $query = "SELECT * FROM jadwal_kuliah where id_mhs = ".$userId;
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        // get kehadiran tiap jadwal
        foreach ($result as $row){
            $q2 = "SELECT tanggal, status, mulai, selesai FROM kehadiran_mahasiswa where id_jadwal = ". $row->id  ." ORDER BY tanggal DESC";
            $stmt2 = getDB()->query($q2); 
            $result2 = $stmt2->fetchAll(PDO::FETCH_OBJ);
            $row->kehadiran = $result2;
        }

        return ok($response,$result);
    } catch(PDOException $e) {
        return error400($response, $e->$e->getMessage());
    }
});

$app->get('/mahasiswa/jadwal_ujian', function ($request, $response) {
    // Set Fakultas
    if(!setFakultasFromHeader($request)){
        return error400($response,"Header fakultas harus diikutsertakan");
    }

    try {
        $user = getUserByToken($request);
        if (!$user) {
            return errorAuth($response);
        }

        // jika type ORTU, get info user mahasiswa nya
        $typeUserOrigin = $user['type_user'];
        if ($typeUserOrigin == "ORTU") {
            $usernameMhs = convert_ortu_to_mhs($user['username']);
            $query = "SELECT id FROM user where username = '".$usernameMhs ."' LIMIT 1";    
            $stmt = getDB()->query($query); 
            $user = $stmt->fetch();

            // jika user tidak ditemukan
            if(!$user){
                return error401($response, "Mahasiswa dengan nrp ". $usernameMhs ." tidak terdaftar");
            }
        }
        $userId = $user['id'];

        $query = "SELECT * FROM jadwal_ujian where id_mhs = ".$userId;
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return ok($response,$result);
    } catch(PDOException $e) {
        return error400($response, $e->$e->getMessage());
    }
});

$app->get('/mahasiswa/nilai', function ($request, $response) {
    // Set Fakultas
    if(!setFakultasFromHeader($request)){
        return error400($response,"Header fakultas harus diikutsertakan");
    }
    try {
        // ambil user berdasarkan user
        $user = getUserByToken($request);
        if (!$user) {
            errorAuth($response);
            return;
        }

        // jika type ORTU, get info user mahasiswa nya
        $typeUserOrigin = $user['type_user'];
        if ($typeUserOrigin == "ORTU") {
            $usernameMhs = convert_ortu_to_mhs($user['username']);
            $query = "SELECT id FROM user where username = '".$usernameMhs ."' LIMIT 1";    
            $stmt = getDB()->query($query); 
            $user = $stmt->fetch();

            // jika user tidak ditemukan
            if(!$user){
               return error401($response, "Mahasiswa dengan nrp ". $usernameMhs ." tidak terdaftar");
            }
        }
        $userId = $user['id'];

        $query = "SELECT * FROM nilai where id_mhs = ".$userId ." ORDER BY semester DESC, mata_kuliah ASC";
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        return ok($response,$result);
    } catch(PDOException $e) {
        return error400($response, $e->$e->getMessage());
    }
});



?>