<?php
function passwordVerify($password, $hashDb) {
    // md5 -> md5 -> ambil 10 char pertama
    $password = md5($password);
    $password = md5($password);
    $password = substr($password, 0, 10);
    if ($password == $hashDb) {
        return true;
    }else {
        return false;
    }
}

function generateNewToken() {
    $newToken = null;
    $i = 0;
    $conn = getDB();
    do {
        $i++;
        $newToken = random_string(50);
        $query = "SELECT * FROM user where token = '".$newToken ."' LIMIT 1";
        $stmt = $conn->query($query); 
        $user = $stmt->fetch();
        if ($i == 100) {
            $conn = null;
            die("iterasi new token sudah 100x");
        }
    } while ($user);
    $conn = null;
    return $newToken;
}

function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

function updateUserToken($userId, $newToken) {
    try {
        $conn = getDB();
        $updateSql = "UPDATE user
                     SET token = :token
                     WHERE id = :user_id";
        $updateStmt = $conn->prepare($updateSql);
        $updateStmt->bindValue(":token", $newToken);
        $updateStmt->bindValue(":user_id", $userId);
        $count = $updateStmt->execute();
        $count = null;
        $conn = null;
    }catch(PDOException $e) {
        return error401($response,$e->getMessage());
    }
}

function getUserByToken($request) {
    $token = $request->getHeaderLine('token');
    if (is_null($token)) {
        return false;
    }
    $conn = getDB();
    $query = "SELECT * FROM user where token = '".$token ."' LIMIT 1";
    $stmt = $conn->query($query); 
    $user = $stmt->fetch();
    $conn = null;
    return $user;
}

function setFakultasFromHeader($request) {
    $fakultas =$request->getHeaderLine('fakultas');
    if (is_null($fakultas)) {
        return false;
    }
    setDB($fakultas);
    return $fakultas;
}

function errorAuth($response, $message = "Token tidak valid"){
    $res = new stdClass();
    $res->errorMessage = $message;
    $newResponse = $response->withJson($res);
    $newResponse = $newResponse->withStatus(401);
    return $newResponse;
}

// fungsi bantuan
function convert_ortu_to_mhs($ortuId){
        $first = substr($ortuId, 0, 5); 
        $last = substr($ortuId, -3); 
        return $first."0".$last;
}

function error400($response, $message){
    $res = new stdClass();
    $res->errorMessage = $message;
    $newResponse = $response->withJson($res);
    $newResponse = $newResponse->withStatus(400);
    return $newResponse;
}

function error401($response, $message){
    $res = new stdClass();
    $res->errorMessage = $message;
    $newResponse = $response->withJson($res);
    $newResponse = $newResponse->withStatus(401);
    return $newResponse;
}

function ok($response, $result){
    $newResponse = $response->withJson($result);
    return $newResponse;
}

$app->get('/login', function ($request, $response, $args) {

    // Set Fakultas
    if(!setFakultasFromHeader($request))
        return error400($response,"Header fakultas harus diikutsertakan");

    // get username dan password dari req header
    $username = strtolower($request->getHeaderLine('username'));
    $password = $request->getHeaderLine('password');


    
     try {
        $query = "SELECT * FROM user where username = '".$username ."' LIMIT 1";
        $stmt = getDB()->query($query); 
        $user = $stmt->fetch();

        // jika user tidak ditemukan
        if(!$user)
            return error401($response,"User tidak ditemukan");

        // Bandingkan password
        if (!passwordVerify($password,$user['password'])) {
            return error401($response,"Password salah");
        }
        
        // generate new token
        $newToken = generateNewToken();

        // update token ke table user
        updateUserToken($user['id'],$newToken);
        
        $namaOrtu = null;

        // jika type ORTU, get info user mahasiswa nya
        $typeUserOrigin = $user['type_user'];
        if ($typeUserOrigin == "ORTU") {
            $usernameMhs = convert_ortu_to_mhs($user['username']);
            $namaOrtu = $user['nama'];

            $query = "SELECT * FROM user where username = '".$usernameMhs ."' LIMIT 1";    
            $stmt = getDB()->query($query); 
            $user = $stmt->fetch();

            // jika user tidak ditemukan
            if(!$user){
                return error401($response, "Mahasiswa dengan nrp ". $usernameMhs ." tidak terdaftar");
            }
        }
        

        // buat objek untuk dikembalikan ke client
        $res['npm'] = $user['npm'];
        $res['type_user'] = $user['type_user'];
        $res['nama'] = $user['nama'];
        $res['url_foto'] = $user['url_foto'];
        $res['token'] = $newToken;


        // get semester jika user type mahasiswa      
        if ($user['type_user'] == "MAHASISWA") {
            $q2 = "SELECT sem.id, sem.nama_semester, sem.status 
                        FROM mahasiswa_semester  as ms
                        INNER JOIN semester as sem ON sem.id = ms.id_semester
                        WHERE id_mhs = ".$user['id'];
            $stmt2 = getDB()->query($q2); 
            $listSemester = $stmt2->fetchAll(PDO::FETCH_OBJ);
            $res['list_semester'] = $listSemester;
            $res['type_user'] = $typeUserOrigin;
            $res['nama_ortu'] = $namaOrtu;
       }
       return ok($response,$res);
    } catch(PDOException $e) {
        return error400($response,$e->getMessage());
    }
});

$app->get('/logout', function ($request, $response, $args) {
	// Set Fakultas
    if(!setFakultasFromHeader($request))
        return error400($response,"header fakultas harus diikutsertakan");
    
    $user = getUserByToken($request);
    if (!$user) {
        return errorAuth($response);
    }
    updateUserToken($user['id'],null);
    $res['message'] = 'Logout berhasil';
    return ok($response,$res);
});

$app->get('/version', function ($request,$response) {
	global $version;
    $res['version'] = $version;
    return ok($response,$res);
});

?>