<?php
	$dbhost="127.0.0.1";
	$dbuser="root";
	$dbpass="";
	$dbname="ft";
	$version = "1.0.1";

	function getDB() {
		global $dbhost;
		global $dbuser;
		global $dbpass;
		global $dbname;
		
		$dbConnection = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass); 
		$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $dbConnection;
	}


	function setDB($fakultas){
		global $dbname;
		if ($fakultas == "baa") {
			$dbname = "baa";
		}else if ($fakultas == "fh"){
			$dbname = "hukum";
		}else if ($fakultas == "fisip"){
			$dbname = "fisip";
		}
		// Baru database Fakultas Teknik saja yang tersedia
		else if ($fakultas == "ft"){
			$dbname = "ft";
		}else if ($fakultas == "fe"){
			$dbname = "ekonomi";
		}else if ($fakultas == "fkip"){
			$dbname = "fkip";
		}else if ($fakultas == "fiss"){
			$dbname = "fiss";
		}else if ($fakultas == "fps"){
			$dbname = "pascasarjana";
		}else {
			$dbname = "NOTSET";
		}
	}
?>