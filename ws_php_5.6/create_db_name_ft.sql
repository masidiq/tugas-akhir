-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2017 at 03:02 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ft`
--

-- --------------------------------------------------------

--
-- Table structure for table `hasil_kuisioner`
--

CREATE TABLE IF NOT EXISTS `hasil_kuisioner` (
  `id` int(11) NOT NULL,
  `mata_kuliah` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `prodi` varchar(255) DEFAULT NULL,
  `semester` varchar(255) DEFAULT NULL,
  `sks` varchar(255) DEFAULT NULL,
  `peserta` varchar(255) DEFAULT NULL,
  `responden` varchar(255) DEFAULT NULL,
  `nilai` varchar(255) DEFAULT NULL,
  `rata_rata` varchar(255) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil_kuisioner`
--

INSERT INTO `hasil_kuisioner` (`id`, `mata_kuliah`, `kelas`, `prodi`, `semester`, `sks`, `peserta`, `responden`, `nilai`, `rata_rata`, `id_dosen`) VALUES
(1, 'SISTEM BASISDATA I', 'A', '302', '20152', '3', '50', '49', '460', '3.13', 3),
(2, 'SISTEM BASISDATA II', 'A', '302', '20152', '3', '46', '45', '367', '3.07', 3),
(3, 'PEMROGRAMAN BASISDATA', 'A', '302', '20152', '3', '45', '41', '445', '2.99', 3),
(4, 'PEMODELAN PROSES BISNIS', 'A', '302', '20152', '3', '52', '50', '364', '2.98', 3),
(5, 'PERAWATAN & PENGUJIAN PERANGKAT LUNAK', 'A', '302', '20152', '3', '48', '44', '456', '3.15', 4),
(6, 'KONSTRUKSI P/L BERORIENTASI OBJEK', 'A', '302', '20152', '3', '47', '44', '440', '3.13', 4);

-- --------------------------------------------------------

--
-- Table structure for table `hasil_kuisioner_detail`
--

CREATE TABLE IF NOT EXISTS `hasil_kuisioner_detail` (
  `id` int(11) NOT NULL,
  `id_hasil_kuisioner` int(11) NOT NULL,
  `pertanyaan` varchar(500) NOT NULL,
  `nilai` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil_kuisioner_detail`
--

INSERT INTO `hasil_kuisioner_detail` (`id`, `id_hasil_kuisioner`, `pertanyaan`, `nilai`) VALUES
(1, 1, 'Aapakah Benar', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_kuliah`
--

CREATE TABLE IF NOT EXISTS `jadwal_kuliah` (
  `id` int(11) NOT NULL,
  `id_semester` int(11) DEFAULT NULL,
  `id_mhs` varchar(255) NOT NULL,
  `hari` varchar(255) DEFAULT NULL,
  `jam` varchar(255) DEFAULT NULL,
  `kode_mk` varchar(255) DEFAULT NULL,
  `mata_kuliah` varchar(255) DEFAULT NULL,
  `ruang` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `sks` varchar(255) DEFAULT NULL,
  `dosen` varchar(255) DEFAULT NULL,
  `presensi` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_kuliah`
--

INSERT INTO `jadwal_kuliah` (`id`, `id_semester`, `id_mhs`, `hari`, `jam`, `kode_mk`, `mata_kuliah`, `ruang`, `kelas`, `sks`, `dosen`, `presensi`) VALUES
(1, 20152, '1', 'Senin', '07:00-09:30', 'IT507', 'REKAYASA WEB', 'SB405', 'A', '3', 'FERRY MULYANTO, ST., M.KOM,', '5/8'),
(2, 20152, '1', 'Senin', '10:00-12:30', 'IT506', 'INTERAKSI MANUSIA & KOMPUTER', 'SB401', 'A', '3', 'WANDA GUSDYA, ST., MT., ST, MT.', '5/8'),
(3, 20152, '1', 'Senin', '13:00-15:30', 'IT742', 'SISTEM INFORMASI GEOGRAFIS', 'SB405', 'A', '3', 'SALI ALAS MAJAPAHIT, S.ST., M.KOM.,', '5/8'),
(4, 20152, '1', 'Selasa', '10:00-12:30', 'IT744', 'MANAJEMEN ASET DIGITAL', 'SB403', 'A', '3', 'MELLIA LIYANTHY, ST., MT.,', '5/8'),
(5, 20152, '1', 'Rabu', '13:00-14:40', 'IT302', 'ILMU BUDAYA SUNDA', 'SB301', 'C', '2', 'RIO TEGUH ARIF, S.Pd., M.Pd,', '5/8'),
(6, 20152, '1', 'Kamis', '10:00-12:30', 'IT501', 'SISTEM MANAJEMEN BASIS DATA', 'SB404', 'A', '3', 'CACA EMILE SUPRIANA, S.SI., MT.,', '5/8'),
(7, 20152, '1', 'Jumat', '07:00-09:30', 'IT734', 'PROYEK APLIKASI MULTIMEDIA', 'P1401', 'A', '3', 'MELLIA LIYANTHY, ST., MT.,', '5/8'),
(8, 20152, '1', 'Jumat', '13:00-15:30', 'IT105', 'MATEMATIKA DASAR', 'SB405', 'A', '3', 'DRS. ANWAR GHOZIE,', '5/8'),
(13, 20152, '2', 'Senin', '07:00-09:30', 'IT507', 'REKAYASA WEB', 'SB405', 'A', '3', 'FERRY MULYANTO, ST., M.KOM,', '5/8'),
(14, 20152, '2', 'Senin', '10:00-12:30', 'IT506', 'INTERAKSI MANUSIA & KOMPUTER', 'SB401', 'A', '3', 'WANDA GUSDYA, ST., MT., ST, MT.', '5/8');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_mengajar`
--

CREATE TABLE IF NOT EXISTS `jadwal_mengajar` (
  `id` int(11) NOT NULL,
  `id_dosen` varchar(255) NOT NULL,
  `mata_kuliah` varchar(255) DEFAULT NULL,
  `hari` varchar(255) DEFAULT NULL,
  `jam` varchar(255) DEFAULT NULL,
  `ruang` varchar(255) DEFAULT NULL,
  `kode_mk` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `sks` varchar(255) DEFAULT NULL,
  `presensi` varchar(255) DEFAULT NULL,
  `id_semester` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `jadwal_mengajar`
--

INSERT INTO `jadwal_mengajar` (`id`, `id_dosen`, `mata_kuliah`, `hari`, `jam`, `ruang`, `kode_mk`, `kelas`, `sks`, `presensi`, `id_semester`) VALUES
(1, '3', 'SISTEM BASISDATA I', 'Senin', '13:00-15:30', 'SB119', 'IT206', 'D', '3', '6', 20152),
(2, '3', 'PEMROGRAMAN BASISDATA', 'Selasa', '13:00-15:30', 'SB119', 'IT602', 'A', '3', '5', 20152),
(3, '4', 'KONSTRUKSI P/L BERORIENTASI OBJEK', 'Senin', '10:40-12:20', 'SB403', 'IT405', 'A', '3', '6', 20152),
(4, '4', 'PERAWATAN & PENGUJIAN PERANGKAT LUNAK', 'Senin', '13:00-14:40', 'SB405', 'IT852', 'A', '2', '6', 20152),
(5, '4', 'KONSTRUKSI P/L BERORIENTASI OBJEK', 'Senin', '14:50-16:30', 'SB403', 'IT405', 'A', '3', '6', 20152),
(6, '4', 'KONSTRUKSI P/L BERORIENTASI OBJEK', 'Selasa', '07:00-10:20', 'SB403', 'IT405', 'B', '3', '5', 20152);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_ujian`
--

CREATE TABLE IF NOT EXISTS `jadwal_ujian` (
  `id` int(11) NOT NULL,
  `id_mhs` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jam` varchar(255) DEFAULT NULL,
  `mata_kuliah` varchar(255) DEFAULT NULL,
  `ruang` varchar(255) DEFAULT NULL,
  `kode_mk` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `sks` varchar(255) DEFAULT NULL,
  `dosen` varchar(255) DEFAULT NULL,
  `presensi` varchar(255) DEFAULT NULL,
  `id_semester` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_ujian`
--

INSERT INTO `jadwal_ujian` (`id`, `id_mhs`, `tanggal`, `jam`, `mata_kuliah`, `ruang`, `kode_mk`, `kelas`, `sks`, `dosen`, `presensi`, `id_semester`) VALUES
(1, 1, '2016-04-04', '07:15-09:00', 'REKAYASA WEB', 'SB405', 'IT507', 'A', '3', 'FERRY MULYANTO, ST., M.KOM,', '5/8', 20152),
(2, 1, '2016-04-04', '09:15-10:45', 'INTERAKSI MANUSIA & KOMPUTER', 'SB401', 'IT506', 'A', '3', 'WANDA GUSDYA, ST., MT., ST, MT.', '5/8', 20152),
(3, 1, '2016-04-05', '07:15-09:00', 'SISTEM INFORMASI GEOGRAFIS', 'SB405', 'IT742', 'A', '3', 'SALI ALAS MAJAPAHIT, S.ST., M.KOM.,', '5/8', 20152),
(4, 1, '2016-04-05', '09:15-10:45', 'MANAJEMEN ASET DIGITAL', 'SB403', 'IT744', 'A', '3', 'MELLIA LIYANTHY, ST., MT.,', '5/8', 20152),
(5, 1, '2016-04-06', '07:15-09:00', 'ILMU BUDAYA SUNDA', 'SB301', 'IT302', 'C', '2', 'RIO TEGUH ARIF, S.Pd., M.Pd,', '5/8', 20152),
(6, 1, '2016-04-06', '09:15-10:45', 'SISTEM MANAJEMEN BASIS DATA', 'SB404', 'IT501', 'A', '3', 'CACA EMILE SUPRIANA, S.SI., MT.,', '5/8', 20152),
(7, 1, '2016-04-07', '07:15-09:00', 'PROYEK APLIKASI MULTIMEDIA', 'P1401', 'IT734', 'A', '3', 'MELLIA LIYANTHY, ST., MT.,', '5/8', 20152),
(8, 1, '2016-04-07', '09:15-10:45', 'MATEMATIKA DASAR', 'SB405', 'IT105', 'A', '3', 'DRS. ANWAR GHOZIE,', '5/8', 20152),
(11, 2, '2016-04-04', '07:15-09:00', 'SISTEM INFORMASI GEOGRAFIS', 'SB405', 'IT507', 'A', '3', 'FERRY MULYANTO, ST., M.KOM,', '5/8', 20152),
(12, 2, '2016-04-04', '09:15-10:45', 'MANAJEMEN ASET DIGITAL', 'SB401', 'IT506', 'A', '3', 'WANDA GUSDYA, ST., MT., ST, MT.', '5/8', 20152);

-- --------------------------------------------------------

--
-- Table structure for table `kehadiran_dosen`
--

CREATE TABLE IF NOT EXISTS `kehadiran_dosen` (
  `id` int(11) NOT NULL,
  `id_jadwal` int(11) DEFAULT NULL,
  `tanggal` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `mulai` varchar(255) DEFAULT NULL,
  `selesai` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kehadiran_dosen`
--

INSERT INTO `kehadiran_dosen` (`id`, `id_jadwal`, `tanggal`, `status`, `mulai`, `selesai`) VALUES
(1, 1, '2016-2-8', 'Hadir', '13:00', '15:30'),
(2, 1, '2016-2-15', 'Hadir', '13:00', '15:30'),
(3, 1, '2016-2-22', 'Hadir', '13:00', '15:30'),
(4, 1, '2016-2-29', 'Hadir', '13:00', '15:30'),
(5, 1, '2016-3-7', 'Hadir', '13:00', '15:30'),
(6, 1, '2016-3-14', 'Hadir', '13:00', '15:30'),
(7, 2, '2016-2-9', 'Hadir', '13:00', '15:30'),
(8, 2, '2016-2-16', 'Hadir', '13:00', '15:30'),
(9, 2, '2016-2-23', 'Hadir', '13:00', '15:30'),
(10, 2, '2016-3-1', 'Hadir', '13:00', '15:30'),
(11, 2, '2016-3-8', 'Hadir', '13:00', '15:30'),
(24, 3, '2016-2-8', 'Hadir', '10:40', '12:20'),
(25, 3, '2016-2-15', 'Hadir', '10:40', '12:20'),
(26, 3, '2016-2-22', 'Hadir', '10:40', '12:20'),
(27, 3, '2016-2-29', 'Hadir', '10:40', '12:20'),
(28, 3, '2016-3-7', 'Hadir', '10:40', '12:20'),
(29, 3, '2016-3-14', 'Hadir', '10:40', '12:20'),
(30, 4, '2016-2-8', 'Hadir', '13:00', '14:40'),
(31, 4, '2016-2-15', 'Hadir', '13:00', '14:40'),
(32, 4, '2016-2-22', 'Hadir', '13:00', '14:40'),
(33, 4, '2016-2-29', 'Hadir', '13:00', '14:40'),
(34, 4, '2016-3-7', 'Hadir', '13:00', '14:40'),
(35, 4, '2016-3-14', 'Hadir', '13:00', '14:40'),
(36, 5, '2016-2-8', 'Hadir', '14:50', '16:30'),
(37, 5, '2016-2-15', 'Hadir', '14:50', '16:30'),
(38, 5, '2016-2-22', 'Hadir', '14:50', '16:30'),
(39, 5, '2016-2-29', 'Hadir', '14:50', '16:30'),
(40, 5, '2016-3-7', 'Hadir', '14:50', '16:30'),
(41, 5, '2016-3-14', 'Hadir', '14:50', '16:30'),
(42, 6, '2016-2-9', 'Hadir', '07:00', '10:20'),
(43, 6, '2016-2-16', 'Hadir', '07:00', '10:20'),
(44, 6, '2016-2-23', 'Hadir', '07:00', '10:20'),
(45, 6, '2016-3-1', 'Hadir', '07:00', '10:20'),
(46, 6, '2016-3-8', 'Hadir', '07:00', '10:20');

-- --------------------------------------------------------

--
-- Table structure for table `kehadiran_mahasiswa`
--

CREATE TABLE IF NOT EXISTS `kehadiran_mahasiswa` (
  `id` int(255) NOT NULL,
  `id_jadwal` int(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `mulai` varchar(255) DEFAULT NULL,
  `selesai` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kehadiran_mahasiswa`
--

INSERT INTO `kehadiran_mahasiswa` (`id`, `id_jadwal`, `tanggal`, `status`, `mulai`, `selesai`) VALUES
(8, 1, '2015-10-01', 'Hadir', '07:00', '09:30'),
(9, 1, '2015-10-08', '-', '07:00', '09:30'),
(10, 1, '2015-10-15', 'Hadir', '07:00', '09:30'),
(11, 1, '2015-10-22', 'Hadir', '07:00', '09:30'),
(12, 1, '2015-10-29', '-', '07:00', '09:30'),
(13, 2, '2015-10-01', 'Hadir', '10:00', '12:30'),
(14, 2, '2015-10-08', '-', '10:00', '12:30'),
(15, 2, '2015-10-15', 'Hadir', '10:00', '12:30'),
(16, 2, '2015-10-22', 'Hadir', '10:00', '12:30'),
(17, 2, '2015-10-29', '-', '10:00', '12:30'),
(18, 3, '2015-10-01', 'Hadir', '13:00', '15:30'),
(19, 3, '2015-10-08', '-', '13:00', '15:30'),
(20, 3, '2015-10-15', 'Hadir', '13:00', '15:30'),
(21, 3, '2015-10-22', 'Hadir', '13:00', '15:30'),
(22, 3, '2015-10-29', '-', '13:00', '15:30'),
(23, 4, '2015-10-02', 'Hadir', '10:00', '12:30'),
(24, 4, '2015-10-09', '-', '10:00', '12:30'),
(25, 4, '2015-10-16', 'Hadir', '10:00', '12:30'),
(26, 4, '2015-10-23', 'Hadir', '10:00', '12:30'),
(27, 4, '2015-11-03', '-', '10:00', '12:30'),
(28, 4, '2015-11-10', '-', '10:00', '12:30'),
(29, 4, '2015-11-17', '-', '10:00', '12:30'),
(30, 4, '2015-11-24', '-', '10:00', '12:30'),
(31, 5, '2015-10-02', 'Hadir', '13:00', '14:40'),
(32, 5, '2015-10-09', '-', '13:00', '14:40'),
(33, 5, '2015-10-16', 'Hadir', '13:00', '14:40'),
(34, 5, '2015-10-23', 'Hadir', '13:00', '14:40'),
(35, 5, '2015-11-03', 'Hadir', '13:00', '14:40'),
(36, 5, '2015-11-10', 'Hadir', '13:00', '14:40'),
(37, 5, '2015-11-17', 'Hadir', '13:00', '14:40'),
(38, 5, '2015-11-24', 'Hadir', '13:00', '14:40'),
(39, 6, '2015-10-02', 'Hadir', '13:00', '14:40'),
(40, 6, '2015-10-09', '-', '13:00', '14:40'),
(41, 6, '2015-10-16', 'Hadir', '13:00', '14:40'),
(42, 6, '2015-10-23', 'Hadir', '13:00', '14:40'),
(43, 6, '2015-11-03', 'Hadir', '13:00', '14:40'),
(44, 6, '2015-11-10', 'Hadir', '13:00', '14:40'),
(45, 6, '2015-11-17', 'Hadir', '13:00', '14:40'),
(46, 6, '2015-11-24', 'Hadir', '13:00', '14:40'),
(47, 7, '2015-10-02', 'Hadir', '07:00', '09:30'),
(48, 7, '2015-10-09', '-', '07:00', '09:30'),
(49, 7, '2015-10-16', 'Hadir', '07:00', '09:30'),
(50, 7, '2015-10-23', 'Hadir', '07:00', '09:30'),
(51, 7, '2015-11-03', 'Hadir', '07:00', '09:30'),
(52, 7, '2015-11-10', 'Hadir', '07:00', '09:30'),
(53, 7, '2015-11-17', 'Hadir', '07:00', '09:30'),
(54, 7, '2015-11-24', 'Hadir', '07:00', '09:30'),
(55, 8, '2015-10-02', 'Hadir', '13:00', '15:30'),
(56, 8, '2015-10-09', '-', '13:00', '15:30'),
(57, 8, '2015-10-16', 'Hadir', '13:00', '15:30'),
(58, 8, '2015-10-23', 'Hadir', '13:00', '15:30'),
(59, 8, '2015-11-03', 'Hadir', '13:00', '15:30'),
(60, 8, '2015-11-10', 'Hadir', '13:00', '15:30'),
(61, 8, '2015-11-17', 'Hadir', '13:00', '15:30'),
(62, 8, '2015-11-24', 'Hadir', '13:00', '15:30'),
(63, 9, '2015-10-02', 'Hadir', '10:00', '12:30'),
(64, 9, '2015-10-09', '-', '10:00', '12:30'),
(65, 9, '2015-10-16', 'Hadir', '10:00', '12:30'),
(66, 9, '2015-10-23', 'Hadir', '10:00', '12:30'),
(67, 9, '2015-11-03', 'Hadir', '10:00', '12:30'),
(68, 9, '2015-11-10', 'Hadir', '10:00', '12:30'),
(69, 9, '2015-11-17', 'Hadir', '10:00', '12:30'),
(70, 9, '2015-11-24', 'Hadir', '10:00', '12:30'),
(71, 10, '2015-10-02', 'Hadir', '13:00', '15:30'),
(72, 10, '2015-10-09', '-', '13:00', '15:30'),
(73, 10, '2015-10-16', 'Hadir', '13:00', '15:30'),
(74, 10, '2015-10-23', 'Hadir', '13:00', '15:30'),
(75, 10, '2015-11-03', 'Hadir', '13:00', '15:30'),
(76, 10, '2015-11-10', 'Hadir', '13:00', '15:30'),
(77, 10, '2015-11-17', 'Hadir', '13:00', '15:30'),
(78, 10, '2015-11-24', 'Hadir', '13:00', '15:30'),
(87, 11, '2015-10-16', 'Hadir', '13:00', '15:30'),
(88, 11, '2015-10-25', '-', '13:00', '15:30'),
(89, 11, '2015-11-01', 'Hadir', '13:00', '15:30'),
(90, 11, '2015-11-08', 'Hadir', '13:00', '15:30'),
(91, 12, '2015-10-16', 'Hadir', '13:00', '15:30'),
(92, 12, '2015-10-25', '-', '13:00', '15:30'),
(93, 12, '2015-11-01', 'Hadir', '13:00', '15:30'),
(94, 12, '2015-11-08', 'Hadir', '13:00', '15:30'),
(95, 13, '2015-10-16', 'Hadir', '13:00', '15:30'),
(96, 13, '2015-10-25', '-', '13:00', '15:30'),
(97, 13, '2015-11-01', 'Hadir', '13:00', '15:30'),
(98, 13, '2015-11-08', 'Hadir', '13:00', '15:30'),
(99, 14, '2015-10-16', 'Hadir', '13:00', '15:30'),
(100, 14, '2015-10-25', '-', '13:00', '15:30'),
(101, 14, '2015-11-01', 'Hadir', '13:00', '15:30'),
(102, 14, '2015-11-08', 'Hadir', '13:00', '15:30');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa_semester`
--

CREATE TABLE IF NOT EXISTS `mahasiswa_semester` (
  `id_mhs` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa_semester`
--

INSERT INTO `mahasiswa_semester` (`id_mhs`, `id_semester`) VALUES
(1, 20151),
(2, 20151),
(1, 20152),
(2, 20152);

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `id` int(11) NOT NULL,
  `id_mhs` int(11) DEFAULT NULL,
  `mata_kuliah` varchar(255) DEFAULT NULL,
  `semester` varchar(255) DEFAULT NULL,
  `kode_mk` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `sks` int(11) DEFAULT NULL,
  `tugas1` float DEFAULT NULL,
  `tugas2` float DEFAULT NULL,
  `tugas3` float DEFAULT NULL,
  `tugas4` float DEFAULT NULL,
  `tugas5` float DEFAULT NULL,
  `presensi` float DEFAULT NULL,
  `uts` float DEFAULT NULL,
  `uas` float DEFAULT NULL,
  `akhir` float DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `bobot` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id`, `id_mhs`, `mata_kuliah`, `semester`, `kode_mk`, `kelas`, `sks`, `tugas1`, `tugas2`, `tugas3`, `tugas4`, `tugas5`, `presensi`, `uts`, `uas`, `akhir`, `grade`, `bobot`) VALUES
(1, 1, 'REKAYASA WEB', '20152', 'IT507', 'A', 2, 80, 80, 70, 80, 80, 100, 80, 80, 80, 'A', '4'),
(2, 1, 'INTERAKSI MANUSIA & KOMPUTER', '20152', 'IT506', 'A', 3, 70, 80, 80, 80, 90, 100, 60, 70, 65, 'B', '1'),
(3, 1, 'SISTEM INFORMASI GEOGRAFIS', '20152', 'IT742', 'A', 3, 0, 0, 0, 0, 0, 100, 65, 65, 65, 'B', '3'),
(4, 1, 'MANAJEMEN ASET DIGITAL', '20152', 'IT744', 'A', 3, 0, 0, 0, 0, 0, 100, 90, 80, 85, 'A', '4'),
(5, 1, 'ILMU BUDAYA SUNDA', '20152', 'IT302', 'A', 3, 0, 0, 0, 0, 0, 100, 90, 80, 85, 'A', '4'),
(6, 1, 'SISTEM MANAJEMEN BASIS DATA', '20152', 'IT501', 'A', 3, 0, 0, 0, 0, 0, 100, 90, 80, 85, 'A', '4'),
(7, 1, 'PROYEK APLIKASI MULTIMEDIA', '20152', 'IT734', 'A', 3, 0, 0, 0, 0, 0, 100, 90, 80, 85, 'A', '1'),
(8, 1, 'MATEMATIKA DASAR', '20152', 'IT105', 'A', 2, 0, 0, 0, 0, 0, 100, 50, 70, 65, 'C', '2'),
(9, 1, 'PENGANTAR INFORMATIKA', '20151', 'IT101', 'A', 3, 0, 0, 0, 0, 0, 100, 70, 80, 70, 'B', '3'),
(10, 1, 'MATEMATIKA LOGIKA', '20151', 'IT103', 'A', 3, 0, 0, 0, 0, 0, 100, 90, 80, 85, 'A', '4'),
(11, 2, 'REKAYASA WEB', '20152', 'IT742', 'A', 3, 0, 0, 0, 0, 0, 100, 90, 80, 85, 'B', '3'),
(12, 2, 'INTERAKSI MANUSIA & KOMPUTER', '20152', 'IT744', 'A', 3, 0, 0, 0, 0, 0, 100, 90, 80, 85, 'A', '4'),
(13, 2, 'SISTEM INFORMASI GEOGRAFIS', '20151', 'IT302', 'A', 3, 0, 0, 0, 0, 0, 100, 90, 80, 85, 'A', '4'),
(14, 2, 'MANAJEMEN ASET DIGITAL', '20151', 'IT501', 'A', 3, 0, 0, 0, 0, 0, 100, 90, 80, 85, 'B', '3');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE IF NOT EXISTS `semester` (
  `id` int(11) NOT NULL,
  `nama_semester` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id`, `nama_semester`, `status`) VALUES
(20131, 'Semester Gasal 2013-2014', 0),
(20132, 'Semester Genap 2013-2014', 0),
(20141, 'Semester Gasal 2014-2015', 0),
(20142, 'Semester Genap 2014-2015', 0),
(20151, 'Semester Gasal 2015-2016', 0),
(20152, 'Semester Genap 2015-2016', 1);

-- --------------------------------------------------------

--
-- Table structure for table `status_perwalian`
--

CREATE TABLE IF NOT EXISTS `status_perwalian` (
  `id` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `nama_mhs` varchar(255) DEFAULT NULL,
  `program` varchar(255) DEFAULT NULL,
  `angkatan` varchar(255) DEFAULT NULL,
  `total_sks` int(11) DEFAULT NULL,
  `ipk` float DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tgl_perwalian` date DEFAULT NULL,
  `jml_mk` int(11) DEFAULT NULL,
  `jml_sks` int(11) DEFAULT NULL,
  `sks_tunda` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_perwalian`
--

INSERT INTO `status_perwalian` (`id`, `id_dosen`, `nama_mhs`, `program`, `angkatan`, `total_sks`, `ipk`, `status`, `tgl_perwalian`, `jml_mk`, `jml_sks`, `sks_tunda`) VALUES
(3, 3, 'ADINDA DESFIANINGRUM', '302', '2013', 86, 3.47, 'Aktif', '2016-02-01', 6, 20, 0),
(4, 3, 'SRI NURLELA', '302', '2013', 89, 3.23, 'Aktif', '2016-02-01', 5, 19, 0),
(5, 3, 'YAFIANTY SITI VESSIA', '302', '2013', 78, 3.12, 'Aktif', '2016-02-01', 7, 22, 0),
(6, 3, 'KARTIKA PERMATASARI', '302', '2013', 68, 3.18, 'Aktif', '2016-02-01', 7, 22, 0),
(7, 3, 'ARFA REZA MATAHARI', '302', '2013', 73, 2.98, 'Aktif', '2016-02-01', 6, 20, 0),
(8, 3, 'EKA ARDILAH FEBRIYANTI', '302', '2013', 75, 3.89, 'Aktif', '2016-02-01', 5, 21, 0),
(9, 3, 'PRAMHESWARI KRISDHI UTAMI PUTRI', '302', '2013', 88, 3.67, 'Aktif', '2016-02-01', 7, 20, 0),
(10, 3, 'NANDA AULIA NUGRAHA', '302', '2013', 90, 3.45, 'Aktif', '2016-02-01', 6, 19, 0),
(11, 3, 'NADYNE KUSUMAH', '302', '2013', 86, 3.01, 'Aktif', '2016-02-01', 6, 22, 0),
(12, 3, 'LARAS SHINTA MEGA DEWI SAPUTRA', '302', '2013', 86, 2.89, 'Aktif', '2016-02-01', 5, 22, 0),
(13, 3, 'RADEN ALLYA SITI ZAHRAH WAHJOE', '302', '2013', 89, 2.98, 'Aktif', '2016-02-01', 7, 20, 0),
(14, 3, 'AZHAR HANA RIFAI', '302', '2013', 78, 3.47, 'Aktif', '2016-02-01', 7, 21, 0),
(15, 3, 'HALIMATUN SA ADAH', '302', '2013', 68, 3.23, 'Aktif', '2016-02-01', 6, 21, 0),
(16, 3, 'FITRI RAFI AH ALAWIYAH', '302', '2013', 73, 3.12, 'Aktif', '2016-02-01', 5, 20, 0),
(17, 3, 'NAJMI MUNA', '302', '2013', 75, 3.18, 'Aktif', '2016-02-01', 7, 19, 0),
(18, 3, 'DENAYA CANDRA SAFIRA', '302', '2013', 88, 2.98, 'Aktif', '2016-02-01', 6, 22, 0),
(19, 3, 'NENI YULIANTIKA', '302', '2013', 90, 3.89, 'Aktif', '2016-02-01', 6, 22, 0),
(20, 3, 'ANNYSA EKA YUSILAWATI', '302', '2013', 86, 3.67, 'Aktif', '2016-02-01', 5, 20, 0),
(21, 3, 'SYAUQI RAHMAT SUGARA', '302', '2013', 89, 3.45, 'Aktif', '2016-02-01', 7, 21, 0),
(22, 3, 'MEILIZA CAHYA HAPSARI', '302', '2013', 78, 3.01, 'Aktif', '2016-02-01', 7, 20, 0),
(23, 3, 'BALEBAT BUANA PUSPA', '302', '2013', 68, 2.89, 'Aktif', '2016-02-01', 6, 19, 0),
(24, 3, 'VIKI CANDRA TAHIR AHMAD', '302', '2013', 73, 2.98, 'Aktif', '2016-02-01', 5, 22, 0),
(25, 3, 'JATIPUJI ANDAWURI', '302', '2013', 75, 3.47, 'Aktif', '2016-02-01', 7, 22, 0),
(26, 3, 'AYU ALDILA SALMA', '302', '2013', 88, 3.67, 'Aktif', '2016-02-01', 7, 20, 0),
(27, 3, 'RIZAL MAULANA GHAFFAR', '302', '2013', 90, 3.45, 'Aktif', '2016-02-01', 6, 21, 0),
(28, 0, 'SRI RAHAYU', '302', '2013', 86, 3.01, 'Aktif', '2016-02-01', 5, 20, 0),
(29, 0, 'SYIFA NUZULIA RAHMI', '302', '2013', 86, 2.89, 'Aktif', '2016-02-01', 6, 19, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type_user` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `url_foto` varchar(255) DEFAULT NULL,
  `npm` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `type_user`, `nama`, `url_foto`, `npm`, `token`) VALUES
(1, '133040336', 'd9b1d7db4c', 'MAHASISWA', 'MUHAMMAD AKBAR SIDIQ', 'https://akademik.unpas.ac.id/ft/gambar/avatars/133040336.jpg', '133040336', 't6m0am5afl0j9fitg3t94cwo3dhreyo3ghcat5zqt7jtazgviq'),
(2, '133040335', 'd9b1d7db4c', 'MAHASISWA', 'BIBEN NURBANI HASAN', 'https://akademik.unpas.ac.id/ft/gambar/avatars/133040335.jpg', '133040335', 'e7rzq3b2xrpyiog1c4shf81tj1445gg7va4ckuw6zhz3wm9xxn'),
(3, 'if101', 'd9b1d7db4c', 'DOSEN', 'SANDRA ISLAMA PUTRA, S.SI.,', NULL, '24015142', 'metxmh4v36hx1zoph9d7jl14v5cyhl7j16sw5msr3s5clh36mc'),
(4, 'if102', 'd9b1d7db4c', 'DOSEN', 'DR. AYI PURBASARI, ST., MT.,', '', '21456521', 'fo93w2z1ptsz81yb9adet1j7gdrdb5dq992l6xgihl6vd19y6t'),
(5, '133041336', 'd9b1d7db4c', 'ORTU', 'KAMSI', NULL, NULL, '7wj9j0ppmvk5vdc6ity8awzt87g874057s9va2socn5d9dcayy'),
(6, '133041337', 'd9b1d7db4c', 'ORTU', '', '', '', 'ixxe6p62t4ntaoqs3gzjraqoqley8ci4l6ir522d9n8txqafff');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hasil_kuisioner`
--
ALTER TABLE `hasil_kuisioner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_kuisioner_detail`
--
ALTER TABLE `hasil_kuisioner_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_kuliah`
--
ALTER TABLE `jadwal_kuliah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_mengajar`
--
ALTER TABLE `jadwal_mengajar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_ujian`
--
ALTER TABLE `jadwal_ujian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kehadiran_dosen`
--
ALTER TABLE `kehadiran_dosen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kehadiran_mahasiswa`
--
ALTER TABLE `kehadiran_mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa_semester`
--
ALTER TABLE `mahasiswa_semester`
  ADD PRIMARY KEY (`id_semester`,`id_mhs`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_perwalian`
--
ALTER TABLE `status_perwalian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hasil_kuisioner`
--
ALTER TABLE `hasil_kuisioner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `hasil_kuisioner_detail`
--
ALTER TABLE `hasil_kuisioner_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jadwal_kuliah`
--
ALTER TABLE `jadwal_kuliah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `jadwal_mengajar`
--
ALTER TABLE `jadwal_mengajar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `jadwal_ujian`
--
ALTER TABLE `jadwal_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `kehadiran_dosen`
--
ALTER TABLE `kehadiran_dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `kehadiran_mahasiswa`
--
ALTER TABLE `kehadiran_mahasiswa`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `status_perwalian`
--
ALTER TABLE `status_perwalian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
