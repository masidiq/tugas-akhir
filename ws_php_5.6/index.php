<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '/vendor/autoload.php';
require 'db.php';

$app = new \Slim\App;
$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");
	$data = array('name' => 'Bob', 'age' => 40);
	$newResponse = $response->withJson($data);
    return $newResponse;
});

$app->get('/', function ($req, $res, $args) {
    global $dbhost;
    global $dbuser;
    global $dbpass;
    global $dbname;
    mysql_connect($dbhost, $dbuser, $dbpass) or die ("Koneksi DB Error");
    mysql_select_db ($dbname) or die ("Database \"".$dbname ."\" Tidak dapat diakses");
    return "Server is Up";
});

require_once 'services/login.php';
require_once 'services/mahasiswa.php';
require_once 'services/dosen.php';

$app->run();