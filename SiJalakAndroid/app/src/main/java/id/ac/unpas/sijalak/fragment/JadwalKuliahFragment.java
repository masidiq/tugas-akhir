package id.ac.unpas.sijalak.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.ac.unpas.sijalak.MainActivity;
import id.ac.unpas.sijalak.MainData;
import id.ac.unpas.sijalak.R;
import id.ac.unpas.sijalak.customAdapter.JadwalKuliahAdapter;
import id.ac.unpas.sijalak.detailActivity.DetailJadwalKuliahActivity;
import id.ac.unpas.sijalak.helper.EnumHari;
import id.ac.unpas.sijalak.model.JadwalKuliah;
import id.ac.unpas.sijalak.scheduler.AlarmReceiver;
import id.ac.unpas.sijalak.service.JadwalKuliahService;
import id.ac.unpas.sijalak.utils.AlertHelper;
import id.ac.unpas.sijalak.utils.OnTaskListener;
import id.ac.unpas.sijalak.utils.RequestTask;
import id.ac.unpas.sijalak.utils.ResponseData;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link JadwalKuliahFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link JadwalKuliahFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JadwalKuliahFragment extends Fragment implements OnTaskListener {
    private MainActivity activity;
    private StickyListHeadersListView mList;
    private JadwalKuliahAdapter mAdapter;
    List<JadwalKuliah> jdwl;
    private TextView txtLastUpdate;

    private OnFragmentInteractionListener mListener;



    public static JadwalKuliahFragment newInstance() {
        JadwalKuliahFragment fragment = new JadwalKuliahFragment();
        return fragment;
    }

    public JadwalKuliahFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jadwal_kuliah, container, false);
        // Init
        initUI(view);
        return view;
    }

    private void initUI(View view){
        //txtResponse = (TextView) view.findViewById(R.id.txtResponse);
        jdwl = new ArrayList<>();
        mList = (StickyListHeadersListView) view.findViewById(R.id.list);
        txtLastUpdate = (TextView)view.findViewById(R.id.txtLastUpdate);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(activity, DetailJadwalKuliahActivity.class);
                intent.putExtra(DetailJadwalKuliahActivity.PARAMS_POSITION, position);
                startActivity(intent);
            }

        });

        setValueToUI();
    }

    public void requestToServer(){
        RequestTask task = new RequestTask(this);
        task.execute(JadwalKuliahService.endPoint);
    }

    public void setValueToUI(){

        if (MainData.getInstance().getJadwalKuliahList() == null){
            requestToServer();
            return;
        }
        jdwl = MainData.getInstance().getJadwalKuliahList();
        if(!jdwl.isEmpty()) {
            refreshListView();

        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.activity = (MainActivity)activity;
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onTaskCompleted(ResponseData response) {
        this.activity.hideLoading();

        if (response.getStatus() == HttpURLConnection.HTTP_OK){
            jdwl = JadwalKuliahService.parserJsonResult(response.getResponseString());
            if (jdwl != null && jdwl.size() > 0){
                //delete alarm
                AlarmReceiver.cancelJadwalKuliah(activity);

                // save data
                MainData.getInstance().setJadwalKuliahLastUpdate(new Date());
                MainData.getInstance().setJadwalKuliahList(jdwl);
                MainData.getInstance().save(this.activity);

                // set alarm
                AlarmReceiver.setScheduleJadwalKuliah(activity);
                refreshListView();

                activity.showLoadSuccessToast();
            }
            return;
        }else if (response.getStatus() == HttpURLConnection.HTTP_UNAUTHORIZED){
            activity.showAlertToken();
        }else {
            AlertHelper.ShowAlert(activity, activity.getString(R.string.load_failed), response.getResponseString());
        }
    }

    private void refreshListView(){
        mAdapter = new JadwalKuliahAdapter(getActivity(), jdwl);
        mList.setAdapter(mAdapter);
        txtLastUpdate.setText(EnumHari.generateLastUpdate(MainData.getInstance().getJadwalKuliahLastUpdate()));
    }

    @Override
    public void onTaskStarted() {
        this.activity.showLoading();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


}

