package id.ac.unpas.sijalak;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.util.Date;
import java.util.List;

import id.ac.unpas.sijalak.model.HasilKuisioner;
import id.ac.unpas.sijalak.model.JadwalKuliah;
import id.ac.unpas.sijalak.model.JadwalMengajar;
import id.ac.unpas.sijalak.model.JadwalUjian;
import id.ac.unpas.sijalak.model.Nilai;
import id.ac.unpas.sijalak.model.Profile;
import id.ac.unpas.sijalak.model.RekapKuesioner;
import id.ac.unpas.sijalak.model.RekapMahasiswa;
import id.ac.unpas.sijalak.model.RekapNilai;
import id.ac.unpas.sijalak.model.StatusPerwalian;

/**
 * Created by Sidiq on 2/27/2016.
 */
public class MainData implements Serializable {
    //Server host
    private static String serverHost = "http://service.unpas.ac.id/";
    //private String serverHost = "http://192.168.1.166:9090/tugas-akhir/ws/";

    // Properties
    private String fakultas;
    private boolean hasLoaded = false;
    private Profile profile;
    private List<JadwalKuliah> jadwalKuliahList;
    private List<JadwalMengajar> jadwalMengajarList;
    private List<JadwalUjian> jadwalUjianList;
    private List<Nilai> nilaiList;
    private List<HasilKuisioner> hasilKuisionerList;
    private List<StatusPerwalian> statusPerwalianList;
    private List<RekapMahasiswa> rekapMahasiswaList;
    private List<RekapNilai> rekapNilaiList;
    private List<RekapKuesioner> rekapKuesionerList;


    // last update setiap fitur
    private Date jadwalKuliahLastUpdate;
    private Date jadwalUjianLastUpdate;
    private Date jadwalMengajarLastUpdate;
    private Date nilaiLastUpdate;
    private Date hasilKuisionerLastUpdate;
    private Date statusPerwalianLastUpdate;
    private Date rekapMahasiswaLastUpdate;
    private Date rekapNilaiLastUpdate;
    private Date rekapKuesionerLastUpdate;

    public List<RekapNilai> getRekapNilaiList() {
        return rekapNilaiList;
    }

    public void setRekapNilaiList(List<RekapNilai> rekapNilaiList) {
        this.rekapNilaiList = rekapNilaiList;
    }

    public List<RekapKuesioner> getRekapKuesionerList() {
        return rekapKuesionerList;
    }

    public void setRekapKuesionerList(List<RekapKuesioner> rekapKuesionerList) {
        this.rekapKuesionerList = rekapKuesionerList;
    }

    public Date getRekapMahasiswaLastUpdate() {
        return rekapMahasiswaLastUpdate;
    }

    public Date getRekapNilaiLastUpdate() {
        return rekapNilaiLastUpdate;
    }

    public void setRekapNilaiLastUpdate(Date rekapNilaiLastUpdate) {
        this.rekapNilaiLastUpdate = rekapNilaiLastUpdate;
    }

    public Date getRekapKuesionerLastUpdate() {
        return rekapKuesionerLastUpdate;
    }

    public void setRekapKuesionerLastUpdate(Date rekapKuesionerLastUpdate) {
        this.rekapKuesionerLastUpdate = rekapKuesionerLastUpdate;
    }

    private static String filename = "data.sav";
    private static MainData ourInstance;

    public static MainData getInstance() {
        if (ourInstance == null)
            ourInstance = new MainData();
        return ourInstance;
    }

    private MainData() {
    }

    public static void newInstance(){
        ourInstance = new MainData();
    }

    public void save(Context context){
        try {
            FileOutputStream fos;
            fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(this);
            os.close();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load(Context context){
        try {
            FileInputStream fis = context.openFileInput(filename);
            ObjectInputStream is = new ObjectInputStream(fis);

            MainData result = (MainData) is.readObject();
            fakultas = result.fakultas;
            profile = result.profile;
            hasLoaded = result.hasLoaded;
            jadwalKuliahList = result.jadwalKuliahList;
            jadwalMengajarList = result.jadwalMengajarList;
            jadwalUjianList = result.jadwalUjianList;
            nilaiList = result.nilaiList;
            hasilKuisionerList = result.hasilKuisionerList;
            statusPerwalianList = result.statusPerwalianList;

            jadwalKuliahLastUpdate = result.jadwalKuliahLastUpdate;
            jadwalMengajarLastUpdate = result.jadwalMengajarLastUpdate;
            jadwalUjianLastUpdate = result.jadwalUjianLastUpdate;
            nilaiLastUpdate = result.nilaiLastUpdate;
            hasilKuisionerLastUpdate = result.hasilKuisionerLastUpdate;
            statusPerwalianLastUpdate = result.statusPerwalianLastUpdate;

            rekapMahasiswaLastUpdate = result.rekapMahasiswaLastUpdate;
            rekapKuesionerLastUpdate = result.rekapKuesionerLastUpdate;
            rekapNilaiLastUpdate = result.rekapNilaiLastUpdate;

            rekapMahasiswaList = result.rekapMahasiswaList;
            rekapKuesionerList = result.rekapKuesionerList;
            rekapNilaiList = result.rekapNilaiList;

            is.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (OptionalDataException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean getHasLoaded() {
        return hasLoaded;
    }

    public void setHasLoaded(boolean hasLoaded) {
        this.hasLoaded = hasLoaded;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getServer() {
        return this.serverHost;
    }



    public List<JadwalKuliah> getJadwalKuliahList() {
        return jadwalKuliahList;
    }

    public void setJadwalKuliahList(List<JadwalKuliah> jadwalKuliahList) {
        this.jadwalKuliahList = jadwalKuliahList;
    }

    public List<JadwalMengajar> getJadwalMengajarList() {
        return jadwalMengajarList;
    }

    public void setJadwalMengajarList(List<JadwalMengajar> jadwalMengajarList) {
        this.jadwalMengajarList = jadwalMengajarList;
    }

    public List<JadwalUjian> getJadwalUjianList() {
        return jadwalUjianList;
    }

    public void setJadwalUjianList(List<JadwalUjian> jadwalUjianList) {
        this.jadwalUjianList = jadwalUjianList;
    }

    public Date getJadwalKuliahLastUpdate() {
        return jadwalKuliahLastUpdate;
    }

    public void setJadwalKuliahLastUpdate(Date jadwalKuliahLastUpdate) {
        this.jadwalKuliahLastUpdate = jadwalKuliahLastUpdate;
    }

    public Date getJadwalUjianLastUpdate() {
        return jadwalUjianLastUpdate;
    }

    public void setJadwalUjianLastUpdate(Date jadwalUjianLastUpdate) {
        this.jadwalUjianLastUpdate = jadwalUjianLastUpdate;
    }

    public Date getJadwalMengajarLastUpdate() {
        return jadwalMengajarLastUpdate;
    }

    public void setJadwalMengajarLastUpdate(Date jadwalMengajarLastUpdate) {
        this.jadwalMengajarLastUpdate = jadwalMengajarLastUpdate;
    }

    public List<Nilai> getNilaiList() {
        return nilaiList;
    }

    public void setNilaiList(List<Nilai> nilaiList) {
        this.nilaiList = nilaiList;
    }

    public Date getNilaiLastUpdate() {
        return nilaiLastUpdate;
    }

    public void setNilaiLastUpdate(Date nilaiLastUpdate) {
        this.nilaiLastUpdate = nilaiLastUpdate;
    }

    public List<HasilKuisioner> getHasilKuisionerList() {
        return hasilKuisionerList;
    }

    public void setHasilKuisionerList(List<HasilKuisioner> hasilKuisionerList) {
        this.hasilKuisionerList = hasilKuisionerList;
    }

    public List<StatusPerwalian> getStatusPerwalianList() {
        return statusPerwalianList;
    }

    public void setStatusPerwalianList(List<StatusPerwalian> statusPerwalianList) {
        this.statusPerwalianList = statusPerwalianList;
    }

    public List<RekapMahasiswa> getRekapMahasiswaList() {
        return rekapMahasiswaList;
    }

    public void setRekapMahasiswaList(List<RekapMahasiswa> rekapMahasiswaList) {
        this.rekapMahasiswaList = rekapMahasiswaList;
    }

    public Date getHasilKuisionerLastUpdate() {
        return hasilKuisionerLastUpdate;
    }

    public void setHasilKuisionerLastUpdate(Date hasilKuisionerLastUpdate) {
        this.hasilKuisionerLastUpdate = hasilKuisionerLastUpdate;
    }

    public Date getStatusPerwalianLastUpdate() {
        return statusPerwalianLastUpdate;
    }

    public void setStatusPerwalianLastUpdate(Date statusPerwalianLastUpdate) {
        this.statusPerwalianLastUpdate = statusPerwalianLastUpdate;
    }

    public Date getRekapMahasiswaLastLastUpdate() {
        return rekapMahasiswaLastUpdate;
    }
    public void setRekapMahasiswaLastUpdate(Date date) {
        this.rekapMahasiswaLastUpdate = date;
    }

    public int countJmlSksPerSemester(String semesterId){
        int jmlSks = 0;
        for (Nilai n: getNilaiList()){
            if (n.getIdSemester().equals(semesterId)){
                jmlSks += n.getSks();
            }
        }
        return jmlSks;
    }

    public int countTotalSks(){
        int jmlSks = 0;
        for (Nilai n: getNilaiList()){
            jmlSks += n.getSks();
        }
        return jmlSks;
    }

    public float countIpk(){
        int jmlSks = 0;
        int jmlNilai = 0;
        for (Nilai n: getNilaiList()){
            jmlSks += n.getSks();
            jmlNilai += n.getBobot()*n.getSks();
        }
        return (float)jmlNilai/(float)jmlSks;
    }

    public float countIps(String semesterId){
        int jmlSks = 0;
        int jmlNilai = 0;
        for (Nilai n: getNilaiList()){
            if (n.getIdSemester().equals(semesterId)){
                jmlSks += n.getSks();
                jmlNilai += n.getBobot()*n.getSks();
            }
        }
        return (float)jmlNilai/(float)jmlSks;
    }

    public String getFakultas() {
        return fakultas;
    }

    public void setFakultas(String fakultas) {
        this.fakultas = fakultas;
    }
}