package id.ac.unpas.sijalak.detailActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import id.ac.unpas.sijalak.MainData;
import id.ac.unpas.sijalak.R;
import id.ac.unpas.sijalak.helper.ThemeManager;
import id.ac.unpas.sijalak.model.Nilai;

public class DetailNilaiActivity extends AppCompatActivity {

    public final static String PARAMS_POSITION = "item_position";
    private Nilai nilai;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ThemeManager.setCurrentTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_nilai);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent myIntent = getIntent();
        int position = myIntent.getIntExtra(PARAMS_POSITION, 0);
        nilai = MainData.getInstance().getNilaiList().get(position);


        String title = "Detail Nilai ["+ nilai.getKodeMk()+"]";
        if (ThemeManager.isTextDark()){
            setTitle(Html.fromHtml("<font color='#000000'>" + title + "</font>"));
        }else {
            setTitle(title);
        }
        setUiValue();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUiValue(){
        ((TextView)findViewById(R.id.txtMataKuliah)).setText(nilai.getMataKuliah());
        ((TextView)findViewById(R.id.txtSks)).setText(nilai.getSks() + " sks");
        ((TextView)findViewById(R.id.txtTugas1)).setText("Tugas 1 : " + nilai.getTugas1());
        ((TextView)findViewById(R.id.txtTugas2)).setText("Tugas 2 : " + nilai.getTugas2());
        ((TextView)findViewById(R.id.txtTugas3)).setText("Tugas 3 : " + nilai.getTugas3());
        ((TextView)findViewById(R.id.txtTugas4)).setText("Tugas 4 : " + nilai.getTugas4());
        ((TextView)findViewById(R.id.txtTugas5)).setText("Tugas 5 : " + nilai.getTugas5());
        ((TextView)findViewById(R.id.txtUts)).setText(nilai.getUts());
        ((TextView)findViewById(R.id.txtUas)).setText(nilai.getUas());
        ((TextView)findViewById(R.id.txtAkhir)).setText(nilai.getAkhir());
        ((TextView)findViewById(R.id.txtGrade)).setText(nilai.getGrade());
        ((TextView)findViewById(R.id.txtBobot)).setText(Float.toString(nilai.getBobot()));

    }
}
