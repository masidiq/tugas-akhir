package id.ac.unpas.sijalak.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sidiq on 3/13/2016.
 */
public class ResponseData {
    private int status;
    private String responseString;
    private static String KEY_ERROR_MESSAGE = "errorMessage";

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResponseString() {
        return responseString;
    }

    public void setResponseString(String responseString) {
        this.responseString = responseString;
    }


    public String parseErrorMessage(){
        try {
            if (responseString == null)
                return null;
            JSONObject j = new JSONObject(responseString);

            return j.getString(KEY_ERROR_MESSAGE);
        } catch (JSONException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
