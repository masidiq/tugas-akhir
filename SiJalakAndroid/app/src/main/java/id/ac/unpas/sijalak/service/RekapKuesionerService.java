package id.ac.unpas.sijalak.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.ac.unpas.sijalak.model.RekapKuesioner;
import id.ac.unpas.sijalak.model.RekapMahasiswa;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class RekapKuesionerService {
    public static String endPoint = "pimpinan/rekap_kuesioner";
    public static List<RekapKuesioner> parserJsonResult(String jsonStr){
        try {

            List<RekapKuesioner> result = new ArrayList<>();
            if (jsonStr == null)
                return result;

            JSONArray jsonarray = new JSONArray(jsonStr);

            for (int i=0; i<jsonarray.length(); i++){
                JSONObject json = jsonarray.getJSONObject(i);
                RekapKuesioner s = new RekapKuesioner();

                s.setKodeProdi(json.getString("kode_prodi"));
                s.setNamaProdi(json.getString("nama_prodi"));
                s.setMin(json.getString("v_min"));
                s.setMax(json.getString("v_max"));
                s.setAvg(json.getString("v_avg"));
                result.add(s);

            }
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
