package id.ac.unpas.sijalak.customAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.ac.unpas.sijalak.R;
import id.ac.unpas.sijalak.model.HasilKuisioner;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class HasilKuisionerAdapter extends ArrayAdapter<HasilKuisioner> {
    private final Context context;
    private final List<HasilKuisioner> listValue;


    public HasilKuisionerAdapter(Context context, List<HasilKuisioner> listValue) {
        super(context,-1, listValue);
        this.context = context;
        this.listValue = listValue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_hasil_kuisioner, parent, false);
        ((TextView) rowView.findViewById(R.id.txtSemester)).setText("Semester " + listValue.get(position).getIdSemester());
        ((TextView) rowView.findViewById(R.id.txtKelas)).setText("Kelas " + listValue.get(position).getKelas());
        ((TextView) rowView.findViewById(R.id.txtProdi)).setText(listValue.get(position).getProdi());
        ((TextView) rowView.findViewById(R.id.txtMataKuliah)).setText(listValue.get(position).getMataKuliah() + " (" + listValue.get(position).getSks() + " sks)");
        ((TextView) rowView.findViewById(R.id.txtNilai)).setText("Nilai IKD : " + listValue.get(position).getNilai());
        ((TextView) rowView.findViewById(R.id.txtPeserta)).setText(listValue.get(position).getPeserta());
        ((TextView) rowView.findViewById(R.id.txtResponden)).setText(listValue.get(position).getResponden());
        ((TextView) rowView.findViewById(R.id.txtRataRata)).setText("Rata-rata : " + listValue.get(position).getRataRata());
        return rowView;
    }
}
