package id.ac.unpas.sijalak.detailActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;

import java.text.Format;
import java.text.SimpleDateFormat;

import id.ac.unpas.sijalak.MainData;
import id.ac.unpas.sijalak.R;
import id.ac.unpas.sijalak.helper.ThemeManager;
import id.ac.unpas.sijalak.model.JadwalMengajar;
import id.ac.unpas.sijalak.model.Kehadiran;

public class DetailJadwalMengajarActivity extends AppCompatActivity {
    public final static String PARAMS_POSITION = "index";
    private JadwalMengajar jadwalMengajar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ThemeManager.setCurrentTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_jadwal_mengajar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent myIntent = getIntent();
        int position = myIntent.getIntExtra(PARAMS_POSITION, 0);
        jadwalMengajar = MainData.getInstance().getJadwalMengajarList().get(position);

        String title = "Detail Jadwal Mengajar ["+jadwalMengajar.getKodeMk()+"]";
        if (ThemeManager.isTextDark()){
            setTitle(Html.fromHtml("<font color='#000000'>" + title + "</font>"));
        }else {
            setTitle(title);
        }
        setUiValue();
        generateTableKehadiran();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void generateTableKehadiran(){
        // Generate Table kehadiran
        Format formatter = new SimpleDateFormat("dd-MM-yyyy");
        LayoutInflater inflater = LayoutInflater.from(this);
        TableLayout tl = (TableLayout) findViewById(R.id.table);
        int i = 1;
        for (Kehadiran kehadiran : jadwalMengajar.getKehadiranList()){
            View tr = inflater.inflate(R.layout.row_table_kehadiran,null, false);
            ((TextView)tr.findViewById(R.id.txtNo)).setText(Integer.toString(i));
            ((TextView)tr.findViewById(R.id.txtTgl)).setText(formatter.format(kehadiran.getTanggal()));
            ((TextView)tr.findViewById(R.id.txtMulai)).setText(kehadiran.getMulai());
            ((TextView)tr.findViewById(R.id.txtSelesai)).setText(kehadiran.getSelesai());
            ((TextView)tr.findViewById(R.id.txtStatus)).setText(kehadiran.getStatus());
            if (i % 2 != 0)
                tr.setBackgroundColor(getResources().getColor(R.color.odd_row));
            else
                tr.setBackgroundColor(getResources().getColor(R.color.even_row));
            tl.addView(tr);
            i++;
        }
    }

    private void setUiValue(){
        ((TextView)findViewById(R.id.txtMataKuliah)).setText(jadwalMengajar.getMataKuliah());
        ((TextView)findViewById(R.id.txtSks)).setText(jadwalMengajar.getSks() + " sks");
        ((TextView)findViewById(R.id.txtHari)).setText(jadwalMengajar.getHari() + ", " + jadwalMengajar.getJam());
        ((TextView)findViewById(R.id.txtRuang)).setText(jadwalMengajar.getRuang());
        ((TextView)findViewById(R.id.txtKelas)).setText("Kelas " + jadwalMengajar.getKelas());
        ((TextView)findViewById(R.id.txtPresensi)).setText(jadwalMengajar.getPresensiLong());
    }
}
