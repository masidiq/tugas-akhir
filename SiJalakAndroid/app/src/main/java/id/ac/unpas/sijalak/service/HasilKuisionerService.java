package id.ac.unpas.sijalak.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.ac.unpas.sijalak.model.HasilKuisioner;
import id.ac.unpas.sijalak.model.Pertanyaan;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class HasilKuisionerService {

    public static String endPoint = "dosen/hasil_kuisioner";
    private static final String KEY_MataKuliah = "mata_kuliah";
    private static final String KEY_Kelas = "kelas";
    private static final String KEY_Prodi = "prodi";
    private static final String KEY_Semester = "semester";
    private static final String KEY_Sks = "sks";
    private static final String KEY_Peserta = "peserta";
    private static final String KEY_Responden = "responden";
    private static final String KEY_Nilai = "nilai";
    private static final String KEY_RataRata = "rata_rata";


    private static final String KEY_ListPertanyaan = "list_pertanyaan";
    private static final String KEY_Pertanyaan = "pertanyaan";
    private static final String KEY_Pertanyaan_Nilai = "nilai";



    public static List<HasilKuisioner> parserJsonResult(String jsonStr){
        try {

            List<HasilKuisioner> result = new ArrayList<>();
            if (jsonStr == null)
                return result;

            JSONArray jsonarray = new JSONArray(jsonStr);

            for (int i=0; i<jsonarray.length(); i++){
                JSONObject json = jsonarray.getJSONObject(i);
                HasilKuisioner s = new HasilKuisioner();

                s.setIdSemester(json.getString(KEY_Semester));
                s.setKelas(json.getString(KEY_Kelas));
                s.setMataKuliah(json.getString(KEY_MataKuliah));
                s.setNilai(json.getString(KEY_Nilai));
                s.setPeserta(json.getString(KEY_Peserta));
                s.setProdi(json.getString(KEY_Prodi));
                s.setSks(json.getString(KEY_Sks));
                s.setResponden(json.getString(KEY_Responden));
                s.setRataRata(json.getString(KEY_RataRata));


                // DETAIL PERTANYAAN
                JSONArray listPertanyaanJSON = json.getJSONArray(KEY_ListPertanyaan);
                if (listPertanyaanJSON != null) {
                    for (int i2 = 0; i2 < listPertanyaanJSON.length(); i2++) {
                        JSONObject o2 = listPertanyaanJSON.getJSONObject(i2);
                        Pertanyaan p = new Pertanyaan();
                        p.setPertanyaan(o2.getString(KEY_Pertanyaan));
                        p.setNilai(o2.getString(KEY_Nilai));
                        s.getListPertanyaan().add(p);
                    }
                }
                result.add(s);
            }
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
