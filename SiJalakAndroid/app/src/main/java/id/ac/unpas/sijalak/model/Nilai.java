package id.ac.unpas.sijalak.model;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by Sidiq on 2/28/2016.
 */
public class Nilai  implements Serializable{
    public String mataKuliah;
    public String idSemester;
    public String kodeMk;
    public String kelas;
    public int sks;
    public String tugas1;
    public String tugas2;
    public String tugas3;
    public String tugas4;
    public String tugas5;
    public String presensi;
    public String uts;
    public String uas;
    public String grade;
    public String akhir;

    public float getBobot() {
        return bobot;
    }

    public void setBobot(float bobot) {
        this.bobot = bobot;
    }

    public float bobot;

    public int no;

    public String getMataKuliah() {
        return mataKuliah;
    }

    public void setMataKuliah(String mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    public String getIdSemester() {
        return idSemester;
    }

    public void setIdSemester(String idSemester) {
        this.idSemester = idSemester;
    }

    public String getKodeMk() {
        return kodeMk;
    }

    public void setKodeMk(String kodeMk) {
        this.kodeMk = kodeMk;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public int getSks() {
        return sks;
    }

    public void setSks(int sks) {
        this.sks = sks;
    }

    public String getTugas1() {
        return tugas1;
    }

    public void setTugas1(String tugas1) {
        this.tugas1 = tugas1;
    }

    public String getTugas2() {
        return tugas2;
    }

    public void setTugas2(String tugas2) {
        this.tugas2 = tugas2;
    }

    public String getTugas3() {
        return tugas3;
    }

    public void setTugas3(String tugas3) {
        this.tugas3 = tugas3;
    }

    public String getTugas4() {
        return tugas4;
    }

    public void setTugas4(String tugas4) {
        this.tugas4 = tugas4;
    }

    public String getTugas5() {
        return tugas5;
    }

    public void setTugas5(String tugas5) {
        this.tugas5 = tugas5;
    }

    public String getPresensi() {
        return presensi;
    }

    public void setPresensi(String presensi) {
        this.presensi = presensi;
    }

    public String getUts() {
        return uts;
    }

    public void setUts(String uts) {
        this.uts = uts;
    }

    public String getUas() {
        return uas;
    }

    public void setUas(String uas) {
        this.uas = uas;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getAkhir() {
        return akhir;
    }

    public void setAkhir(String akhir) {
        this.akhir = akhir;
    }



    public static Comparator<Nilai> SemesterAsc = new Comparator<Nilai>() {
        public int compare(Nilai s1, Nilai s2) {
            String str1 = s1.getIdSemester() + s1.getKodeMk().toUpperCase();
            String str2 = s2.getIdSemester() + s2.getKodeMk().toUpperCase();
            return str2.compareTo(str1);
        }
    };

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }
}
