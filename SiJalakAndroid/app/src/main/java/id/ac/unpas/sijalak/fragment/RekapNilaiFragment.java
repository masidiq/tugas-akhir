package id.ac.unpas.sijalak.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.ac.unpas.sijalak.MainActivity;
import id.ac.unpas.sijalak.MainData;
import id.ac.unpas.sijalak.R;
import id.ac.unpas.sijalak.customAdapter.RekapMahasiswaAdapter;
import id.ac.unpas.sijalak.customAdapter.RekapNilaiAdapter;
import id.ac.unpas.sijalak.helper.EnumHari;
import id.ac.unpas.sijalak.model.RekapMahasiswa;
import id.ac.unpas.sijalak.model.RekapNilai;
import id.ac.unpas.sijalak.service.RekapMahasiswaService;
import id.ac.unpas.sijalak.service.RekapNilaiService;
import id.ac.unpas.sijalak.utils.AlertHelper;
import id.ac.unpas.sijalak.utils.OnTaskListener;
import id.ac.unpas.sijalak.utils.RequestTask;
import id.ac.unpas.sijalak.utils.ResponseData;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RekapNilaiFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RekapNilaiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RekapNilaiFragment extends Fragment  implements OnTaskListener {
    private OnFragmentInteractionListener mListener;
    private MainActivity activity;
    private ListView mList;
    private RekapNilaiAdapter mAdapter;
    private List<RekapNilai> mainList;
    private TextView txtLastUpdate;

    public static RekapNilaiFragment newInstance(String param1, String param2) {
        RekapNilaiFragment fragment = new RekapNilaiFragment();
        return fragment;
    }

    public RekapNilaiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_rekap_nilai, container, false);
        initUI(view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.activity = (MainActivity)activity;
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onTaskCompleted(ResponseData response) {
        this.activity.hideLoading();

        if (response.getStatus() == HttpURLConnection.HTTP_OK){
            mainList = RekapNilaiService.parserJsonResult(response.getResponseString());
            if (mainList != null && mainList.size() > 0){
                MainData.getInstance().setRekapNilaiLastUpdate(new Date());
                MainData.getInstance().setRekapNilaiList(mainList);
                MainData.getInstance().save(this.activity);
                refreshListView();
                activity.showLoadSuccessToast();
            }
            return;
        }else if (response.getStatus() == HttpURLConnection.HTTP_UNAUTHORIZED){
            activity.showAlertToken();
        }else {
            AlertHelper.ShowAlert(activity, activity.getString(R.string.load_failed), response.getResponseString());
        }
    }

    @Override
    public void onTaskStarted() {
        this.activity.showLoading();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private void initUI(View view){
        mainList = new ArrayList<>();
        mList = (ListView) view.findViewById(R.id.list);
        txtLastUpdate = (TextView)view.findViewById(R.id.txtLastUpdate);
        setValueToUI();
    }

    public void requestToServer(){
        RequestTask task = new RequestTask(this);
        task.execute(RekapNilaiService.endPoint);
    }

    public void setValueToUI(){
        if (MainData.getInstance().getRekapNilaiList() == null){
            requestToServer();
            return;
        }
        mainList = MainData.getInstance().getRekapNilaiList();
        if(!mainList.isEmpty()) {
            refreshListView();

        }
    }

    private void refreshListView(){
        mAdapter = new RekapNilaiAdapter(getActivity(), mainList);
        mList.setAdapter(mAdapter);
        txtLastUpdate.setText(EnumHari.generateLastUpdate(MainData.getInstance().getRekapNilaiLastUpdate()));
    }

}
