package id.ac.unpas.sijalak.helper;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Sidiq on 3/13/2016.
 */
public class EnumHari {

    public static long getId(String hari){
        hari = hari.toLowerCase();
        switch (hari) {
            case "senin":
                return 1;
            case "selasa":
                return 2;
            case "rabu":
                return 3;
            case "kamis":
                return 4;
            case "jumat":
                return 5;
            case "sabtu":
                return 6;
            case "minggu":
                return 7;
            default:
                return 0;
        }
    }

    public static int getEnum(String hari){
        hari = hari.toLowerCase();
        switch (hari) {
            case "minggu":
                return Calendar.SUNDAY;
            case "senin":
                return Calendar.MONDAY;
            case "selasa":
                return Calendar.TUESDAY;
            case "rabu":
                return Calendar.WEDNESDAY;
            case "kamis":
                return Calendar.THURSDAY;
            case "jumat":
                return Calendar.FRIDAY;
            case "sabtu":
                return Calendar.SATURDAY;
            default:
                return 0;
        }
    }



    public static String getHari(int dayOfWeek){
        switch (dayOfWeek) {
            case 1:
                return "Minggu";
            case 2:
                return "Senin";
            case 3:
                return "Selasa";
            case 4:
                return "Rabu";
            case 5:
                return "Kamis";
            case 6:
                return "Jumat";
            case 7:
                return "Sabtu";

            default:
                return "-";
        }
    }

    public static String generateLastUpdate(Date date){
        Format formatter = new SimpleDateFormat("dd-MM-yyyy, HH:mm");
        return "Terakhir update : " + formatter.format(date);
    }

    public static String dateToString(Date date){
        Format formatter = new SimpleDateFormat("dd-MM-yyyy");
        return formatter.format(date);
    }

    public static String dateToShortString(Date date){
        if (date == null)
            return "-";
        Format formatter = new SimpleDateFormat("dd-MM-yy");
        String dateStr = formatter.format(date);
        return dateStr;
        //return dateStr.substring(0,6)  + "'" + dateStr.substring(6,8);
    }
}
