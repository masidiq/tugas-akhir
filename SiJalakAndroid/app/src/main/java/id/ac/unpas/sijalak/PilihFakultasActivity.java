package id.ac.unpas.sijalak;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import id.ac.unpas.sijalak.helper.ThemeManager;
import id.ac.unpas.sijalak.utils.Settings;

public class PilihFakultasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MainData.getInstance().load(this);
        super.onCreate(savedInstanceState);


        if (Settings.hasConnected(this)){
            goToMainActivity();
        }else {
            goToLoginActivity(2);
        }
        setContentView(R.layout.activity_pilih_fakultas);

        ListView listView = (ListView)findViewById(R.id.list);
        String[] values = new String[] {
                "Fakultas Hukum",
                "Fakultas Ilmu Sosial & Ilmu Politik",
                "Fakultas Teknik",
                "Fakultas Ekonomi",
                "Fakultas Keguruan & Ilmu Pendidikan",
                "Fakultas Ilmu Seni & Sastra",
                "Pascasarjana"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // ListView Clicked item index
                int itemPosition = position;

                goToLoginActivity(position);
            }

        });
    }

    private void goToLoginActivity(int position){
        MainData.getInstance().setFakultas(ThemeManager.getFakultasByPosition(position));
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
