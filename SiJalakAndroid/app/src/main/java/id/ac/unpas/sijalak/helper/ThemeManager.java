package id.ac.unpas.sijalak.helper;

import android.content.Context;

import id.ac.unpas.sijalak.MainData;
import id.ac.unpas.sijalak.R;

/**
 * Created by Sidiq on 3/29/2016.
 */
public class ThemeManager {

    public static String ADMINISTRASI = "baa";
    public static String HUKUM= "fh";
    public static String SOSIAL_POLITIK = "fisip";
    public static String TEKNIK = "unpas";
    public static String EKONOMI = "fe";
    public static String KEGURUAN= "fkip";
    public static String SENI= "fiss";
    public static String PASCA_SARJANA= "fps";

    public static void setCurrentTheme(Context context){
        String fakultas = MainData.getInstance().getFakultas();
        if (fakultas.equals(ADMINISTRASI)){
            context.setTheme(R.style.Theme_Administrasi);
        } else if (fakultas.equals(HUKUM)){
            context.setTheme(R.style.Theme_Hukum);
        } else if (fakultas.equals(SOSIAL_POLITIK)){
            context.setTheme(R.style.Theme_SosialPolitik);
        } else if (fakultas.equals(TEKNIK)){
            context.setTheme(R.style.Theme_Teknik);
        } else if (fakultas.equals(EKONOMI)){
            context.setTheme(R.style.Theme_Ekonomi);
        } else if (fakultas.equals(KEGURUAN)){
            context.setTheme(R.style.Theme_Keguruan);
        } else if (fakultas.equals(SENI)){
            context.setTheme(R.style.Theme_Seni);
        } else if (fakultas.equals(PASCA_SARJANA)){
            context.setTheme(R.style.Theme_PascaSarjana);
        } else {
            context.setTheme(R.style.Theme_Teknik);
        }
    }

    public static void setCurrentThemeNoActionBar(Context context){
        String fakultas = MainData.getInstance().getFakultas();
        if (fakultas.equals(ADMINISTRASI)){
            context.setTheme(R.style.Theme_Administrasi_NoActionBar);
        } else if (fakultas.equals(HUKUM)){
            context.setTheme(R.style.Theme_Hukum_NoActionBar);
        } else if (fakultas.equals(SOSIAL_POLITIK)){
            context.setTheme(R.style.Theme_SosialPolitik_NoActionBar);
        } else if (fakultas.equals(TEKNIK)){
            context.setTheme(R.style.Theme_Teknik_NoActionBar);
        } else if (fakultas.equals(EKONOMI)){
            context.setTheme(R.style.Theme_Ekonomi_NoActionBar);
        } else if (fakultas.equals(KEGURUAN)){
            context.setTheme(R.style.Theme_Keguruan_NoActionBar);
        } else if (fakultas.equals(SENI)){
            context.setTheme(R.style.Theme_Seni_NoActionBar);
        } else if (fakultas.equals(PASCA_SARJANA)){
            context.setTheme(R.style.Theme_PascaSarjana_NoActionBar);
        } else {
            context.setTheme(R.style.Theme_Teknik_NoActionBar);
        }
    }

    public static String getNamaFakultas(){
        String fakultas = MainData.getInstance().getFakultas();
        if (fakultas.equals(ADMINISTRASI)){
            return "Fakultas Biro Administrasi Akademik";
        } else if (fakultas.equals(HUKUM)){
            return "Fakultas Hukum";
        } else if (fakultas.equals(SOSIAL_POLITIK)){
            return "Fakultas Ilmu Sosial & Ilmu Politik";
        } else if (fakultas.equals(TEKNIK)){
            return "Universitas Pasundan";
        } else if (fakultas.equals(EKONOMI)){
            return "Fakultas Ekonomi";
        } else if (fakultas.equals(KEGURUAN)){
            return "Fakultas Keguruan";
        } else if (fakultas.equals(SENI)){
            return "Fakultas Seni";
        } else if (fakultas.equals(PASCA_SARJANA)){
            return "Fakultas Pasca Sarjana";
        } else {
            return "";
        }
    }

    public static String getFakultasByPosition(int pos){
        switch (pos) {
//            case 0:
//                return ADMINISTRASI;
            case 0:
                return HUKUM;
            case 1:
                return SOSIAL_POLITIK;
            case 2:
                return TEKNIK;
            case 3:
                return EKONOMI;
            case 4:
                return KEGURUAN;
            case 5:
                return SENI;
            case 6:
                return PASCA_SARJANA;

            default:
                return TEKNIK;
        }
    }

    public static int getColorPrimary(Context context){
        String fakultas = MainData.getInstance().getFakultas();

        if (fakultas.equals(ADMINISTRASI)){
            return context.getResources().getColor(R.color.colorPrimaryAdministrasi);
        } else if (fakultas.equals(HUKUM)){
            return context.getResources().getColor(R.color.colorPrimaryHukum);
        } else if (fakultas.equals(SOSIAL_POLITIK)){
            return context.getResources().getColor(R.color.colorPrimarySosial);
        } else if (fakultas.equals(TEKNIK)){
            return context.getResources().getColor(R.color.colorPrimaryTeknik);
        } else if (fakultas.equals(EKONOMI)){
            return context.getResources().getColor(R.color.colorPrimaryEkonomi);
        } else if (fakultas.equals(KEGURUAN)){
            return context.getResources().getColor(R.color.colorPrimaryKeguruan);
        } else if (fakultas.equals(SENI)){
            return context.getResources().getColor(R.color.colorPrimarySeni);
        } else if (fakultas.equals(PASCA_SARJANA)){
            return context.getResources().getColor(R.color.colorPrimaryPascaSarjana);
        } else {
            return context.getResources().getColor(R.color.colorPrimaryTeknik);
        }
    }

    public static int getColorPrimaryDark(Context context){
        String fakultas = MainData.getInstance().getFakultas();
        if (fakultas.equals(ADMINISTRASI)){
            return context.getResources().getColor(R.color.colorPrimaryAdministrasiDark);
        } else if (fakultas.equals(HUKUM)){
            return context.getResources().getColor(R.color.colorPrimaryHukumDark);
        } else if (fakultas.equals(SOSIAL_POLITIK)){
            return context.getResources().getColor(R.color.colorPrimarySosialDark);
        } else if (fakultas.equals(TEKNIK)){
            return context.getResources().getColor(R.color.colorPrimaryTeknikDark);
        } else if (fakultas.equals(EKONOMI)){
            return context.getResources().getColor(R.color.colorPrimaryEkonomiDark);
        } else if (fakultas.equals(KEGURUAN)){
            return context.getResources().getColor(R.color.colorPrimaryKeguruanDark);
        } else if (fakultas.equals(SENI)){
            return context.getResources().getColor(R.color.colorPrimarySeniDark);
        } else if (fakultas.equals(PASCA_SARJANA)){
            return context.getResources().getColor(R.color.colorPrimaryPascaSarjanaDark);
        } else {
            return context.getResources().getColor(R.color.colorPrimaryTeknikDark);
        }
    }

    public static boolean isTextDark(){
        String fakultas = MainData.getInstance().getFakultas();
        if (fakultas.equals(EKONOMI) || fakultas.equals(KEGURUAN)){
            return true;
        } else {
            return false;
        }
    }
}
