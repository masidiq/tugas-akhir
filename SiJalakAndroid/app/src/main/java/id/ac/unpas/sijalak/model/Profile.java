package id.ac.unpas.sijalak.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sidiq on 2/28/2016.
 */
public class Profile implements Serializable {
    public static String USER_DOSEN = "DOSEN";
    public static String USER_MAHASISWA = "MAHASISWA";
    public static String USER_PIMPINAN = "PIMPINAN";

    private String npm;
    private String nama;
    private String namaOrtu;
    private String userType;
    private String fotoUrl;
    private List<Semester> semesterList;

    public Profile() {
        semesterList = new ArrayList<>();
    }


    public String getNamaOrtu() {
        if (namaOrtu == null)
            return "";
        return namaOrtu;
    }

    public void setNamaOrtu(String namaOrtu) {
        this.namaOrtu = namaOrtu;
    }

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }


    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

//    public boolean isDosen(){
//        if (userType.equals(USER_DOSEN))
//            return true;
//        else return false;
//    }
//    public boolean isMahasiswa(){
//        if (userType.equals(USER_MAHASISWA))
//            return true;
//        else return false;
//    }


    public List<Semester> getSemesterList() {
        return semesterList;
    }

    public void setSemesterList(List<Semester> semesterList) {
        this.semesterList = semesterList;
    }

    public String getNamaSemesterById(String id){
        for (Semester s : getSemesterList()){
            if (s.getId().equals(id)){
                return s.getNama();
            }
        }
        return id;
    }

    public String getTypeUserToString(){
        if (getUserType().equals("ORTU")){
            return "Orang Tua/Wali dari";
        }else if (getUserType().equals("DOSEN")) {
            return "Dosen";
        }else if (getUserType().equals("PIMPINAN")) {
            return "Pimpinan";
        }else {
            return "Mahasiswa";
        }
    }
}
