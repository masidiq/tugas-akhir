package id.ac.unpas.sijalak.utils;

/**
 * Created by Sidiq on 2/28/2016.
 */
public interface OnTaskListener {
    void onTaskCompleted(ResponseData result);
    void onTaskStarted();
}
