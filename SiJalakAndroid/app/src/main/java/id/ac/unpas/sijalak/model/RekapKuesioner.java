package id.ac.unpas.sijalak.model;

import java.io.Serializable;

/**
 * Created by Sidiq on 19-Oct-16.
 */
public class RekapKuesioner implements Serializable {
    private String kodeProdi;
    private String namaProdi;
    private String min;
    private String max;
    private String avg;

    public String getKodeProdi() {
        return kodeProdi;
    }

    public void setKodeProdi(String kodeProdi) {
        this.kodeProdi = kodeProdi;
    }

    public String getNamaProdi() {
        return namaProdi;
    }

    public void setNamaProdi(String namaProdi) {
        this.namaProdi = namaProdi;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getAvg() {
        return avg;
    }

    public void setAvg(String avg) {
        this.avg = avg;
    }
}
