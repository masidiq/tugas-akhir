package id.ac.unpas.sijalak.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import id.ac.unpas.sijalak.model.RekapMahasiswa;
import id.ac.unpas.sijalak.model.StatusPerwalian;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class RekapMahasiswaService {
    public static String endPoint = "pimpinan/rekap_mahasiswa";
    private static final String KEY_KodeProdi = "kode_prodi";
    private static final String KEY_NamaProdi = "nama_prodi";
    private static final String KEY_A = "a";
    private static final String KEY_T = "t";
    private static final String KEY_C = "c";
    private static final String KEY_N = "n";
    private static final String KEY_D = "d";
    private static final String KEY_K = "k";
    private static final String KEY_L = "l";

    public static List<RekapMahasiswa> parserJsonResult(String jsonStr){
        try {

            List<RekapMahasiswa> result = new ArrayList<>();
            if (jsonStr == null)
                return result;

            JSONArray jsonarray = new JSONArray(jsonStr);

            for (int i=0; i<jsonarray.length(); i++){
                JSONObject json = jsonarray.getJSONObject(i);
                RekapMahasiswa s = new RekapMahasiswa();

                s.setKodeProdi(json.getString(KEY_KodeProdi));
                s.setNamaProdi(json.getString(KEY_NamaProdi));
                s.setA(Integer.parseInt(json.getString(KEY_A)));
                s.setN(Integer.parseInt(json.getString(KEY_N)));
                s.setT(Integer.parseInt(json.getString(KEY_T)));
                s.setC(Integer.parseInt(json.getString(KEY_C)));
                s.setD(Integer.parseInt(json.getString(KEY_D)));
                s.setK(Integer.parseInt(json.getString(KEY_K)));
                s.setL(Integer.parseInt(json.getString(KEY_L)));
                result.add(s);

            }
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
