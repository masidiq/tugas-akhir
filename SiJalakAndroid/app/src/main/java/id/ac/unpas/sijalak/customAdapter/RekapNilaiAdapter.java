package id.ac.unpas.sijalak.customAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.ac.unpas.sijalak.R;
import id.ac.unpas.sijalak.model.RekapMahasiswa;
import id.ac.unpas.sijalak.model.RekapNilai;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class RekapNilaiAdapter extends ArrayAdapter<RekapNilai> {
    private final Context context;
    private final List<RekapNilai> listValue;

    public RekapNilaiAdapter(Context context, List<RekapNilai> listValue) {
        super(context,-1, listValue);
        this.context = context;
        this.listValue = listValue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_rekap_nilai, parent, false);

        RekapNilai o = listValue.get(position);
        ((TextView) rowView.findViewById(R.id.txtNo)).setText(Integer.toString(position + 1));
        ((TextView) rowView.findViewById(R.id.txtProdi)).setText(o.getKodeProdi() + "\n" + o.getNamaProdi());
        ((TextView) rowView.findViewById(R.id.txtBs)).setText(o.getBs());
        ((TextView) rowView.findViewById(R.id.txtBe)).setText(o.getBe());
        ((TextView) rowView.findViewById(R.id.txtSe)).setText(o.getSe());
        return rowView;
    }
}
