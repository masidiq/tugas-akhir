package id.ac.unpas.sijalak.customAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import id.ac.unpas.sijalak.R;
import id.ac.unpas.sijalak.helper.ThemeManager;
import id.ac.unpas.sijalak.model.JadwalKuliah;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Sidiq on 3/7/2016.
 */
public class JadwalKuliahAdapter extends ArrayAdapter<JadwalKuliah> implements StickyListHeadersAdapter {
    private final Context context;
    private final List<JadwalKuliah> listValue;

    public JadwalKuliahAdapter(Context context, List<JadwalKuliah> listValue) {
        super(context,-1, listValue);
        this.context = context;
        this.listValue = listValue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_jadwal_kuliah, parent, false);

        ((TextView) rowView.findViewById(R.id.txtMataKuliah)).setText(listValue.get(position).getMataKuliah());
        ((TextView) rowView.findViewById(R.id.txtRuang)).setText(listValue.get(position).getRuang());
        ((TextView) rowView.findViewById(R.id.txtKelas)).setText("Kelas " + listValue.get(position).getKelas());
        ((TextView) rowView.findViewById(R.id.txtDosen)).setText(listValue.get(position).getDosen());
        ((TextView) rowView.findViewById(R.id.txtJam)).setText(listValue.get(position).getJam());
        ((TextView) rowView.findViewById(R.id.txtSks)).setText(listValue.get(position).getSks()+ " sks");
        return rowView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView =inflater.inflate(R.layout.sticky_header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.header = (LinearLayout) convertView.findViewById(R.id.header);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        //set header text as first char in name
        String headerText = listValue.get(position).getHari();
        holder.text.setText(headerText);
        holder.header.setBackgroundColor(ThemeManager.getColorPrimary(getContext()));
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return listValue.get(position).getIdHari();
    }

    @Override
    public JadwalKuliah getItem(int position) {
        return listValue.get(position);
    }

    class HeaderViewHolder {
        TextView text;
        LinearLayout header;
    }
}
