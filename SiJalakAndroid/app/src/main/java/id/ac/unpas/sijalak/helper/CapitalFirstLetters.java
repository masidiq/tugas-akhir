package id.ac.unpas.sijalak.helper;

/**
 * Created by Sidiq on 3/14/2016.
 */
public class CapitalFirstLetters {

    public static String toCapital(String word) {
        String firstLetter = word.substring(0, 1);
        if(word.substring(0, 1).equals(" ")){
            word = word.substring(1);
        }
        word = word.toLowerCase();
        String[] wordSplit;
        String wordCapital = "";
        wordSplit = word.split(" ");
        for (int i = 0; i < wordSplit.length; i++) {
            wordCapital += wordSplit[i].substring(0, 1).toUpperCase() + wordSplit[i].substring(1);
            if (i != wordSplit.length -1)
                wordCapital += " ";
        }
        return wordCapital;
    }
}