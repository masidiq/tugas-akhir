package id.ac.unpas.sijalak.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import id.ac.unpas.sijalak.model.StatusPerwalian;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class StatusPerwalianService {
    public static String endPoint = "dosen/status_perwalian";
    private static final String KEY_NamaMhs = "nama_mhs";
    private static final String KEY_Npm = "npm";
    private static final String KEY_Program = "program";
    private static final String KEY_Angkatan = "angkatan";
    private static final String KEY_TotalSks = "total_sks";
    private static final String KEY_Ipk = "ipk";
    private static final String KEY_Status = "status";
    private static final String KEY_TglPerwalian = "tgl_perwalian";
    private static final String KEY_JmlMk = "jml_mk";
    private static final String KEY_JmlSks = "jml_sks";
    private static final String KEY_SksTunda = "sks_tunda";

    public static List<StatusPerwalian> parserJsonResult(String jsonStr){
        try {

            List<StatusPerwalian> result = new ArrayList<>();
            if (jsonStr == null)
                return result;

            JSONArray jsonarray = new JSONArray(jsonStr);

            for (int i=0; i<jsonarray.length(); i++){
                JSONObject json = jsonarray.getJSONObject(i);
                StatusPerwalian s = new StatusPerwalian();

                s.setNpm(json.getString(KEY_Npm));
                s.setAngkatan(json.getString(KEY_Angkatan));
                s.setIpk(json.getString(KEY_Ipk));
                s.setJmlMk(json.getString(KEY_JmlMk));
                s.setMahasiswa(json.getString(KEY_NamaMhs));
                s.setProgram(json.getString(KEY_Program));
                s.setTotalSks(json.getString(KEY_TotalSks));
                s.setStatus(json.getString(KEY_Status));

                String dateStr = json.getString(KEY_TglPerwalian);
                if (dateStr.equals("0000-00-00") || dateStr.equals("") || dateStr.equals("null")){

                }else {
                    s.setTglPerwalian(Date.valueOf(dateStr));
                }

                s.setJmlSks(json.getString(KEY_JmlSks));
                s.setSksTunda(json.getString(KEY_SksTunda));

                result.add(s);

            }

            Collections.sort(result, StatusPerwalian.NpmAsc);
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
