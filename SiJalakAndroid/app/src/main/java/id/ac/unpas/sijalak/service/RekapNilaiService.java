package id.ac.unpas.sijalak.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.ac.unpas.sijalak.model.RekapMahasiswa;
import id.ac.unpas.sijalak.model.RekapNilai;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class RekapNilaiService {
    public static String endPoint = "pimpinan/rekap_nilai";


    public static List<RekapNilai> parserJsonResult(String jsonStr){
        try {

            List<RekapNilai> result = new ArrayList<>();
            if (jsonStr == null)
                return result;

            JSONArray jsonarray = new JSONArray(jsonStr);

            for (int i=0; i<jsonarray.length(); i++){
                JSONObject json = jsonarray.getJSONObject(i);
                RekapNilai s = new RekapNilai();

                s.setKodeProdi(json.getString("kode_prodi"));
                s.setNamaProdi(json.getString("nama_prodi"));
                s.setBe(json.getString("be"));
                s.setBs(json.getString("bs"));
                s.setSe(json.getString("se"));
                result.add(s);

            }
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
