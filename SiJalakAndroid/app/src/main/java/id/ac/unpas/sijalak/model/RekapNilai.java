package id.ac.unpas.sijalak.model;

import java.io.Serializable;

/**
 * Created by Sidiq on 19-Oct-16.
 */
public class RekapNilai implements Serializable {
    private String kodeProdi;
    private String namaProdi;
    private String bs;
    private String be;
    private String se;

    public String getKodeProdi() {
        return kodeProdi;
    }

    public void setKodeProdi(String kodeProdi) {
        this.kodeProdi = kodeProdi;
    }

    public String getNamaProdi() {
        return namaProdi;
    }

    public void setNamaProdi(String namaProdi) {
        this.namaProdi = namaProdi;
    }

    public String getBs() {
        return bs;
    }

    public void setBs(String bs) {
        this.bs = bs;
    }

    public String getBe() {
        return be;
    }

    public void setBe(String be) {
        this.be = be;
    }

    public String getSe() {
        return se;
    }

    public void setSe(String se) {
        this.se = se;
    }
}
