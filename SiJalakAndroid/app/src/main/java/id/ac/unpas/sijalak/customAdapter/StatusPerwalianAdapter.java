package id.ac.unpas.sijalak.customAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.ac.unpas.sijalak.R;
import id.ac.unpas.sijalak.helper.EnumHari;
import id.ac.unpas.sijalak.model.StatusPerwalian;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class StatusPerwalianAdapter extends ArrayAdapter<StatusPerwalian> {
    private final Context context;
    private final List<StatusPerwalian> listValue;

    public StatusPerwalianAdapter(Context context, List<StatusPerwalian> listValue) {
        super(context,-1, listValue);
        this.context = context;
        this.listValue = listValue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_status_perwalian, parent, false);
        rowView.setBackgroundColor(getBgColor(context, listValue.get(position)));

        TextView txtNo = ((TextView) rowView.findViewById(R.id.txtNo));
        TextView txtMhs = ((TextView) rowView.findViewById(R.id.txtMahasiswa));
        TextView txtSmt = ((TextView) rowView.findViewById(R.id.txtJmlMk));
        TextView txtTotal = ((TextView) rowView.findViewById(R.id.txtJmlSks));
        TextView txtTanggal = ((TextView) rowView.findViewById(R.id.txtTgl));

        txtNo.setText(Integer.toString(position + 1));
        txtMhs.setText(listValue.get(position).getNpm() +"\n" +   listValue.get(position).getMahasiswa());
        txtSmt.setText("MK:" + listValue.get(position).getJmlMk() +"\nSKS:" + listValue.get(position).getJmlSks());
        txtTotal.setText("SKS:" + listValue.get(position).getTotalSks() +"\nIPK:" + listValue.get(position).getIpk());
        txtTanggal.setText(EnumHari.dateToShortString(listValue.get(position).getTglPerwalian()));


        txtNo.setTextColor(getTextColor(context,listValue.get(position)));
        txtMhs.setTextColor(getTextColor(context,listValue.get(position)));
        txtSmt.setTextColor(getTextColor(context,listValue.get(position)));
        txtTotal.setTextColor(getTextColor(context,listValue.get(position)));
        txtTanggal.setTextColor(getTextColor(context,listValue.get(position)));
        return rowView;
    }

    private int getBgColor(Context context,StatusPerwalian statusPerwalian){

        if (statusPerwalian.getStatus().toLowerCase().equals("aktif") &&
                statusPerwalian.getTglPerwalian() == null &&
                (statusPerwalian.getJmlMk().equals("0") || statusPerwalian.getJmlMk().isEmpty())){
            return context.getResources().getColor(R.color.status_putih);
        }else if (statusPerwalian.getStatus().toLowerCase().equals("aktif") &&
                statusPerwalian.getTglPerwalian() == null &&
                !statusPerwalian.getJmlMk().equals("0")){
            return context.getResources().getColor(R.color.status_kuning);
        }
        else if (statusPerwalian.getStatus().toLowerCase().equals("aktif") &&
                statusPerwalian.getTglPerwalian() != null &&
                (!statusPerwalian.getJmlMk().equals("0")) ){
            return context.getResources().getColor(R.color.status_biru);
        }
        else if (!statusPerwalian.getStatus().toLowerCase().equals("aktif")){
            return context.getResources().getColor(R.color.status_merah);
        }
        else
            return context.getResources().getColor(R.color.status_putih);
    }


    private int getTextColor(Context context,StatusPerwalian statusPerwalian){

        if (statusPerwalian.getStatus().toLowerCase().equals("aktif") &&
                statusPerwalian.getTglPerwalian() == null &&
                !statusPerwalian.getJmlMk().equals("0")){
            return context.getResources().getColor(R.color.black);
        }
        else if (statusPerwalian.getStatus().toLowerCase().equals("aktif") &&
                statusPerwalian.getTglPerwalian() != null &&
                (!statusPerwalian.getJmlMk().equals("0")) ){
            return context.getResources().getColor(R.color.white);
        }
        else if (!statusPerwalian.getStatus().toLowerCase().equals("aktif")){
            return context.getResources().getColor(R.color.white);
        }
        else
            return context.getResources().getColor(R.color.black);
    }
}
