package id.ac.unpas.sijalak.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import id.ac.unpas.sijalak.model.JadwalUjian;

/**
 * Created by Sidiq on 3/15/2016.
 */
public class JadwalUjianService {
    public static String endPoint = "mahasiswa/jadwal_ujian";
    private static final String KEY_MataKuliah = "mata_kuliah";
    private static final String KEY_IdSemester = "id_semester";
    private static final String KEY_Tanggal = "tanggal";
    private static final String KEY_Jam = "jam";
    private static final String KEY_Ruang = "ruang";
    private static final String KEY_Kelas = "kelas";
    private static final String KEY_Sks = "sks";
    private static final String KEY_Dosen = "dosen";
    private static final String KEY_Presensi = "presensi";
    private static final String KEY_Kode = "kode_mk";


    public static List<JadwalUjian> parserJsonResult(String jsonStr){
        try {

            List<JadwalUjian> result = new ArrayList<>();
            if (jsonStr == null)
                return result;

            JSONArray jsonarray = new JSONArray(jsonStr);

            for (int i=0; i<jsonarray.length(); i++){
                JSONObject o = jsonarray.getJSONObject(i);


                    JadwalUjian j = new JadwalUjian();
                    j.setDosen(o.getString(KEY_Dosen));
                    j.setPresensi(o.getString(KEY_Presensi));
                    j.setSks(o.getString(KEY_Sks));
                    j.setKelas(o.getString(KEY_Kelas));
                    j.setJam(o.getString(KEY_Jam));
                    j.setKodeMk(o.getString(KEY_Kode));
                    j.setMataKuliah(o.getString(KEY_MataKuliah));
                    j.setRuang(o.getString(KEY_Ruang));
                    j.setTanggal(Date.valueOf(o.getString(KEY_Tanggal)));

                    j.generateIdWaktu();
                    result.add(j);

            }

            Collections.sort(result, JadwalUjian.JamAsc);


            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }



    }
}
