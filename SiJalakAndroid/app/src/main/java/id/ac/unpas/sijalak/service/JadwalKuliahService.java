package id.ac.unpas.sijalak.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import id.ac.unpas.sijalak.helper.EnumHari;
import id.ac.unpas.sijalak.model.JadwalKuliah;
import id.ac.unpas.sijalak.model.Kehadiran;

/**
 * Created by Sidiq on 3/7/2016.
 */
public class JadwalKuliahService {
    public static String endPoint = "mahasiswa/jadwal_kuliah";
    private static final String KEY_IdSemester = "id_semester";
    private static final String KEY_MataKuliah = "mata_kuliah";
    private static final String KEY_Hari = "hari";
    private static final String KEY_Jam = "jam";
    private static final String KEY_Ruang = "ruang";
    private static final String KEY_Kelas = "kelas";
    private static final String KEY_Sks = "sks";
    private static final String KEY_Dosen = "dosen";
    private static final String KEY_Presensi = "presensi";
    private static final String KEY_Kode = "kode_mk";


    private static final String KEY_KehadiranList = "kehadiran";
    private static final String KEY_Kehadiran_tanggal = "tanggal";
    private static final String KEY_Kehadiran_status = "status";
    private static final String KEY_Kehadiran_mulai = "mulai";
    private static final String KEY_Kehadiran_selesai = "selesai";

    public static List<JadwalKuliah> parserJsonResult(String jsonStr){
        try {

            List<JadwalKuliah> result = new ArrayList<JadwalKuliah>();
            if (jsonStr == null)
                return result;

            JSONArray jsonarray = new JSONArray(jsonStr);

            for (int i=0; i<jsonarray.length(); i++){
                JSONObject o = jsonarray.getJSONObject(i);
                //ambil yg semester terakhir saja

                    JadwalKuliah j = new JadwalKuliah();
                    j.setDosen(o.getString(KEY_Dosen));
                    j.setPresensi(o.getString(KEY_Presensi));
                    j.setSks(o.getString(KEY_Sks));
                    j.setKelas(o.getString(KEY_Kelas));
                    j.setHari(o.getString(KEY_Hari));
                    j.setJam(o.getString(KEY_Jam));
                    j.setKode(o.getString(KEY_Kode));
                    j.setMataKuliah(o.getString(KEY_MataKuliah));
                    j.setRuang(o.getString(KEY_Ruang));
                    j.setIdSemester(o.getString(KEY_IdSemester));
                    j.setIdHari(EnumHari.getId(o.getString(KEY_Hari)));


                    // KEHADIRAN
                    JSONArray kehadiranJson = o.getJSONArray(KEY_KehadiranList);
                    if (kehadiranJson != null) {
                        for (int i2 = 0; i2 < kehadiranJson.length(); i2++) {
                            JSONObject o2 = kehadiranJson.getJSONObject(i2);
                            Kehadiran s = new Kehadiran();
                            s.setTanggal(Date.valueOf(o2.getString(KEY_Kehadiran_tanggal)));
                            s.setStatus(o2.getString(KEY_Kehadiran_status));
                            s.setMulai(o2.getString(KEY_Kehadiran_mulai));
                            s.setSelesai(o2.getString(KEY_Kehadiran_selesai));
                            j.getKehadiranList().add(s);
                        }
                    }
                    result.add(j);

            }

            Collections.sort(result, JadwalKuliah.JamAsc);


            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }



    }
}
