package id.ac.unpas.sijalak.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class StatusPerwalian implements Serializable{
    private String mahasiswa;
    private String npm;
    private String program;
    private String angkatan;
    private String totalSks;
    private String ipk;
    private String status;
    private Date tglPerwalian;
    private String jmlMk;
    private String jmlSks;
    private String sksTunda;


    public String getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(String mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getAngkatan() {
        return angkatan;
    }

    public void setAngkatan(String angkatan) {
        this.angkatan = angkatan;
    }

    public String getTotalSks() {
        return totalSks;
    }

    public void setTotalSks(String totalSks) {
        this.totalSks = totalSks;
    }

    public String getIpk() {
        return ipk;
    }

    public void setIpk(String ipk) {
        this.ipk = ipk;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getTglPerwalian() {
        return tglPerwalian;
    }

    public void setTglPerwalian(Date tglPerwalian) {
        this.tglPerwalian = tglPerwalian;
    }

    public String getJmlMk() {
        return jmlMk;
    }

    public void setJmlMk(String jmlMk) {
        this.jmlMk = jmlMk;
    }

    public String getJmlSks() {
        return jmlSks;
    }

    public void setJmlSks(String jmlSks) {
        this.jmlSks = jmlSks;
    }

    public String getSksTunda() {
        return sksTunda;
    }

    public void setSksTunda(String sksTunda) {
        this.sksTunda = sksTunda;
    }

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public static Comparator<StatusPerwalian> NpmAsc = new Comparator<StatusPerwalian>() {
        public int compare(StatusPerwalian s1, StatusPerwalian s2) {
            String str1 = s1.getNpm();
            String str2 = s2.getNpm();
            return str1.compareTo(str2);
        }
    };
}
