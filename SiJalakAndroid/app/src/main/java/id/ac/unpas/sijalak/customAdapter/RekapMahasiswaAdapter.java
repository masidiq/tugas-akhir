package id.ac.unpas.sijalak.customAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.ac.unpas.sijalak.R;
import id.ac.unpas.sijalak.helper.EnumHari;
import id.ac.unpas.sijalak.model.RekapMahasiswa;
import id.ac.unpas.sijalak.model.StatusPerwalian;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class RekapMahasiswaAdapter extends ArrayAdapter<RekapMahasiswa> {
    private final Context context;
    private final List<RekapMahasiswa> listValue;

    public RekapMahasiswaAdapter(Context context, List<RekapMahasiswa> listValue) {
        super(context,-1, listValue);
        this.context = context;
        this.listValue = listValue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_rekap_mahasiswa, parent, false);

         RekapMahasiswa o = listValue.get(position);
        ((TextView) rowView.findViewById(R.id.txtNo)).setText(Integer.toString(position + 1));
        ((TextView) rowView.findViewById(R.id.txtProdi)).setText(o.getKodeProdi() + "\n" + o.getNamaProdi());
        ((TextView) rowView.findViewById(R.id.txtAT)).setText("A:"+o.getA() + "\nT:" + o.getT());
        ((TextView) rowView.findViewById(R.id.txtCN)).setText("C:"+o.getC() + "\nN:" + o.getN());
        ((TextView) rowView.findViewById(R.id.txtDKL)).setText("D:" + o.getD() + "\nK:" + o.getK() + "\nL:" + o.getL());
        return rowView;
    }
}
