package id.ac.unpas.sijalak.customAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.ac.unpas.sijalak.R;
import id.ac.unpas.sijalak.model.RekapKuesioner;
import id.ac.unpas.sijalak.model.RekapKuesioner;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class RekapKuesionerAdapter extends ArrayAdapter<RekapKuesioner> {
    private final Context context;
    private final List<RekapKuesioner> listValue;

    public RekapKuesionerAdapter(Context context, List<RekapKuesioner> listValue) {
        super(context,-1, listValue);
        this.context = context;
        this.listValue = listValue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_rekap_kuesioner, parent, false);

         RekapKuesioner o = listValue.get(position);
        ((TextView) rowView.findViewById(R.id.txtNo)).setText(Integer.toString(position + 1));
        ((TextView) rowView.findViewById(R.id.txtProdi)).setText(o.getKodeProdi() + "\n" + o.getNamaProdi());
        ((TextView) rowView.findViewById(R.id.txtMin)).setText(o.getMin());
        ((TextView) rowView.findViewById(R.id.txtMax)).setText(o.getMax());
        ((TextView) rowView.findViewById(R.id.txtAvg)).setText(o.getAvg());
        return rowView;
    }
}
