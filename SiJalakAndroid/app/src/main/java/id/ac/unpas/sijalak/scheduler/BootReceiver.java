package id.ac.unpas.sijalak.scheduler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import id.ac.unpas.sijalak.MainData;

/**
 * Created by Sidiq on 3/21/2016.
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
            MainData.getInstance().load(context);
            AlarmReceiver.setScheduleJadwalKuliah(context);
            AlarmReceiver.setScheduleJadwalMengajar(context);
            AlarmReceiver.setScheduleJadwalUjian(context);
        }
    }
}
