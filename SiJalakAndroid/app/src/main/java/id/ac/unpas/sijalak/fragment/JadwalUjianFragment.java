package id.ac.unpas.sijalak.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.ac.unpas.sijalak.MainActivity;
import id.ac.unpas.sijalak.MainData;
import id.ac.unpas.sijalak.R;
import id.ac.unpas.sijalak.customAdapter.JadwalUjianAdapter;
import id.ac.unpas.sijalak.helper.EnumHari;
import id.ac.unpas.sijalak.model.JadwalUjian;
import id.ac.unpas.sijalak.scheduler.AlarmReceiver;
import id.ac.unpas.sijalak.service.JadwalUjianService;
import id.ac.unpas.sijalak.utils.AlertHelper;
import id.ac.unpas.sijalak.utils.OnTaskListener;
import id.ac.unpas.sijalak.utils.RequestTask;
import id.ac.unpas.sijalak.utils.ResponseData;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link JadwalUjianFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link JadwalUjianFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JadwalUjianFragment extends Fragment implements OnTaskListener {
    private MainActivity activity;
    private StickyListHeadersListView mList;
    private JadwalUjianAdapter mAdapter;
    List<JadwalUjian> jdwl;
    private TextView txtLastUpdate;

    private OnFragmentInteractionListener mListener;



    public static JadwalUjianFragment newInstance(String param1, String param2) {
        JadwalUjianFragment fragment = new JadwalUjianFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    public JadwalUjianFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_jadwal_ujian, container, false);

        // Init
        initUI(view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.activity = (MainActivity)activity;
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    public void requestToServer(){
        RequestTask task = new RequestTask(this);
        task.execute(JadwalUjianService.endPoint);
    }
    private void initUI(View view){
        //txtResponse = (TextView) view.findViewById(R.id.txtResponse);
        jdwl = new ArrayList<>();
        mList = (StickyListHeadersListView) view.findViewById(R.id.list);
        txtLastUpdate = (TextView)view.findViewById(R.id.txtLastUpdate);
        setValueToUI();
    }

    public void setValueToUI(){
        if (MainData.getInstance().getJadwalUjianList() == null){
            requestToServer();
            return;
        }
        jdwl = MainData.getInstance().getJadwalUjianList();
        if(!jdwl.isEmpty()) {
            refreshListView();

        }
    }

    @Override
    public void onTaskCompleted(ResponseData response) {
        this.activity.hideLoading();

        if (response.getStatus() == HttpURLConnection.HTTP_OK){
            jdwl = JadwalUjianService.parserJsonResult(response.getResponseString());
            if (jdwl != null && jdwl.size() > 0){
                //delete alarm
                AlarmReceiver.cancelJadwalUjian(activity);

                MainData.getInstance().setJadwalUjianList(jdwl);
                MainData.getInstance().setJadwalUjianLastUpdate(new Date());
                MainData.getInstance().save(this.activity);
                AlarmReceiver.setScheduleJadwalUjian(activity);

                refreshListView();
                activity.showLoadSuccessToast();
            }
            return;
        }else if (response.getStatus() == HttpURLConnection.HTTP_UNAUTHORIZED){
            activity.showAlertToken();
        }else {
            AlertHelper.ShowAlert(activity, activity.getString(R.string.load_failed), response.getResponseString());
        }
    }

    private void refreshListView(){
        mAdapter = new JadwalUjianAdapter(getActivity(), jdwl);
        mList.setAdapter(mAdapter);
        txtLastUpdate.setText(EnumHari.generateLastUpdate(MainData.getInstance().getJadwalUjianLastUpdate()));
    }

    @Override
    public void onTaskStarted() {
        this.activity.showLoading();
    }

}
