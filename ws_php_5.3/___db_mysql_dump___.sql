/*
Navicat MySQL Data Transfer

Source Server         : local sidiq
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : ws

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2016-03-23 09:22:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for hasil_kuisioner
-- ----------------------------
DROP TABLE IF EXISTS `hasil_kuisioner`;
CREATE TABLE `hasil_kuisioner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mata_kuliah` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `prodi` varchar(255) DEFAULT NULL,
  `semester` varchar(255) DEFAULT NULL,
  `sks` varchar(255) DEFAULT NULL,
  `peserta` varchar(255) DEFAULT NULL,
  `responden` varchar(255) DEFAULT NULL,
  `nilai` varchar(255) DEFAULT NULL,
  `rata_rata` varchar(255) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hasil_kuisioner
-- ----------------------------
INSERT INTO `hasil_kuisioner` VALUES ('1', 'SISTEM BASISDATA I', 'A', '302', '20152', '3', '50', '49', '460', '3.13', '3');
INSERT INTO `hasil_kuisioner` VALUES ('2', 'SISTEM BASISDATA II', 'A', '302', '20152', '3', '46', '45', '367', '3.07', '3');
INSERT INTO `hasil_kuisioner` VALUES ('3', 'PEMROGRAMAN BASISDATA', 'A', '302', '20152', '3', '45', '41', '445', '2.99', '3');
INSERT INTO `hasil_kuisioner` VALUES ('4', 'PEMODELAN PROSES BISNIS', 'A', '302', '20152', '3', '52', '50', '364', '2.98', '3');
INSERT INTO `hasil_kuisioner` VALUES ('5', 'PERAWATAN & PENGUJIAN PERANGKAT LUNAK', 'A', '302', '20152', '3', '48', '44', '456', '3.15', '4');
INSERT INTO `hasil_kuisioner` VALUES ('6', 'KONSTRUKSI P/L BERORIENTASI OBJEK', 'A', '302', '20152', '3', '47', '44', '440', '3.13', '4');

-- ----------------------------
-- Table structure for jadwal_kuliah
-- ----------------------------
DROP TABLE IF EXISTS `jadwal_kuliah`;
CREATE TABLE `jadwal_kuliah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_semester` int(11) DEFAULT NULL,
  `id_mhs` varchar(255) NOT NULL,
  `hari` varchar(255) DEFAULT NULL,
  `jam` varchar(255) DEFAULT NULL,
  `kode_mk` varchar(255) DEFAULT NULL,
  `mata_kuliah` varchar(255) DEFAULT NULL,
  `ruang` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `sks` varchar(255) DEFAULT NULL,
  `dosen` varchar(255) DEFAULT NULL,
  `presensi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jadwal_kuliah
-- ----------------------------
INSERT INTO `jadwal_kuliah` VALUES ('1', '20152', '1', 'Senin', '07:00-09:30', 'IT507', 'REKAYASA WEB', 'SB405', 'A', '3', 'FERRY MULYANTO, ST., M.KOM,', '5/8');
INSERT INTO `jadwal_kuliah` VALUES ('2', '20152', '1', 'Senin', '10:00-12:30', 'IT506', 'INTERAKSI MANUSIA & KOMPUTER', 'SB401', 'A', '3', 'WANDA GUSDYA, ST., MT., ST, MT.', '5/8');
INSERT INTO `jadwal_kuliah` VALUES ('3', '20152', '1', 'Senin', '13:00-15:30', 'IT742', 'SISTEM INFORMASI GEOGRAFIS', 'SB405', 'A', '3', 'SALI ALAS MAJAPAHIT, S.ST., M.KOM.,', '5/8');
INSERT INTO `jadwal_kuliah` VALUES ('4', '20152', '1', 'Selasa', '10:00-12:30', 'IT744', 'MANAJEMEN ASET DIGITAL', 'SB403', 'A', '3', 'MELLIA LIYANTHY, ST., MT.,', '5/8');
INSERT INTO `jadwal_kuliah` VALUES ('5', '20152', '1', 'Rabu', '13:00-14:40', 'IT302', 'ILMU BUDAYA SUNDA', 'SB301', 'C', '2', 'RIO TEGUH ARIF, S.Pd., M.Pd,', '5/8');
INSERT INTO `jadwal_kuliah` VALUES ('6', '20152', '1', 'Kamis', '10:00-12:30', 'IT501', 'SISTEM MANAJEMEN BASIS DATA', 'SB404', 'A', '3', 'CACA EMILE SUPRIANA, S.SI., MT.,', '5/8');
INSERT INTO `jadwal_kuliah` VALUES ('7', '20152', '1', 'Jumat', '07:00-09:30', 'IT734', 'PROYEK APLIKASI MULTIMEDIA', 'P1401', 'A', '3', 'MELLIA LIYANTHY, ST., MT.,', '5/8');
INSERT INTO `jadwal_kuliah` VALUES ('8', '20152', '1', 'Jumat', '13:00-15:30', 'IT105', 'MATEMATIKA DASAR', 'SB405', 'A', '3', 'DRS. ANWAR GHOZIE,', '5/8');
INSERT INTO `jadwal_kuliah` VALUES ('13', '20152', '2', 'Senin', '07:00-09:30', 'IT507', 'REKAYASA WEB', 'SB405', 'A', '3', 'FERRY MULYANTO, ST., M.KOM,', '5/8');
INSERT INTO `jadwal_kuliah` VALUES ('14', '20152', '2', 'Senin', '10:00-12:30', 'IT506', 'INTERAKSI MANUSIA & KOMPUTER', 'SB401', 'A', '3', 'WANDA GUSDYA, ST., MT., ST, MT.', '5/8');

-- ----------------------------
-- Table structure for jadwal_mengajar
-- ----------------------------
DROP TABLE IF EXISTS `jadwal_mengajar`;
CREATE TABLE `jadwal_mengajar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_dosen` varchar(255) NOT NULL,
  `mata_kuliah` varchar(255) DEFAULT NULL,
  `hari` varchar(255) DEFAULT NULL,
  `jam` varchar(255) DEFAULT NULL,
  `ruang` varchar(255) DEFAULT NULL,
  `kode_mk` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `sks` varchar(255) DEFAULT NULL,
  `presensi` varchar(255) DEFAULT NULL,
  `id_semester` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of jadwal_mengajar
-- ----------------------------
INSERT INTO `jadwal_mengajar` VALUES ('1', '3', 'SISTEM BASISDATA I', 'Senin', '13:00-15:30', 'SB119', 'IT206', 'D', '3', '6', '20152');
INSERT INTO `jadwal_mengajar` VALUES ('2', '3', 'PEMROGRAMAN BASISDATA', 'Selasa', '13:00-15:30', 'SB119', 'IT602', 'A', '3', '5', '20152');
INSERT INTO `jadwal_mengajar` VALUES ('3', '4', 'KONSTRUKSI P/L BERORIENTASI OBJEK', 'Senin', '10:40-12:20', 'SB403', 'IT405', 'A', '3', '6', '20152');
INSERT INTO `jadwal_mengajar` VALUES ('4', '4', 'PERAWATAN & PENGUJIAN PERANGKAT LUNAK', 'Senin', '13:00-14:40', 'SB405', 'IT852', 'A', '2', '6', '20152');
INSERT INTO `jadwal_mengajar` VALUES ('5', '4', 'KONSTRUKSI P/L BERORIENTASI OBJEK', 'Senin', '14:50-16:30', 'SB403', 'IT405', 'A', '3', '6', '20152');
INSERT INTO `jadwal_mengajar` VALUES ('6', '4', 'KONSTRUKSI P/L BERORIENTASI OBJEK', 'Selasa', '07:00-10:20', 'SB403', 'IT405', 'B', '3', '5', '20152');

-- ----------------------------
-- Table structure for jadwal_ujian
-- ----------------------------
DROP TABLE IF EXISTS `jadwal_ujian`;
CREATE TABLE `jadwal_ujian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mhs` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jam` varchar(255) DEFAULT NULL,
  `mata_kuliah` varchar(255) DEFAULT NULL,
  `ruang` varchar(255) DEFAULT NULL,
  `kode_mk` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `sks` varchar(255) DEFAULT NULL,
  `dosen` varchar(255) DEFAULT NULL,
  `presensi` varchar(255) DEFAULT NULL,
  `id_semester` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jadwal_ujian
-- ----------------------------
INSERT INTO `jadwal_ujian` VALUES ('1', '1', '2016-04-04', '07:15-09:00', 'REKAYASA WEB', 'SB405', 'IT507', 'A', '3', 'FERRY MULYANTO, ST., M.KOM,', '5/8', '20152');
INSERT INTO `jadwal_ujian` VALUES ('2', '1', '2016-04-04', '09:15-10:45', 'INTERAKSI MANUSIA & KOMPUTER', 'SB401', 'IT506', 'A', '3', 'WANDA GUSDYA, ST., MT., ST, MT.', '5/8', '20152');
INSERT INTO `jadwal_ujian` VALUES ('3', '1', '2016-04-05', '07:15-09:00', 'SISTEM INFORMASI GEOGRAFIS', 'SB405', 'IT742', 'A', '3', 'SALI ALAS MAJAPAHIT, S.ST., M.KOM.,', '5/8', '20152');
INSERT INTO `jadwal_ujian` VALUES ('4', '1', '2016-04-05', '09:15-10:45', 'MANAJEMEN ASET DIGITAL', 'SB403', 'IT744', 'A', '3', 'MELLIA LIYANTHY, ST., MT.,', '5/8', '20152');
INSERT INTO `jadwal_ujian` VALUES ('5', '1', '2016-04-06', '07:15-09:00', 'ILMU BUDAYA SUNDA', 'SB301', 'IT302', 'C', '2', 'RIO TEGUH ARIF, S.Pd., M.Pd,', '5/8', '20152');
INSERT INTO `jadwal_ujian` VALUES ('6', '1', '2016-04-06', '09:15-10:45', 'SISTEM MANAJEMEN BASIS DATA', 'SB404', 'IT501', 'A', '3', 'CACA EMILE SUPRIANA, S.SI., MT.,', '5/8', '20152');
INSERT INTO `jadwal_ujian` VALUES ('7', '1', '2016-04-07', '07:15-09:00', 'PROYEK APLIKASI MULTIMEDIA', 'P1401', 'IT734', 'A', '3', 'MELLIA LIYANTHY, ST., MT.,', '5/8', '20152');
INSERT INTO `jadwal_ujian` VALUES ('8', '1', '2016-04-07', '09:15-10:45', 'MATEMATIKA DASAR', 'SB405', 'IT105', 'A', '3', 'DRS. ANWAR GHOZIE,', '5/8', '20152');
INSERT INTO `jadwal_ujian` VALUES ('11', '2', '2016-04-04', '07:15-09:00', 'SISTEM INFORMASI GEOGRAFIS', 'SB405', 'IT507', 'A', '3', 'FERRY MULYANTO, ST., M.KOM,', '5/8', '20152');
INSERT INTO `jadwal_ujian` VALUES ('12', '2', '2016-04-04', '09:15-10:45', 'MANAJEMEN ASET DIGITAL', 'SB401', 'IT506', 'A', '3', 'WANDA GUSDYA, ST., MT., ST, MT.', '5/8', '20152');

-- ----------------------------
-- Table structure for kehadiran_dosen
-- ----------------------------
DROP TABLE IF EXISTS `kehadiran_dosen`;
CREATE TABLE `kehadiran_dosen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jadwal` int(11) DEFAULT NULL,
  `tanggal` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `mulai` varchar(255) DEFAULT NULL,
  `selesai` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kehadiran_dosen
-- ----------------------------
INSERT INTO `kehadiran_dosen` VALUES ('1', '1', '2016-2-8', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_dosen` VALUES ('2', '1', '2016-2-15', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_dosen` VALUES ('3', '1', '2016-2-22', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_dosen` VALUES ('4', '1', '2016-2-29', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_dosen` VALUES ('5', '1', '2016-3-7', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_dosen` VALUES ('6', '1', '2016-3-14', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_dosen` VALUES ('7', '2', '2016-2-9', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_dosen` VALUES ('8', '2', '2016-2-16', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_dosen` VALUES ('9', '2', '2016-2-23', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_dosen` VALUES ('10', '2', '2016-3-1', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_dosen` VALUES ('11', '2', '2016-3-8', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_dosen` VALUES ('24', '3', '2016-2-8', 'Hadir', '10:40', '12:20');
INSERT INTO `kehadiran_dosen` VALUES ('25', '3', '2016-2-15', 'Hadir', '10:40', '12:20');
INSERT INTO `kehadiran_dosen` VALUES ('26', '3', '2016-2-22', 'Hadir', '10:40', '12:20');
INSERT INTO `kehadiran_dosen` VALUES ('27', '3', '2016-2-29', 'Hadir', '10:40', '12:20');
INSERT INTO `kehadiran_dosen` VALUES ('28', '3', '2016-3-7', 'Hadir', '10:40', '12:20');
INSERT INTO `kehadiran_dosen` VALUES ('29', '3', '2016-3-14', 'Hadir', '10:40', '12:20');
INSERT INTO `kehadiran_dosen` VALUES ('30', '4', '2016-2-8', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_dosen` VALUES ('31', '4', '2016-2-15', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_dosen` VALUES ('32', '4', '2016-2-22', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_dosen` VALUES ('33', '4', '2016-2-29', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_dosen` VALUES ('34', '4', '2016-3-7', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_dosen` VALUES ('35', '4', '2016-3-14', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_dosen` VALUES ('36', '5', '2016-2-8', 'Hadir', '14:50', '16:30');
INSERT INTO `kehadiran_dosen` VALUES ('37', '5', '2016-2-15', 'Hadir', '14:50', '16:30');
INSERT INTO `kehadiran_dosen` VALUES ('38', '5', '2016-2-22', 'Hadir', '14:50', '16:30');
INSERT INTO `kehadiran_dosen` VALUES ('39', '5', '2016-2-29', 'Hadir', '14:50', '16:30');
INSERT INTO `kehadiran_dosen` VALUES ('40', '5', '2016-3-7', 'Hadir', '14:50', '16:30');
INSERT INTO `kehadiran_dosen` VALUES ('41', '5', '2016-3-14', 'Hadir', '14:50', '16:30');
INSERT INTO `kehadiran_dosen` VALUES ('42', '6', '2016-2-9', 'Hadir', '07:00', '10:20');
INSERT INTO `kehadiran_dosen` VALUES ('43', '6', '2016-2-16', 'Hadir', '07:00', '10:20');
INSERT INTO `kehadiran_dosen` VALUES ('44', '6', '2016-2-23', 'Hadir', '07:00', '10:20');
INSERT INTO `kehadiran_dosen` VALUES ('45', '6', '2016-3-1', 'Hadir', '07:00', '10:20');
INSERT INTO `kehadiran_dosen` VALUES ('46', '6', '2016-3-8', 'Hadir', '07:00', '10:20');

-- ----------------------------
-- Table structure for kehadiran_mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `kehadiran_mahasiswa`;
CREATE TABLE `kehadiran_mahasiswa` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_jadwal` int(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `mulai` varchar(255) DEFAULT NULL,
  `selesai` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kehadiran_mahasiswa
-- ----------------------------
INSERT INTO `kehadiran_mahasiswa` VALUES ('8', '1', '2015-10-01', 'Hadir', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('9', '1', '2015-10-08', '-', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('10', '1', '2015-10-15', 'Hadir', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('11', '1', '2015-10-22', 'Hadir', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('12', '1', '2015-10-29', '-', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('13', '2', '2015-10-01', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('14', '2', '2015-10-08', '-', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('15', '2', '2015-10-15', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('16', '2', '2015-10-22', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('17', '2', '2015-10-29', '-', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('18', '3', '2015-10-01', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('19', '3', '2015-10-08', '-', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('20', '3', '2015-10-15', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('21', '3', '2015-10-22', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('22', '3', '2015-10-29', '-', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('23', '4', '2015-10-02', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('24', '4', '2015-10-09', '-', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('25', '4', '2015-10-16', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('26', '4', '2015-10-23', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('27', '4', '2015-11-03', '-', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('28', '4', '2015-11-10', '-', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('29', '4', '2015-11-17', '-', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('30', '4', '2015-11-24', '-', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('31', '5', '2015-10-02', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('32', '5', '2015-10-09', '-', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('33', '5', '2015-10-16', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('34', '5', '2015-10-23', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('35', '5', '2015-11-03', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('36', '5', '2015-11-10', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('37', '5', '2015-11-17', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('38', '5', '2015-11-24', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('39', '6', '2015-10-02', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('40', '6', '2015-10-09', '-', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('41', '6', '2015-10-16', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('42', '6', '2015-10-23', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('43', '6', '2015-11-03', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('44', '6', '2015-11-10', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('45', '6', '2015-11-17', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('46', '6', '2015-11-24', 'Hadir', '13:00', '14:40');
INSERT INTO `kehadiran_mahasiswa` VALUES ('47', '7', '2015-10-02', 'Hadir', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('48', '7', '2015-10-09', '-', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('49', '7', '2015-10-16', 'Hadir', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('50', '7', '2015-10-23', 'Hadir', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('51', '7', '2015-11-03', 'Hadir', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('52', '7', '2015-11-10', 'Hadir', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('53', '7', '2015-11-17', 'Hadir', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('54', '7', '2015-11-24', 'Hadir', '07:00', '09:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('55', '8', '2015-10-02', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('56', '8', '2015-10-09', '-', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('57', '8', '2015-10-16', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('58', '8', '2015-10-23', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('59', '8', '2015-11-03', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('60', '8', '2015-11-10', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('61', '8', '2015-11-17', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('62', '8', '2015-11-24', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('63', '9', '2015-10-02', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('64', '9', '2015-10-09', '-', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('65', '9', '2015-10-16', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('66', '9', '2015-10-23', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('67', '9', '2015-11-03', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('68', '9', '2015-11-10', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('69', '9', '2015-11-17', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('70', '9', '2015-11-24', 'Hadir', '10:00', '12:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('71', '10', '2015-10-02', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('72', '10', '2015-10-09', '-', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('73', '10', '2015-10-16', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('74', '10', '2015-10-23', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('75', '10', '2015-11-03', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('76', '10', '2015-11-10', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('77', '10', '2015-11-17', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('78', '10', '2015-11-24', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('87', '11', '2015-10-16', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('88', '11', '2015-10-25', '-', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('89', '11', '2015-11-01', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('90', '11', '2015-11-08', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('91', '12', '2015-10-16', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('92', '12', '2015-10-25', '-', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('93', '12', '2015-11-01', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('94', '12', '2015-11-08', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('95', '13', '2015-10-16', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('96', '13', '2015-10-25', '-', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('97', '13', '2015-11-01', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('98', '13', '2015-11-08', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('99', '14', '2015-10-16', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('100', '14', '2015-10-25', '-', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('101', '14', '2015-11-01', 'Hadir', '13:00', '15:30');
INSERT INTO `kehadiran_mahasiswa` VALUES ('102', '14', '2015-11-08', 'Hadir', '13:00', '15:30');

-- ----------------------------
-- Table structure for mahasiswa_semester
-- ----------------------------
DROP TABLE IF EXISTS `mahasiswa_semester`;
CREATE TABLE `mahasiswa_semester` (
  `id_mhs` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  PRIMARY KEY (`id_semester`,`id_mhs`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mahasiswa_semester
-- ----------------------------
INSERT INTO `mahasiswa_semester` VALUES ('1', '20151');
INSERT INTO `mahasiswa_semester` VALUES ('2', '20151');
INSERT INTO `mahasiswa_semester` VALUES ('1', '20152');
INSERT INTO `mahasiswa_semester` VALUES ('2', '20152');

-- ----------------------------
-- Table structure for nilai
-- ----------------------------
DROP TABLE IF EXISTS `nilai`;
CREATE TABLE `nilai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mhs` int(11) DEFAULT NULL,
  `mata_kuliah` varchar(255) DEFAULT NULL,
  `semester` varchar(255) DEFAULT NULL,
  `kode_mk` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `sks` int(11) DEFAULT NULL,
  `tugas1` float DEFAULT NULL,
  `tugas2` float DEFAULT NULL,
  `tugas3` float DEFAULT NULL,
  `tugas4` float DEFAULT NULL,
  `tugas5` float DEFAULT NULL,
  `presensi` float DEFAULT NULL,
  `uts` float DEFAULT NULL,
  `uas` float DEFAULT NULL,
  `akhir` float DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `bobot` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nilai
-- ----------------------------
INSERT INTO `nilai` VALUES ('1', '1', 'REKAYASA WEB', '20152', 'IT507', 'A', '2', '80', '80', '70', '80', '80', '100', '80', '80', '80', 'A', '4');
INSERT INTO `nilai` VALUES ('2', '1', 'INTERAKSI MANUSIA & KOMPUTER', '20152', 'IT506', 'A', '3', '70', '80', '80', '80', '90', '100', '60', '70', '65', 'B', '1');
INSERT INTO `nilai` VALUES ('3', '1', 'SISTEM INFORMASI GEOGRAFIS', '20152', 'IT742', 'A', '3', '0', '0', '0', '0', '0', '100', '65', '65', '65', 'B', '3');
INSERT INTO `nilai` VALUES ('4', '1', 'MANAJEMEN ASET DIGITAL', '20152', 'IT744', 'A', '3', '0', '0', '0', '0', '0', '100', '90', '80', '85', 'A', '4');
INSERT INTO `nilai` VALUES ('5', '1', 'ILMU BUDAYA SUNDA', '20152', 'IT302', 'A', '3', '0', '0', '0', '0', '0', '100', '90', '80', '85', 'A', '4');
INSERT INTO `nilai` VALUES ('6', '1', 'SISTEM MANAJEMEN BASIS DATA', '20152', 'IT501', 'A', '3', '0', '0', '0', '0', '0', '100', '90', '80', '85', 'A', '4');
INSERT INTO `nilai` VALUES ('7', '1', 'PROYEK APLIKASI MULTIMEDIA', '20152', 'IT734', 'A', '3', '0', '0', '0', '0', '0', '100', '90', '80', '85', 'A', '1');
INSERT INTO `nilai` VALUES ('8', '1', 'MATEMATIKA DASAR', '20152', 'IT105', 'A', '2', '0', '0', '0', '0', '0', '100', '50', '70', '65', 'C', '2');
INSERT INTO `nilai` VALUES ('9', '1', 'PENGANTAR INFORMATIKA', '20151', 'IT101', 'A', '3', '0', '0', '0', '0', '0', '100', '70', '80', '70', 'B', '3');
INSERT INTO `nilai` VALUES ('10', '1', 'MATEMATIKA LOGIKA', '20151', 'IT103', 'A', '3', '0', '0', '0', '0', '0', '100', '90', '80', '85', 'A', '4');
INSERT INTO `nilai` VALUES ('11', '2', 'REKAYASA WEB', '20152', 'IT742', 'A', '3', '0', '0', '0', '0', '0', '100', '90', '80', '85', 'B', '3');
INSERT INTO `nilai` VALUES ('12', '2', 'INTERAKSI MANUSIA & KOMPUTER', '20152', 'IT744', 'A', '3', '0', '0', '0', '0', '0', '100', '90', '80', '85', 'A', '4');
INSERT INTO `nilai` VALUES ('13', '2', 'SISTEM INFORMASI GEOGRAFIS', '20151', 'IT302', 'A', '3', '0', '0', '0', '0', '0', '100', '90', '80', '85', 'A', '4');
INSERT INTO `nilai` VALUES ('14', '2', 'MANAJEMEN ASET DIGITAL', '20151', 'IT501', 'A', '3', '0', '0', '0', '0', '0', '100', '90', '80', '85', 'B', '3');

-- ----------------------------
-- Table structure for semester
-- ----------------------------
DROP TABLE IF EXISTS `semester`;
CREATE TABLE `semester` (
  `id` int(11) NOT NULL,
  `nama_semester` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of semester
-- ----------------------------
INSERT INTO `semester` VALUES ('20131', 'Semester Gasal 2013-2014', '0');
INSERT INTO `semester` VALUES ('20132', 'Semester Genap 2013-2014', '0');
INSERT INTO `semester` VALUES ('20141', 'Semester Gasal 2014-2015', '0');
INSERT INTO `semester` VALUES ('20142', 'Semester Genap 2014-2015', '0');
INSERT INTO `semester` VALUES ('20151', 'Semester Gasal 2015-2016', '0');
INSERT INTO `semester` VALUES ('20152', 'Semester Genap 2015-2016', '1');

-- ----------------------------
-- Table structure for status_perwalian
-- ----------------------------
DROP TABLE IF EXISTS `status_perwalian`;
CREATE TABLE `status_perwalian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_mhs` varchar(255) DEFAULT NULL,
  `program` varchar(255) DEFAULT NULL,
  `angkatan` varchar(255) DEFAULT NULL,
  `total_sks` int(11) DEFAULT NULL,
  `ipk` float DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tgl_perwalian` date DEFAULT NULL,
  `jml_mk` int(11) DEFAULT NULL,
  `jml_sks` int(11) DEFAULT NULL,
  `sks_tunda` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of status_perwalian
-- ----------------------------
INSERT INTO `status_perwalian` VALUES ('3', 'ADINDA DESFIANINGRUM', '302', '2013', '86', '3.47', 'Aktif', '2016-02-01', '6', '20', '0');
INSERT INTO `status_perwalian` VALUES ('4', 'SRI NURLELA', '302', '2013', '89', '3.23', 'Aktif', '2016-02-01', '5', '19', '0');
INSERT INTO `status_perwalian` VALUES ('5', 'YAFIANTY SITI VESSIA', '302', '2013', '78', '3.12', 'Aktif', '2016-02-01', '7', '22', '0');
INSERT INTO `status_perwalian` VALUES ('6', 'KARTIKA PERMATASARI', '302', '2013', '68', '3.18', 'Aktif', '2016-02-01', '7', '22', '0');
INSERT INTO `status_perwalian` VALUES ('7', 'ARFA REZA MATAHARI', '302', '2013', '73', '2.98', 'Aktif', '2016-02-01', '6', '20', '0');
INSERT INTO `status_perwalian` VALUES ('8', 'EKA ARDILAH FEBRIYANTI', '302', '2013', '75', '3.89', 'Aktif', '2016-02-01', '5', '21', '0');
INSERT INTO `status_perwalian` VALUES ('9', 'PRAMHESWARI KRISDHI UTAMI PUTRI', '302', '2013', '88', '3.67', 'Aktif', '2016-02-01', '7', '20', '0');
INSERT INTO `status_perwalian` VALUES ('10', 'NANDA AULIA NUGRAHA', '302', '2013', '90', '3.45', 'Aktif', '2016-02-01', '6', '19', '0');
INSERT INTO `status_perwalian` VALUES ('11', 'NADYNE KUSUMAH', '302', '2013', '86', '3.01', 'Aktif', '2016-02-01', '6', '22', '0');
INSERT INTO `status_perwalian` VALUES ('12', 'LARAS SHINTA MEGA DEWI SAPUTRA', '302', '2013', '86', '2.89', 'Aktif', '2016-02-01', '5', '22', '0');
INSERT INTO `status_perwalian` VALUES ('13', 'RADEN ALLYA SITI ZAHRAH WAHJOE', '302', '2013', '89', '2.98', 'Aktif', '2016-02-01', '7', '20', '0');
INSERT INTO `status_perwalian` VALUES ('14', 'AZHAR HANA RIFAI', '302', '2013', '78', '3.47', 'Aktif', '2016-02-01', '7', '21', '0');
INSERT INTO `status_perwalian` VALUES ('15', 'HALIMATUN SA ADAH', '302', '2013', '68', '3.23', 'Aktif', '2016-02-01', '6', '21', '0');
INSERT INTO `status_perwalian` VALUES ('16', 'FITRI RAFI AH ALAWIYAH', '302', '2013', '73', '3.12', 'Aktif', '2016-02-01', '5', '20', '0');
INSERT INTO `status_perwalian` VALUES ('17', 'NAJMI MUNA', '302', '2013', '75', '3.18', 'Aktif', '2016-02-01', '7', '19', '0');
INSERT INTO `status_perwalian` VALUES ('18', 'DENAYA CANDRA SAFIRA', '302', '2013', '88', '2.98', 'Aktif', '2016-02-01', '6', '22', '0');
INSERT INTO `status_perwalian` VALUES ('19', 'NENI YULIANTIKA', '302', '2013', '90', '3.89', 'Aktif', '2016-02-01', '6', '22', '0');
INSERT INTO `status_perwalian` VALUES ('20', 'ANNYSA EKA YUSILAWATI', '302', '2013', '86', '3.67', 'Aktif', '2016-02-01', '5', '20', '0');
INSERT INTO `status_perwalian` VALUES ('21', 'SYAUQI RAHMAT SUGARA', '302', '2013', '89', '3.45', 'Aktif', '2016-02-01', '7', '21', '0');
INSERT INTO `status_perwalian` VALUES ('22', 'MEILIZA CAHYA HAPSARI', '302', '2013', '78', '3.01', 'Aktif', '2016-02-01', '7', '20', '0');
INSERT INTO `status_perwalian` VALUES ('23', 'BALEBAT BUANA PUSPA', '302', '2013', '68', '2.89', 'Aktif', '2016-02-01', '6', '19', '0');
INSERT INTO `status_perwalian` VALUES ('24', 'VIKI CANDRA TAHIR AHMAD', '302', '2013', '73', '2.98', 'Aktif', '2016-02-01', '5', '22', '0');
INSERT INTO `status_perwalian` VALUES ('25', 'JATIPUJI ANDAWURI', '302', '2013', '75', '3.47', 'Aktif', '2016-02-01', '7', '22', '0');
INSERT INTO `status_perwalian` VALUES ('26', 'AYU ALDILA SALMA', '302', '2013', '88', '3.67', 'Aktif', '2016-02-01', '7', '20', '0');
INSERT INTO `status_perwalian` VALUES ('27', 'RIZAL MAULANA GHAFFAR', '302', '2013', '90', '3.45', 'Aktif', '2016-02-01', '6', '21', '0');
INSERT INTO `status_perwalian` VALUES ('28', 'SRI RAHAYU', '302', '2013', '86', '3.01', 'Aktif', '2016-02-01', '5', '20', '0');
INSERT INTO `status_perwalian` VALUES ('29', 'SYIFA NUZULIA RAHMI', '302', '2013', '86', '2.89', 'Aktif', '2016-02-01', '6', '19', '0');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type_user` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `url_foto` varchar(255) DEFAULT NULL,
  `npm` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '133040336', 'd9b1d7db4c', 'MAHASISWA', 'MUHAMMAD AKBAR SIDIQ', 'https://akademik.unpas.ac.id/ft/gambar/avatars/133040336.jpg', '133040336', '3jmt5lbp8ne09ceooqhs782jd2q59wq41edtbbekdgrznah251');
INSERT INTO `user` VALUES ('2', '133040335', 'd9b1d7db4c', 'MAHASISWA', 'BIBEN NURBANI HASAN', 'https://akademik.unpas.ac.id/ft/gambar/avatars/133040335.jpg', '133040335', 'e7rzq3b2xrpyiog1c4shf81tj1445gg7va4ckuw6zhz3wm9xxn');
INSERT INTO `user` VALUES ('3', 'if101', 'd9b1d7db4c', 'DOSEN', 'SANDRA ISLAMA PUTRA, S.SI.,', null, '24015142', 'wfwm1mxo3dpi9gzuol4mldqhlrg1sadcaoodurhkcl6znouq0w');
INSERT INTO `user` VALUES ('4', 'if102', 'd9b1d7db4c', 'DOSEN', 'DR. AYI PURBASARI, ST., MT.,', '', '21456521', 'fo93w2z1ptsz81yb9adet1j7gdrdb5dq992l6xgihl6vd19y6t');
INSERT INTO `user` VALUES ('5', '133041336', 'd9b1d7db4c', 'ORTU', null, null, null, 'mokab1mtnqenzgu6wde3cnhb8wyq1nda5qyebkhp2x67y138g5');
INSERT INTO `user` VALUES ('6', '133041337', 'd9b1d7db4c', 'ORTU', '', '', '', 'ixxe6p62t4ntaoqs3gzjraqoqley8ci4l6ir522d9n8txqafff');
