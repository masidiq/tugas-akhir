<?php
$app->get('/dosen/jadwal_mengajar', function () use ($app) {
    // Set Fakultas
    if(!setFakultasFromHeader($app)){
        $app->response->setStatus(400);
        $res->errorMessage = "header fakultas harus diikutsertakan";
            echo json_encode($res);
            return;
    }
    try {

        $user = getUserByToken($app);
        if (!$user) {
            authError($app);
            return;
        }
        if (!is_dosen($user)) {
            authError($app, "Akses ditolak");
            return;   
        }
        $userId = $user['id'];

        // get jadwal mengajar
        $query = "SELECT * FROM jadwal_mengajar where id_dosen = ".$userId ;
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        // get kehadiran tiap jadwal
        foreach ($result as $row){
            $q2 = "SELECT tanggal, status, mulai, selesai FROM kehadiran_dosen where id_jadwal = ". $row->id ." ORDER BY tanggal DESC";
            $stmt2 = getDB()->query($q2); 
            $result2 = $stmt2->fetchAll(PDO::FETCH_OBJ);
            $row->kehadiran = $result2;
        }
        echo json_encode($result);
    } catch(PDOException $e) {
        $app->response->setStatus(400);
        $res->errorMessage = $e->getMessage();
        echo json_encode($res);
    }
});

$app->get('/dosen/hasil_kuisioner', function () use ($app) {
    // Set Fakultas
    if(!setFakultasFromHeader($app)){
        $app->response->setStatus(400);
        $res->errorMessage = "header fakultas harus diikutsertakan";
            echo json_encode($res);
            return;
    }

    try {

        $user = getUserByToken($app);
        if (!$user) {
            authError($app);
            return;
        }
        if (!is_dosen($user)) {
            authError($app, "Akses ditolak");
            return;   
        }
        $userId = $user['id'];

        $query = "SELECT * FROM hasil_kuisioner where id_dosen = ".$userId;
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($result as $row){
            $q2 = "SELECT pertanyaan, nilai FROM hasil_kuisioner_detail where id_hasil_kuisioner = ". $row->id;
            $stmt2 = getDB()->query($q2); 
            $result2 = $stmt2->fetchAll(PDO::FETCH_OBJ);
            $row->list_pertanyaan = $result2;
        }
        echo json_encode($result);
    } catch(PDOException $e) {
        $app->response->setStatus(400);
        $res->errorMessage = $e->getMessage();
        echo json_encode($res);
    }
});

$app->get('/dosen/status_perwalian', function () use ($app) {
    // Set Fakultas
    if(!setFakultasFromHeader($app)){
        $app->response->setStatus(400);
        $res->errorMessage = "header fakultas harus diikutsertakan";
            echo json_encode($res);
            return;
    }

    try {
        $user = getUserByToken($app);
        if (!$user) {
            authError($app);
            return;
        }
        if (!is_dosen($user)) {
            authError($app, "Akses ditolak");
            return;   
        }
        $userId = $user['id'];

        $query = "SELECT * FROM status_perwalian WHERE id_dosen = " .$userId ." ORDER BY nama_mhs";
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
    } catch(PDOException $e) {
        $app->response->setStatus(400);
        $res->errorMessage = $e->getMessage();
        echo json_encode($res);
    }
});


function is_dosen($user){
    if($user['type_user'] == "DOSEN")
        return true;
    else
        return false;
}
?>