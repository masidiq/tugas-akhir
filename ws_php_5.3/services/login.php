<?php
function passwordVerify($password, $hashDb) {
    // md5 -> md5 -> ambil 10 char pertama
    $password = md5($password);
    $password = md5($password);
    $password = substr($password, 0, 10);
    if ($password == $hashDb) {
        return true;
    }else {
        return false;
    }
}

function generateNewToken() {
    $newToken = null;
    $i = 0;
    $conn = getDB();
    do {
        $i++;
        $newToken = random_string(50);
        $query = "SELECT * FROM user where token = '".$newToken ."' LIMIT 1";
        $stmt = $conn->query($query); 
        $user = $stmt->fetch();
        if ($i == 100) {
            $conn = null;
            die("iterasi new token sudah 100x");
        }
    } while ($user);
    $conn = null;
    return $newToken;
}

function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

function updateUserToken($userId, $newToken) {
    try {
        $conn = getDB();
        $updateSql = "UPDATE user
                     SET token = :token
                     WHERE id = :user_id";
        $updateStmt = $conn->prepare($updateSql);
        $updateStmt->bindValue(":token", $newToken);
        $updateStmt->bindValue(":user_id", $userId);
        $count = $updateStmt->execute();
        $count = null;
        $conn = null;
    }catch(PDOException $e) {
        echo '{"errorMessage":'. $e->getMessage() .'}';
    }
}

function getUserByToken($appSlim) {
    $token = $appSlim->request->headers->get('token');
    if (is_null($token)) {
        return false;
    }
    $conn = getDB();
    $query = "SELECT * FROM user where token = '".$token ."' LIMIT 1";
    $stmt = $conn->query($query); 
    $user = $stmt->fetch();
    $conn = null;
    return $user;
}

function setFakultasFromHeader($appSlim) {
    $fakultas = $appSlim->request->headers->get('fakultas');
    if (is_null($fakultas)) {
        return false;
    }
    setDB($fakultas);
    return $fakultas;
}

function authError($appSlim, $msg = "Token tidak valid"){
    $appSlim->response->setStatus(401);
    $res->errorMessage = $msg;
    echo json_encode($res);
}

// fungsi bantuan
function convert_ortu_to_mhs($ortuId){
        $first = substr($ortuId, 0, 5); 
        $last = substr($ortuId, -3); 
        return $first."0".$last;
}

$app->get('/login', function () use ($app) {
    // Get the header

       // Set Fakultas
    if(!setFakultasFromHeader($app)){
        $app->response->setStatus(400);
        $res->errorMessage = "header fakultas harus diikutsertakan";
            echo json_encode($res);
            return;
    }

    $username = strtolower($app->request->headers->get('username'));
    $password = $app->request->headers->get('password');

    $query = "SELECT * FROM user where username = '".$username ."' LIMIT 1";
    try {
        $stmt = getDB()->query($query); 
        $user = $stmt->fetch();

		
        // jika user tidak ditemukan
        if(!$user){
            $app->response->setStatus(401);
            $res->errorMessage = "Username tidak terdaftar";
            echo json_encode($res);
            return;
        }

        // Bandingkan password
        if (!passwordVerify($password,$user['password'])) {
            $app->response->setStatus(401);
            $res->errorMessage = "Password salah";
            echo json_encode($res);
            return ;
        }
        
        // generate new token
        $newToken = generateNewToken();

        // update token ke table user
        updateUserToken($user['id'],$newToken);

		
        $namaOrtu = null;
        // jika type ORTU, get info user mahasiswa nya
        $typeUserOrigin = $user['type_user'];
        if ($typeUserOrigin == "ORTU") {
            $usernameMhs = convert_ortu_to_mhs($user['username']);
            $namaOrtu = $user['nama'];

            $query = "SELECT * FROM user where username = '".$usernameMhs ."' LIMIT 1";    
            $stmt = getDB()->query($query); 
            $user = $stmt->fetch();

            // jika user tidak ditemukan
            if(!$user){
                $app->response->setStatus(401);
                $res->errorMessage = "Mahasiswa dengan nrp ". $usernameMhs ." tidak terdaftar";
                echo json_encode($res);
                return;
            }
        }
        

        // buat objek untuk dikembalikan ke client
        $res['npm'] = $user['npm'];
        $res['type_user'] = $user['type_user'];
        $res['nama'] = $user['nama'];
        $res['url_foto'] = $user['url_foto'];
        $res['token'] = $newToken;


        // get semester jika user type mahasiswa      
        if ($user['type_user'] == "MAHASISWA") {
            $q2 = "SELECT sem.id, sem.nama_semester, sem.status 
                        FROM mahasiswa_semester  as ms
                        INNER JOIN semester as sem ON sem.id = ms.id_semester
                        WHERE id_mhs = ".$user['id'];
            $stmt2 = getDB()->query($q2); 
            $listSemester = $stmt2->fetchAll(PDO::FETCH_OBJ);
            $res['list_semester'] = $listSemester;
            $res['type_user'] = $typeUserOrigin;
            $res['nama_ortu'] = $namaOrtu;
        }
       echo json_encode($res);
    } catch(PDOException $e) {
        $app->response->setStatus(400);
        $res->errorMessage = $e->getMessage();
        echo json_encode($res);
    }
});

$app->get('/logout', function () use ($app) {
	
	// Set Fakultas
    if(!setFakultasFromHeader($app)){
        $app->response->setStatus(400);
        $res->errorMessage = "header fakultas harus diikutsertakan";
		echo json_encode($res);
		return;
    }
    $user = getUserByToken($app);
    if (!$user) {
        authError($app);
        return;
    }
    updateUserToken($user['id'],null);
    $res['message'] = 'Logout berhasil';
    echo json_encode($res);
});

$app->get('/version', function () use ($app) {
	global $version;
    $res['version'] = $version;
    echo json_encode($res);
});

?>