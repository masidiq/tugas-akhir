<?php
$app->get('/mahasiswa/jadwal_kuliah', function () use ($app) {
    // Set Fakultas
    if(!setFakultasFromHeader($app)){
        $app->response->setStatus(400);
        $res->errorMessage = "header fakultas harus diikutsertakan";
            echo json_encode($res);
            return;
    }

    try {
        $user = getUserByToken($app);
        if (!$user) {
            authError($app);
            return;
        }
    
        // jika type ORTU, get info user mahasiswa nya
        $typeUserOrigin = $user['type_user'];
        if ($typeUserOrigin == "ORTU") {
            $usernameMhs = convert_ortu_to_mhs($user['username']);
            $query = "SELECT id FROM user where username = '".$usernameMhs ."' LIMIT 1";    
            $stmt = getDB()->query($query); 
            $user = $stmt->fetch();

            // jika user tidak ditemukan
            if(!$user){
                $app->response->setStatus(401);
                $res->errorMessage = "Mahasiswa dengan nrp ". $usernameMhs ." tidak terdaftar";
                echo json_encode($res);
                return;
            }
        }

        $userId = $user['id'];
        // get jadwal kul
        $query = "SELECT * FROM jadwal_kuliah where id_mhs = ".$userId;
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        // get kehadiran tiap jadwal
        foreach ($result as $row){
            $q2 = "SELECT tanggal, status, mulai, selesai FROM kehadiran_mahasiswa where id_jadwal = ". $row->id  ." ORDER BY tanggal DESC";
            $stmt2 = getDB()->query($q2); 
            $result2 = $stmt2->fetchAll(PDO::FETCH_OBJ);
            $row->kehadiran = $result2;
        }

        echo json_encode($result);
    } catch(PDOException $e) {
        $app->response->setStatus(400);
        $res->errorMessage = $e->getMessage();
        echo json_encode($res);
    }
});

$app->get('/mahasiswa/jadwal_ujian', function () use ($app) {
    // Set Fakultas
    if(!setFakultasFromHeader($app)){
        $app->response->setStatus(400);
        $res->errorMessage = "header fakultas harus diikutsertakan";
            echo json_encode($res);
            return;
    }

    try {
        $user = getUserByToken($app);
        if (!$user) {
            authError($app);
            return;
        }

        // jika type ORTU, get info user mahasiswa nya
        $typeUserOrigin = $user['type_user'];
        if ($typeUserOrigin == "ORTU") {
            $usernameMhs = convert_ortu_to_mhs($user['username']);
            $query = "SELECT id FROM user where username = '".$usernameMhs ."' LIMIT 1";    
            $stmt = getDB()->query($query); 
            $user = $stmt->fetch();

            // jika user tidak ditemukan
            if(!$user){
                $app->response->setStatus(401);
                $res->errorMessage = "Mahasiswa dengan nrp ". $usernameMhs ." tidak terdaftar";
                echo json_encode($res);
                return;
            }
        }
        $userId = $user['id'];

        $query = "SELECT * FROM jadwal_ujian where id_mhs = ".$userId;
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
    } catch(PDOException $e) {
        $app->response->setStatus(400);
        $res->errorMessage = $e->getMessage();
        echo json_encode($res);
    }
});

$app->get('/mahasiswa/nilai', function () use ($app) {
    // Set Fakultas
    if(!setFakultasFromHeader($app)){
        $app->response->setStatus(400);
        $res->errorMessage = "header fakultas harus diikutsertakan";
            echo json_encode($res);
            return;
    }
    try {
        // ambil user berdasarkan user
        $user = getUserByToken($app);
        if (!$user) {
            authError($app);
            return;
        }

        // jika type ORTU, get info user mahasiswa nya
        $typeUserOrigin = $user['type_user'];
        if ($typeUserOrigin == "ORTU") {
            $usernameMhs = convert_ortu_to_mhs($user['username']);
            $query = "SELECT id FROM user where username = '".$usernameMhs ."' LIMIT 1";    
            $stmt = getDB()->query($query); 
            $user = $stmt->fetch();

            // jika user tidak ditemukan
            if(!$user){
                $app->response->setStatus(401);
                $res->errorMessage = "Mahasiswa dengan nrp ". $usernameMhs ." tidak terdaftar";
                echo json_encode($res);
                return;
            }
        }
        $userId = $user['id'];

        $query = "SELECT * FROM nilai where id_mhs = ".$userId ." ORDER BY semester DESC, mata_kuliah ASC";
    
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
    } catch(PDOException $e) {
        $app->response->setStatus(400);
        $res->errorMessage = $e->getMessage();
        echo json_encode($res);
    }
});



?>