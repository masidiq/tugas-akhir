<?php
// Untuk menghilangkan error reporting
//error_reporting(0);

require 'db.php';
require_once 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

// set response header json
$app->response->headers->set('Content-Type', 'application/json');

// INDEX
$app->get('/', function () use ($app){
    global $dbhost;
    global $dbuser;
    global $dbpass;
    global $dbname;
    mysql_connect($dbhost, $dbuser, $dbpass) or die ("Koneksi DB Error");
    mysql_select_db ($dbname) or die ("Database '".$dbname ."'' Tidak dapat diakses");
    echo "Server is Up";
});

$app->get('/semester', function (){
    $query = "SELECT * FROM semester";
    try {
        $stmt = getDB()->query($query); 
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
});
 

require_once 'services/login.php';
require_once 'services/mahasiswa.php';
require_once 'services/dosen.php';

$app->run();
