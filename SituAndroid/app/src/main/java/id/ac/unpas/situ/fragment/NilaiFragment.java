package id.ac.unpas.situ.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.ac.unpas.situ.MainActivity;
import id.ac.unpas.situ.MainData;
import id.ac.unpas.situ.R;
import id.ac.unpas.situ.customAdapter.JadwalKuliahAdapter;
import id.ac.unpas.situ.customAdapter.NilaiAdapter;
import id.ac.unpas.situ.detailActivity.DetailJadwalKuliahActivity;
import id.ac.unpas.situ.detailActivity.DetailNilaiActivity;
import id.ac.unpas.situ.helper.EnumHari;
import id.ac.unpas.situ.model.JadwalKuliah;
import id.ac.unpas.situ.model.Nilai;
import id.ac.unpas.situ.service.JadwalKuliahService;
import id.ac.unpas.situ.service.NilaiService;
import id.ac.unpas.situ.utils.AlertHelper;
import id.ac.unpas.situ.utils.OnTaskListener;
import id.ac.unpas.situ.utils.RequestTask;
import id.ac.unpas.situ.utils.ResponseData;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NilaiFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NilaiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NilaiFragment extends Fragment  implements OnTaskListener {
    private OnFragmentInteractionListener mListener;

    private MainActivity activity;
    private StickyListHeadersListView mList;
    private NilaiAdapter mAdapter;
    private List<Nilai> mainList;
    private TextView txtLastUpdate;

    public static NilaiFragment newInstance(String param1, String param2) {
        NilaiFragment fragment = new NilaiFragment();
        return fragment;
    }

    public NilaiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_nilai, container, false);
        initUI(view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.activity = (MainActivity)activity;
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onTaskCompleted(ResponseData response) {
        this.activity.hideLoading();

        if (response.getStatus() == HttpURLConnection.HTTP_OK){
            mainList = NilaiService.parserJsonResult(response.getResponseString());
            if (mainList != null && mainList.size() > 0){
                MainData.getInstance().setNilaiLastUpdate(new Date());
                MainData.getInstance().setNilaiList(mainList);
                MainData.getInstance().save(this.activity);
                refreshListView();
                activity.showLoadSuccessToast();
            }
            return;
        }else if (response.getStatus() == HttpURLConnection.HTTP_UNAUTHORIZED){
            activity.showAlertToken();
        }else {
            AlertHelper.ShowAlert(activity, activity.getString(R.string.load_failed), response.getResponseString());
        }
    }

    @Override
    public void onTaskStarted() {
        this.activity.showLoading();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    private void initUI(View view){
        //txtResponse = (TextView) view.findViewById(R.id.txtResponse);
        mainList = new ArrayList<>();
        mList = (StickyListHeadersListView) view.findViewById(R.id.list);
        txtLastUpdate = (TextView)view.findViewById(R.id.txtLastUpdate);


        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(activity, DetailNilaiActivity.class);
                intent.putExtra(DetailNilaiActivity.PARAMS_POSITION, position);
                startActivity(intent);
            }

        });

        setValueToUI();
    }

    public void requestToServer(){
        RequestTask task = new RequestTask(this);
        task.execute(NilaiService.endPoint);
    }

    public void setValueToUI(){
        if (MainData.getInstance().getNilaiList() == null){
            requestToServer();
            return;
        }
        mainList = MainData.getInstance().getNilaiList();
        if(!mainList.isEmpty()) {
            refreshListView();

        }
    }

    private void refreshListView(){
        mAdapter = new NilaiAdapter(getActivity(), mainList);
        mList.setAdapter(mAdapter);
        txtLastUpdate.setText(EnumHari.generateLastUpdate(MainData.getInstance().getNilaiLastUpdate()));
    }
}
