package id.ac.unpas.situ.customAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.ac.unpas.situ.MainData;
import id.ac.unpas.situ.R;
import id.ac.unpas.situ.helper.ThemeManager;
import id.ac.unpas.situ.model.Nilai;
import id.ac.unpas.situ.model.Semester;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Sidiq on 3/18/2016.
 */
public class NilaiAdapter extends ArrayAdapter<Nilai> implements StickyListHeadersAdapter {
    private final Context context;
    private final List<Nilai> listValue;

    public NilaiAdapter(Context context, List<Nilai> listValue) {
        super(context,-1, listValue);
        this.context = context;
        this.listValue = listValue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_nilai, parent, false);

        ((TextView) rowView.findViewById(R.id.txtNo)).setText(Integer.toString(listValue.get(position).getNo()));
        ((TextView) rowView.findViewById(R.id.txtKodeMk)).setText(listValue.get(position).getKodeMk());
        ((TextView) rowView.findViewById(R.id.txtMataKuliah)).setText(listValue.get(position).getMataKuliah());
        ((TextView) rowView.findViewById(R.id.txtSks)).setText(Integer.toString(listValue.get(position).getSks()));
        ((TextView) rowView.findViewById(R.id.txtNilai)).setText(listValue.get(position).getGrade());
        return rowView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView =inflater.inflate(R.layout.sticky_header_nilai, parent, false);
            holder.ipkInfo = (LinearLayout) convertView.findViewById(R.id.topNilai);
            holder.text = (TextView) convertView.findViewById(R.id.txtSemester);
            holder.txtJmlSks = (TextView) convertView.findViewById(R.id.txtJmlSks);
            holder.txtIps = (TextView) convertView.findViewById(R.id.txtIps);
            holder.header = (LinearLayout) convertView.findViewById(R.id.header);
            holder.headerTop = (LinearLayout) convertView.findViewById(R.id.headerTop);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        if (position != 0){
            holder.ipkInfo.setVisibility(View.GONE);
        }else {

            holder.txtTotalSks = (TextView) convertView.findViewById(R.id.txtTotalSks);
            holder.txtIpkStart = (TextView) convertView.findViewById(R.id.txtIpkStart);
            holder.txtIpkEnd = (TextView) convertView.findViewById(R.id.txtIpkEnd);

            holder.txtTotalSks.setText(Integer.toString(MainData.getInstance().countTotalSks()));


            float ipk = MainData.getInstance().countIpk();
            String ipkStr = String.format("%.02f", ipk);
            holder.txtIpkStart.setText(ipkStr.substring(0,1));
            holder.txtIpkEnd.setText(ipkStr.substring(1,4 ));
        }

        //set header text as first char in name
        String headerText = MainData.getInstance().getProfile().getNamaSemesterById(listValue.get(position).getIdSemester());
        holder.text.setText(headerText);
        Float ips = MainData.getInstance().countIps(listValue.get(position).getIdSemester());
        holder.txtIps.setText( String.format("%.02f", ips));
        holder.txtJmlSks.setText(Integer.toString(MainData.getInstance().countJmlSksPerSemester(listValue.get(position).getIdSemester())));
        holder.header.setBackgroundColor(ThemeManager.getColorPrimary(getContext()));
        holder.headerTop.setBackgroundColor(ThemeManager.getColorPrimaryDark(getContext()));
        holder.ipkInfo.setBackgroundColor(ThemeManager.getColorPrimaryDark(getContext()));
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return Long.parseLong(listValue.get(position).getIdSemester());
    }

    @Override
    public Nilai getItem(int position) {
        return listValue.get(position);
    }

    class HeaderViewHolder {
        LinearLayout headerTop;
        LinearLayout header;
        LinearLayout ipkInfo;
        TextView txtTotalSks;
        TextView txtIpkStart;
        TextView txtIpkEnd;
        TextView text;
        TextView txtJmlSks;
        TextView txtIps;
    }
}
