package id.ac.unpas.situ.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import id.ac.unpas.situ.model.Nilai;

/**
 * Created by Sidiq on 3/18/2016.
 */
public class NilaiService {
    public static String endPoint = "mahasiswa/nilai";
    private static final String KEY_IdSemester = "semester";
    private static final String KEY_MataKuliah = "mata_kuliah";
    private static final String KEY_Kode = "kode_mk";
    private static final String KEY_Kelas = "kelas";
    private static final String KEY_Sks = "sks";
    private static final String KEY_Tugas1 = "tugas1";
    private static final String KEY_Tugas2 = "tugas2";
    private static final String KEY_Tugas3 = "tugas3";
    private static final String KEY_Tugas4 = "tugas4";
    private static final String KEY_Tugas5 = "tugas5";
    private static final String KEY_Presensi = "presensi";
    private static final String KEY_Uts = "uts";
    private static final String KEY_Uas = "uas";
    private static final String KEY_Akhir = "akhir";
    private static final String KEY_Grade = "grade";
    private static final String KEY_Bobot = "bobot";


    public static List<Nilai> parserJsonResult(String jsonStr){
        try {

            List<Nilai> result = new ArrayList<>();
            if (jsonStr == null)
                return result;

            JSONArray jsonarray = new JSONArray(jsonStr);

            for (int i=0; i<jsonarray.length(); i++){
                JSONObject o = jsonarray.getJSONObject(i);
                Nilai j = new Nilai();

                j.setPresensi(o.getString(KEY_Presensi));
                j.setSks(Integer.parseInt(o.getString(KEY_Sks)));
                j.setKelas(o.getString(KEY_Kelas));
                j.setMataKuliah(o.getString(KEY_MataKuliah));
                j.setIdSemester(o.getString(KEY_IdSemester));

                j.setKodeMk(o.getString(KEY_Kode));
                j.setTugas1(o.getString(KEY_Tugas1));
                j.setTugas2(o.getString(KEY_Tugas2));
                j.setTugas3(o.getString(KEY_Tugas3));
                j.setTugas4(o.getString(KEY_Tugas4));
                j.setTugas5(o.getString(KEY_Tugas5));

                j.setUts(o.getString(KEY_Uts));
                j.setUas(o.getString(KEY_Uas));
                j.setAkhir(o.getString(KEY_Akhir));
                j.setGrade(o.getString(KEY_Grade));
                j.setBobot(Float.parseFloat(o.getString(KEY_Bobot)));

                result.add(j);

            }
            //Collections.sort(result, Nilai.SemesterAsc);
            setNoBySemester(result);
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void setNoBySemester(List<Nilai> list){
        List<String> listSemester = new ArrayList<>();
        for(Nilai n : list){
            int count = 0;
            for(String s: listSemester){
                if (n.getIdSemester().equals(s)){
                    count++;
                }
            }
            if (count == 0){
                listSemester.add(n.getIdSemester());
            }
        }

        for(String s:listSemester){
            int no = 1;
            for(Nilai n:list){
                if (n.getIdSemester().equals(s)){
                    n.setNo(no++);
                }
            }
        }
    }


}
