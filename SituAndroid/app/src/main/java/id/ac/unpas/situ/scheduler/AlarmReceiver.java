package id.ac.unpas.situ.scheduler;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.ac.unpas.situ.MainActivity;
import id.ac.unpas.situ.MainData;
import id.ac.unpas.situ.R;
import id.ac.unpas.situ.helper.CapitalFirstLetters;
import id.ac.unpas.situ.helper.EnumHari;
import id.ac.unpas.situ.model.JadwalKuliah;
import id.ac.unpas.situ.model.JadwalMengajar;
import id.ac.unpas.situ.model.JadwalUjian;


/**
 * Created by Sidiq on 3/21/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {
    public static int ALARM_JADWAL_KULIAH   = 100;
    public static int ALARM_JADWAL_UJIAN    = 200;
    public static int ALARM_JADWAL_MENGAJAR   = 300;

    private static String PARAMS_MATA_KULIAH = "mata_kuliah";
    private static String PARAMS_PESAN = "pesan";
    private static String PARAMS_ARRAY_STR = "array_str";


    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notifManager = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
        Intent intent1 = new Intent(context.getApplicationContext(), MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent1, 0);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        String mataKuliah = intent.getStringExtra(PARAMS_MATA_KULIAH);
        String pesan = intent.getStringExtra(PARAMS_PESAN);
        String[] listStr = intent.getStringArrayExtra(PARAMS_ARRAY_STR);

        inboxStyle.setBigContentTitle(mataKuliah);

        // add line string
        for (String str:listStr) {
            inboxStyle.addLine(str);
        }

        Notification notif = new NotificationCompat.Builder(context)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.unpas_logo))
                .setContentTitle(mataKuliah)
                .setContentText(pesan)
                .setSmallIcon(R.drawable.ic_student)
                .setColor(context.getResources().getColor(R.color.colorPrimary))
                .setContentIntent(pIntent)
                .setPriority(Notification.PRIORITY_HIGH)
                .setStyle(inboxStyle)
                .setLights(Color.YELLOW, 2000, 5000)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setAutoCancel(true)
                .build();
        notifManager.notify(0, notif);
    }

    public static void cancelAlarm(Context context,int alarmId){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent updateServiceIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingUpdateIntent = PendingIntent.getBroadcast(context, alarmId, updateServiceIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        // Cancel alarms
        try {
            alarmManager.cancel(pendingUpdateIntent);
        } catch (Exception e) {
        }
    }

    public static void cancelJadwalKuliah(Context context){
        if (MainData.getInstance().getJadwalKuliahList() == null)
            return;

        for (int i = 0; i<MainData.getInstance().getJadwalKuliahList().size()*2;i++){
            cancelAlarm(context,ALARM_JADWAL_KULIAH + i);
        }
    }

    public static void cancelJadwalUjian(Context context){
        if (MainData.getInstance().getJadwalUjianList() == null)
            return;

        for (int i = 0; i<(MainData.getInstance().getJadwalUjianList().size()*2)+14;i++){
            cancelAlarm(context,ALARM_JADWAL_UJIAN + i);
        }
    }

    public static void cancelJadwalMengajar(Context context){
        if (MainData.getInstance().getJadwalMengajarList() == null)
            return;

        for (int i = 0; i<MainData.getInstance().getJadwalMengajarList().size()*2;i++){
            cancelAlarm(context,ALARM_JADWAL_MENGAJAR + i);
        }
    }

    public static void setAlarm(Context context,Calendar calendar, String mataKuliah, String pesan, String[] arrayStr, int alarmId){
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(PARAMS_MATA_KULIAH,mataKuliah);
        intent.putExtra(PARAMS_PESAN,pesan);
        intent.putExtra(PARAMS_ARRAY_STR,arrayStr);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarmId, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar now = Calendar.getInstance();
        long _timeMillis = 0;
        if(calendar.getTimeInMillis() <= now.getTimeInMillis())
            _timeMillis = calendar.getTimeInMillis() + (AlarmManager.INTERVAL_DAY * 7);
        else
            _timeMillis = calendar.getTimeInMillis();

        // Get the AlarmManager service
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, _timeMillis, alarmManager.INTERVAL_DAY * 7, pendingIntent);
        // Enable {@code SampleBootReceiver} to automatically restart the alarm when the
        // device is rebooted.
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public static Calendar hariParseToCalendar(String hari, String jam){
        Calendar calendar = Calendar.getInstance();

        int parseDay = EnumHari.getEnum(hari);
        int parseHour = Integer.parseInt(jam.substring(0, 2));
        int parseMinute = Integer.parseInt(jam.substring(3, 5));

        calendar.set(Calendar.DAY_OF_WEEK, parseDay);
        calendar.set(Calendar.HOUR_OF_DAY, parseHour);
        calendar.set(Calendar.MINUTE, parseMinute);
        calendar.set(Calendar.SECOND, 0);
        return calendar;
    }

    public static Calendar dateParseToCalendar(Date date, String jam){
        Calendar calendar = Calendar.getInstance();

        int parseHour = Integer.parseInt(jam.substring(0, 2));
        int parseMinute = Integer.parseInt(jam.substring(3, 5));

        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, parseHour);
        calendar.set(Calendar.MINUTE, parseMinute);
        calendar.set(Calendar.SECOND, 0);
        return calendar;
    }

    public static void setScheduleJadwalKuliah(Context context){
        List<JadwalKuliah> list = MainData.getInstance().getJadwalKuliahList();

        int i = 0;
        int notifId = 0;
        for (JadwalKuliah jadwal: list) {
            Calendar calendar = hariParseToCalendar(jadwal.getHari(),jadwal.getJam());

            // jadwal on time
            setAlarm(context,
                    calendar,
                    CapitalFirstLetters.toCapital(jadwal.getMataKuliah()),
                    "Perkuliahan dimulai",
                    new String[]{jadwal.getDosen(), jadwal.getJam(), "Ruang " +jadwal.getRuang()},
                    ALARM_JADWAL_KULIAH + i++);

            // 15 menit sebelumnya
            calendar.add(Calendar.MINUTE, -15);
            setAlarm(context,
                    calendar,
                    CapitalFirstLetters.toCapital(jadwal.getMataKuliah()),
                    "[" + jadwal.getJam() + "] Kuliah akan dimulai 15 menit lagi",
                    new String[]{jadwal.getDosen(), jadwal.getJam(), "Ruang " +jadwal.getRuang()},
                    ALARM_JADWAL_KULIAH + i++);
        }
    }

    public static void setScheduleJadwalMengajar(Context context){
        List<JadwalMengajar> list = MainData.getInstance().getJadwalMengajarList();

        int i = 0;
        int notifId = 0;
        for (JadwalMengajar jadwal: list) {
            Calendar calendar = hariParseToCalendar(jadwal.getHari(),jadwal.getJam());

            // jadwal on time
            setAlarm(context,
                    calendar,
                    "Selamat mengajar",
                    CapitalFirstLetters.toCapital(jadwal.getMataKuliah()),
                    new String[]{jadwal.getMataKuliah(), jadwal.getJam(), "Ruang " +jadwal.getRuang()},
                    ALARM_JADWAL_MENGAJAR + i++);

            // 15 menit sebelumnya
            calendar.add(Calendar.MINUTE, -15);
            setAlarm(context,
                    calendar,
                    CapitalFirstLetters.toCapital(jadwal.getMataKuliah()),
                    "Jadwal mengajar akan dimulai 15 menit lagi",
                    new String[]{jadwal.getJam(), "Ruang " +jadwal.getRuang()},
                    ALARM_JADWAL_MENGAJAR + i++
                    );
        }
    }

    public static void setScheduleJadwalUjian(Context context){
        List<JadwalUjian> list = MainData.getInstance().getJadwalUjianList();

        int i = 0;
        int notifId = 0;
        List<Date> groupByDate = new ArrayList<>();
        for (JadwalUjian jadwal: list) {
            Calendar calendar = dateParseToCalendar(jadwal.getTanggal(), jadwal.getJam());

            // jadwal on time
            setAlarm(context,
                    calendar,
                    CapitalFirstLetters.toCapital(jadwal.getMataKuliah()),
                    "Ujian dimulai",
                    new String[]{jadwal.getDosen(), jadwal.getJam(), "Ruang " + jadwal.getRuang()},
                    ALARM_JADWAL_UJIAN + i++
                    );

            // 15 menit sebelumnya
            calendar.add(Calendar.MINUTE, -15);
            setAlarm(context,
                    calendar,
                    CapitalFirstLetters.toCapital(jadwal.getMataKuliah()),
                    "[" + jadwal.getJam() + "] Ujian akan dimulai 15 menit lagi",
                    new String[]{jadwal.getDosen(), jadwal.getJam(), "Ruang " + jadwal.getRuang()},
                    ALARM_JADWAL_UJIAN + i++
                    );

            // Dikumpulkan by tanggal dulu
            boolean ketemu = false;
            for (Date dt : groupByDate) {
                if (dt.equals(jadwal.getTanggal())){
                    ketemu = true;
                    break;
                }
            }
            if (!ketemu){
                groupByDate.add(jadwal.getTanggal());
            }

        }

        // sehari sebelum ujian
        // pukul 19:30
        for (Date dt : groupByDate){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dt);
            calendar.add(Calendar.HOUR_OF_DAY, -24);
            calendar.set(Calendar.HOUR_OF_DAY, 19);
            calendar.set(Calendar.MINUTE, 30);
            calendar.set(Calendar.SECOND, 0);
            List<String> listMk = new ArrayList<>();
            for (JadwalUjian ujian : list) {
                if (dt.equals(ujian.getTanggal())){
                    listMk.add("[" + ujian.getJam() + "] " + CapitalFirstLetters.toCapital(ujian.getMataKuliah()));
                }
            }

            String[] array = listMk.toArray(new String[listMk.size()]);

            setAlarm(context,
                    calendar,
                    "Besok ujian, jangan lupa belajar!",
                    "Ingat kembali materi yg pernah disampaikan",
                    array,
                    ALARM_JADWAL_UJIAN + i++
                    );
        }
    }


}