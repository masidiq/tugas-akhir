package id.ac.unpas.situ.customAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

import id.ac.unpas.situ.R;

import id.ac.unpas.situ.helper.EnumHari;
import id.ac.unpas.situ.helper.ThemeManager;
import id.ac.unpas.situ.model.JadwalUjian;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Sidiq on 3/15/2016.
 */
public class JadwalUjianAdapter extends ArrayAdapter<JadwalUjian> implements StickyListHeadersAdapter {
    private final Context context;
    private final List<JadwalUjian> listValue;

    public JadwalUjianAdapter(Context context, List<JadwalUjian> listValue) {
        super(context,-1, listValue);
        this.context = context;
        this.listValue = listValue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_jadwal_kuliah, parent, false);

        ((TextView) rowView.findViewById(R.id.txtMataKuliah)).setText(listValue.get(position).getMataKuliah());
        ((TextView) rowView.findViewById(R.id.txtRuang)).setText(listValue.get(position).getRuang());
        ((TextView) rowView.findViewById(R.id.txtKelas)).setText("Kelas " + listValue.get(position).getKelas());
        ((TextView) rowView.findViewById(R.id.txtDosen)).setText(listValue.get(position).getDosen());
        ((TextView) rowView.findViewById(R.id.txtJam)).setText(listValue.get(position).getJam());
        ((TextView) rowView.findViewById(R.id.txtSks)).setText(listValue.get(position).getSks()+ " sks");
        return rowView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView =inflater.inflate(R.layout.sticky_header, parent, false);
            holder.header = (LinearLayout) convertView.findViewById(R.id.header);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.header.setBackgroundColor(ThemeManager.getColorPrimary(getContext()));
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        String headerText = listValue.get(position).getHari() + ", " + EnumHari.dateToString(listValue.get(position).getTanggal());
        holder.text.setText(headerText);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return listValue.get(position).getIdWaktu();
    }

    @Override
    public JadwalUjian getItem(int position) {
        return listValue.get(position);
    }

    class HeaderViewHolder {
        LinearLayout header;
        TextView text;
    }
}
