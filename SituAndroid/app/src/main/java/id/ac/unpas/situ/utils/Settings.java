package id.ac.unpas.situ.utils;

import android.content.Context;
import android.content.SharedPreferences;

import id.ac.unpas.situ.MainData;

/**
 * Created by Sidiq on 2/28/2016.
 */
public class Settings {
    private static Settings ourInstance = new Settings();
    public static String token;
    public static int currentMenu = 0;
    //KEY SHARED
    private static final String FILE_NAME = "id.ac.unpas.situ_preferences";
    public static final String KEY_TOKEN = "token";

    public static Settings getInstance() {
        return ourInstance;
    }

    private Settings() {
    }


    public static void setConnected(Context ctx){
        SharedPreferences shared = ctx.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    public static boolean hasConnected(Context ctx){
        SharedPreferences shared = ctx.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        token = shared.getString(KEY_TOKEN, "");
        if(token =="")
            return false;
        else
            return true;
    }



    public static void clearConnected(Context ctx){
        currentMenu = 0;
        SharedPreferences shared = ctx.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(KEY_TOKEN, "");
        editor.commit();
    }
}
