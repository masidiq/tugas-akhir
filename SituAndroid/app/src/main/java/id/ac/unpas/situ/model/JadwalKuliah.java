package id.ac.unpas.situ.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Sidiq on 2/28/2016.
 */
public class JadwalKuliah implements Serializable {
    private String mataKuliah;
    private String idSemester;
    private String hari;
    private long idHari;
    private String jam;
    private String ruang;
    private String kode;
    private String kelas;
    private String sks;
    private String dosen;
    private String presensi;

    private List<Kehadiran> kehadiranList;

    public JadwalKuliah() {
        kehadiranList = new ArrayList<>();
    }

    public String getMataKuliah() {
        return mataKuliah;
    }

    public void setMataKuliah(String mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getRuang() {
        return ruang;
    }

    public void setRuang(String ruang) {
        this.ruang = ruang;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getSks() {
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }

    public String getDosen() {
        return dosen;
    }

    public void setDosen(String dosen) {
        this.dosen = dosen;
    }

    public String getPresensi() {
        return presensi;
    }

    public void setPresensi(String presensi) {
        this.presensi = presensi;
    }

    public String getIdSemester() {
        return idSemester;
    }

    public void setIdSemester(String idSemester) {
        this.idSemester = idSemester;
    }


    public static Comparator<JadwalKuliah> JamAsc = new Comparator<JadwalKuliah>() {
        public int compare(JadwalKuliah s1, JadwalKuliah s2) {
            String str1 = Long.toString(s1.getIdHari()) + s1.getJam().toUpperCase();
            String str2 = Long.toString(s2.getIdHari()) + s2.getJam().toUpperCase();
            return str1.compareTo(str2);
        }
    };

    public long getIdHari() {
        return idHari;
    }

    public void setIdHari(long idHari) {
        this.idHari = idHari;
    }

    public List<Kehadiran> getKehadiranList() {
        return kehadiranList;
    }

    public void setKehadiranList(List<Kehadiran> kehadiranList) {
        this.kehadiranList = kehadiranList;
    }

    public int getJmlHadir() {
        int jmlHadir = 0;
        for (Kehadiran k : kehadiranList) {
            if (k.getStatus().toLowerCase().equals("hadir"))
                jmlHadir++;
        }
        return jmlHadir;
    }

    public int getJmlPertemuan(){
        return kehadiranList.size();
    }


    public String getPresensiLong(){
        return "Kehadiran " + Integer.toString(getJmlHadir()) + " dari " +Integer.toString(getJmlPertemuan()) + " pertemuan";
    }
}
