package id.ac.unpas.situ.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Sidiq on 3/13/2016.
 */
public class JadwalMengajar implements Serializable{
    private String mataKuliah;
    private String idSemester;
    private String hari;
    private long idHari;
    private String jam;
    private String ruang;
    private String kodeMk;
    private String kelas;
    private String sks;
    private String presensi;
    private List<Kehadiran> kehadiranList;

    public JadwalMengajar() {
        kehadiranList = new ArrayList<>();
    }

    public String getMataKuliah() {
        return mataKuliah;
    }

    public void setMataKuliah(String mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    public String getIdSemester() {
        return idSemester;
    }

    public void setIdSemester(String idSemester) {
        this.idSemester = idSemester;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public long getIdHari() {
        return idHari;
    }

    public void setIdHari(long idHari) {
        this.idHari = idHari;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getRuang() {
        return ruang;
    }

    public void setRuang(String ruang) {
        this.ruang = ruang;
    }

    public String getKodeMk() {
        return kodeMk;
    }

    public void setKodeMk(String kodeMk) {
        this.kodeMk = kodeMk;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getSks() {
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }

    public String getPresensi() {
        return presensi;
    }

    public void setPresensi(String presensi) {
        this.presensi = presensi;
    }

    public static Comparator<JadwalMengajar> HariAsc = new Comparator<JadwalMengajar>() {
        public int compare(JadwalMengajar s1, JadwalMengajar s2) {
            String str1 = Long.toString(s1.getIdHari()) + s1.getJam().toUpperCase();
            String str2 = Long.toString(s2.getIdHari()) + s2.getJam().toUpperCase();
            return str1.compareTo(str2);
        }
    };

    public List<Kehadiran> getKehadiranList() {
        return kehadiranList;
    }

    public void setKehadiranList(List<Kehadiran> kehadiranList) {
        this.kehadiranList = kehadiranList;
    }

    public int getJmlHadir() {
        int jmlHadir = 0;
        for (Kehadiran k : kehadiranList) {
            if (k.getStatus().toLowerCase().equals("hadir"))
                jmlHadir++;
        }
        return jmlHadir;
    }

    public int getJmlPertemuan(){
        return kehadiranList.size();
    }


    public String getPresensiLong(){
        return "Kehadiran : " + Integer.toString(getJmlPertemuan()) + " pertemuan";
    }
}
