package id.ac.unpas.situ.utils;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import id.ac.unpas.situ.MainData;

/**
 * Created by Sidiq on 3/13/2016.
 * Task khusus manggil fugnsi login karena header nya berbeda dengan yang lain
 * yaitu username dan password
 */
public class LoginRequestTask extends AsyncTask<String,Void,ResponseData> {
    private OnTaskListener callBack;

    public LoginRequestTask(OnTaskListener callBack){
        this.callBack = callBack;
    }

    @Override
    protected ResponseData doInBackground(String... params) {
        ResponseData response = new ResponseData();
        try {

            String host = MainData.getInstance().getServer();
            URL url = new URL(host + params[0]);
            String username = params[1];
            String password = params[2];


            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            // set header
            conn.setRequestProperty("username", username);
            conn.setRequestProperty("password", password);
            conn.setRequestProperty("fakultas", MainData.getInstance().getFakultas());
            conn.connect();

            response.setStatus(conn.getResponseCode());
            BufferedReader br;
            if (response.getStatus() == HttpURLConnection.HTTP_OK)
                br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            else
                br = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line+"\n");
            }
            br.close();
            response.setResponseString(sb.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            response.setStatus(-1);
            response.setResponseString("Tidak dapat mengakses ke server.\nPeriksa koneksi internet anda atau coba beberapa saat lagi");
        } catch (IOException e) {
            e.printStackTrace();
            response.setStatus(-1);
            response.setResponseString("Tidak dapat mengakses ke server.\nPeriksa koneksi internet anda atau coba beberapa saat lagi");
        }
        return response;

    }

    @Override
    protected void onPreExecute() {
        callBack.onTaskStarted();
    }

    @Override
    protected void onPostExecute(ResponseData result) {
        callBack.onTaskCompleted(result);
    }

}
