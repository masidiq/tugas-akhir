package id.ac.unpas.situ.fragment;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.ac.unpas.situ.MainActivity;
import id.ac.unpas.situ.MainData;
import id.ac.unpas.situ.R;
import id.ac.unpas.situ.customAdapter.JadwalKuliahAdapter;
import id.ac.unpas.situ.detailActivity.DetailJadwalKuliahActivity;
import id.ac.unpas.situ.helper.ThemeManager;
import id.ac.unpas.situ.model.JadwalKuliah;
import id.ac.unpas.situ.model.Profile;
import id.ac.unpas.situ.utils.DownloadImageTask;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private MainActivity activity;

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();

        return fragment;
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initUI(view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
            this.activity = (MainActivity)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private void initUI(View view){
        if (MainData.getInstance().getProfile().isDosen()){
            View menu = view.findViewById(R.id.menu_mahasiswa);
            ((LinearLayout)menu.getParent()).removeView(menu);
            setListenerMenuDosen(view);
        }else {
            View menu = view.findViewById(R.id.menu_dosen);
            ((LinearLayout)menu.getParent()).removeView(menu);
            setListenerMenuMahasiswa(view);
        }

        Profile profile = MainData.getInstance().getProfile();

        ((LinearLayout)view.findViewById(R.id.header)).setBackgroundColor(ThemeManager.getColorPrimary(activity));

        ((TextView)view.findViewById(R.id.txtName)).setText(profile.getNama());
        ((TextView)view.findViewById(R.id.txtNrp)).setText(profile.getNpm());
        ((TextView)view.findViewById(R.id.txtTypeUser)).setText(profile.getTypeUserToString());
        ((TextView)view.findViewById(R.id.txtNamOrtu)).setText(profile.getNamaOrtu());
        if (profile.getFotoUrl() != null && !profile.getFotoUrl().isEmpty()) {
            new DownloadImageTask((ImageView) view.findViewById(R.id.fotoUser))
                    .execute(MainData.getInstance().getProfile().getFotoUrl());
        }
    }


    private void setListenerMenuMahasiswa(View view){
        view.findViewById(R.id.nav_jadwalKuliah).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.clickMenu(1);
            }
        });

        view.findViewById(R.id.nav_jadwalUjian).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.clickMenu(2);
            }
        });

        view.findViewById(R.id.nav_nilai).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.clickMenu(3);
            }
        });
    }

    private void setListenerMenuDosen(View view){
        view.findViewById(R.id.nav_jadwalMengajar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.clickMenu(1);
            }
        });

        view.findViewById(R.id.nav_hasilKuisioner).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.clickMenu(2);
            }
        });

        view.findViewById(R.id.nav_statusPerwalian).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.clickMenu(3);
            }
        });
    }

}
