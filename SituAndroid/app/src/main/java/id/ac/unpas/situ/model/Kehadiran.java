package id.ac.unpas.situ.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Sidiq on 3/14/2016.
 */
public class Kehadiran implements Serializable {
    private Date tanggal;
    private String status;
    private String mulai;
    private String selesai;



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMulai() {
        return mulai;
    }

    public void setMulai(String mulai) {
        this.mulai = mulai;
    }

    public String getSelesai() {
        return selesai;
    }

    public void setSelesai(String selesai) {
        this.selesai = selesai;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }
}
