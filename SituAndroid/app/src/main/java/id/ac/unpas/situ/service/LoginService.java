package id.ac.unpas.situ.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;

import id.ac.unpas.situ.model.Profile;
import id.ac.unpas.situ.model.Semester;
import id.ac.unpas.situ.utils.Settings;

/**
 * Created by Sidiq on 3/3/2016.
 */
public class LoginService {
    public static String endPoint = "login";
    private static final String KEY_Npm = "npm";
    private static final String KEY_Nama = "nama";
    private static final String KEY_UserType = "type_user";
    private static final String KEY_FotoUrl = "url_foto";

    private static final String KEY_Semester = "list_semester";
    private static final String KEY_Semester_id = "id";
    private static final String KEY_Semester_nama = "nama_semester";
    private static final String KEY_Semester_isActive = "status";
    private static final String KEY_NamaOrtu = "nama_ortu";

    public static Profile parserJsonResult(String jsonStr){
        JSONObject j;
        Profile profile = new Profile();
        try {
            if (jsonStr == null)
                return null;
            j = new JSONObject(jsonStr);

            profile.setNama(j.getString(KEY_Nama));
            profile.setNpm(j.getString(KEY_Npm));
            profile.setUserType(j.getString(KEY_UserType));
            if (!j.isNull(KEY_FotoUrl)) {
                profile.setFotoUrl(j.getString(KEY_FotoUrl));
            }

            if (!j.isNull(KEY_NamaOrtu)) {
                profile.setNamaOrtu(j.getString(KEY_NamaOrtu));
            }

            // SET TOKEN DI SETTINGS
            Settings.token = j.getString(Settings.KEY_TOKEN);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        JSONArray jsonarray = null;
        try {
            // SET SEMESTER YG DIIKUTIN
            jsonarray = j.getJSONArray(KEY_Semester);
            if (jsonarray != null) {
                for (int i = 0; i < jsonarray.length(); i++) {
                    JSONObject o = jsonarray.getJSONObject(i);
                    Semester s = new Semester();
                    s.setId(o.getString(KEY_Semester_id));
                    s.setNama(o.getString(KEY_Semester_nama));
                    s.setIsActive(Integer.parseInt(o.getString(KEY_Semester_isActive)));
                    profile.getSemesterList().add(s);
                }
                Collections.sort(profile.getSemesterList(), Semester.IdComparator);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return profile;
    }
}
