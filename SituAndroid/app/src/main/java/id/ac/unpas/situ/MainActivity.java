package id.ac.unpas.situ;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import id.ac.unpas.situ.helper.ThemeManager;
import id.ac.unpas.situ.scheduler.AlarmReceiver;
import id.ac.unpas.situ.fragment.HasilKuisionerFragment;
import id.ac.unpas.situ.fragment.HomeFragment;
import id.ac.unpas.situ.fragment.JadwalKuliahFragment;
import id.ac.unpas.situ.fragment.JadwalMengajarFragment;
import id.ac.unpas.situ.fragment.JadwalUjianFragment;
import id.ac.unpas.situ.fragment.NilaiFragment;
import id.ac.unpas.situ.fragment.StatusPerwalianFragment;
import id.ac.unpas.situ.fragment.UnderConstructionFragment;
import id.ac.unpas.situ.model.Profile;
import id.ac.unpas.situ.utils.DownloadImageTask;
import id.ac.unpas.situ.utils.Settings;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        JadwalKuliahFragment.OnFragmentInteractionListener,
        JadwalMengajarFragment.OnFragmentInteractionListener,
        UnderConstructionFragment.OnFragmentInteractionListener,
        JadwalUjianFragment.OnFragmentInteractionListener,
        NilaiFragment.OnFragmentInteractionListener,
        HasilKuisionerFragment.OnFragmentInteractionListener,
        StatusPerwalianFragment.OnFragmentInteractionListener,
        HomeFragment.OnFragmentInteractionListener
{

    private ProgressDialog progress;
    private PendingIntent pendingIntent;
    Fragment currentFragment = null;

    MenuItem refreshAction ;

    private NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (MainData.getInstance().getFakultas() == null){
            doLogout();
            return;
        }
        ThemeManager.setCurrentThemeNoActionBar(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    public void clickMenu(int index){
        MenuItem startMenu = navigationView.getMenu().getItem(index);
        onNavigationItemSelected(startMenu);
        startMenu.setChecked(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (Settings.currentMenu != R.id.nav_homeMahasiswa && Settings.currentMenu != R.id.nav_homeDosen ){
                clickMenu(0);
            }else {
                backButtonHandler();
            }
        }


    }

    public void backButtonHandler() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Keluar Dari Aplikasi");
        // Setting Dialog Message
        alertDialog.setMessage("Apakah anda yakin ingin keluar?");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.dialog_icon);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YA",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("BATAL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        dialog.cancel();
                    }
                });
        // Showing Alert Message
        alertDialog.show();
    }

    public void initView(){
        MainData.getInstance().load(this);
        if (Settings.currentMenu == 0){

            Profile profile = MainData.getInstance().getProfile();
            if (profile != null) {
                if (MainData.getInstance().getProfile().isDosen()) {
                    Settings.currentMenu = R.id.nav_homeDosen;
                } else {
                    Settings.currentMenu = R.id.nav_homeMahasiswa;
                }
            }else {
                doLogout();
                return;
            }
        }

        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);

        LinearLayout header = (LinearLayout) headerLayout.findViewById(R.id.header);
        header.setBackgroundColor(ThemeManager.getColorPrimary(this));

        TextView txtName = (TextView) headerLayout.findViewById(R.id.txtName);
        TextView txtNrp = (TextView) headerLayout.findViewById(R.id.txtNrp);
        TextView txtNamaOrtu = (TextView) headerLayout.findViewById(R.id.txtNamaOrtu);
        TextView txtTypeUser = (TextView) headerLayout.findViewById(R.id.txtTypeUser);




        Profile profile = MainData.getInstance().getProfile();
        if (profile != null) {
            if (profile.isDosen()) {
                navigationView.getMenu().removeGroup(R.id.menu_mahasiswaTop);
                navigationView.getMenu().removeGroup(R.id.menu_mahasiswaBottom);
            }else {
                navigationView.getMenu().removeGroup(R.id.menu_DosenTop);
                navigationView.getMenu().removeGroup(R.id.menu_DosenBottom);
            }
            txtName.setText(profile.getNama());
            txtNrp.setText(profile.getNpm());

            txtNamaOrtu.setText(profile.getNamaOrtu());
            txtTypeUser.setText(profile.getTypeUserToString());

            if (profile == null){
                doLogout();
                return;
            }
            if (profile.getFotoUrl() != null &&  !profile.getFotoUrl().isEmpty())
                new DownloadImageTask((ImageView) headerLayout.findViewById(R.id.fotoUser)).execute(profile.getFotoUrl());
        }

        // set home screen
        MenuItem startMenu = navigationView.getMenu().findItem(Settings.currentMenu);
        onNavigationItemSelected(startMenu);
        startMenu.setChecked(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        refreshAction =  menu.findItem(R.id.action_refresh);

        if (ThemeManager.isTextDark())
            refreshAction.setIcon(R.drawable.ic_refresh_black);

        initView();

        return true;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            switch (Settings.currentMenu) {

                case R.id.nav_homeMahasiswa:
                case R.id.nav_homeDosen:
                    break;
                case R.id.nav_jadwalKuliah:
                    ((JadwalKuliahFragment) currentFragment).requestToServer();
                    break;
                case R.id.nav_jadwalUjian:
                    ((JadwalUjianFragment) currentFragment).requestToServer();
                    break;
                case R.id.nav_nilai:
                    ((NilaiFragment) currentFragment).requestToServer();
                    break;

                case R.id.nav_jadwalMengajar:
                    ((JadwalMengajarFragment) currentFragment).requestToServer();
                    break;
                case R.id.nav_hasilKuisioner:
                    ((HasilKuisionerFragment) currentFragment).requestToServer();
                    break;
                case R.id.nav_statusPerwalian:
                    ((StatusPerwalianFragment) currentFragment).requestToServer();
                    break;
            }
        }
        return super.onOptionsItemSelected(item);
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        FragmentManager fragmentManager = getFragmentManager();

            switch (id) {
                //////////// MENU MAHASISWA
                case R.id.nav_homeMahasiswa:
                    currentFragment = new HomeFragment();
                    setTitleByTheme("Beranda");
                    refreshAction.setVisible(false);
                    break;
                case R.id.nav_jadwalKuliah:
                    currentFragment = new JadwalKuliahFragment();
                    setTitleByTheme("Jadwal Kuliah");
                    refreshAction.setVisible(true);
                    break;
                case R.id.nav_jadwalUjian:
                    setTitleByTheme("Jadwal Ujian");
                    currentFragment = new JadwalUjianFragment();
                    refreshAction.setVisible(true);
                    break;
                case R.id.nav_nilai:
                    setTitleByTheme("Nilai");
                    currentFragment = new NilaiFragment();
                    refreshAction.setVisible(true);
                    break;
                case R.id.nav_kalenderAkademik:
                    setTitleByTheme("Kalender Akademik");
                    currentFragment = new UnderConstructionFragment();
                    break;
                case R.id.nav_jadwalPerkuliahan:
                    setTitleByTheme("Jadwal Perkuliahan");
                    currentFragment = new UnderConstructionFragment();
                    break;

                //////////// MENU DOSEN
                case R.id.nav_homeDosen:
                    setTitleByTheme("Beranda");
                    currentFragment = new HomeFragment();
                    refreshAction.setVisible(false);
                    break;
                case R.id.nav_jadwalMengajar:
                    currentFragment = new JadwalMengajarFragment();
                    setTitleByTheme("Jadwal Mengajar");
                    refreshAction.setVisible(true);
                    break;
                case R.id.nav_hasilKuisioner:
                    setTitleByTheme("Hasil Kuesioner");
                    currentFragment = new HasilKuisionerFragment();
                    refreshAction.setVisible(true);
                    break;
                case R.id.nav_statusPerwalian:
                    setTitleByTheme("Status Perwalian");
                    currentFragment = new StatusPerwalianFragment();
                    refreshAction.setVisible(true);
                    break;
                case R.id.nav_presensiPerkuliahan:
                    setTitleByTheme("Jadwal Perkuliahan");
                    currentFragment = new UnderConstructionFragment();
                    break;
                case R.id.nav_perwalianDosen:
                    setTitleByTheme("Perwalian Dosen");
                    currentFragment = new UnderConstructionFragment();
                    break;
                case R.id.nav_profilAnakWali:
                    setTitleByTheme("Profil Anak Wali");
                    currentFragment = new UnderConstructionFragment();
                    break;
                case R.id.nav_jadwalKuliahMahasiswa:
                    setTitleByTheme("Jadwal Kuliah Mahasiswa");
                    currentFragment = new UnderConstructionFragment();
                    break;

                /////////// LOGOUT
                case R.id.nav_logout:
                    logOutClick();
                    return true;
            }
            if (currentFragment != null){
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, currentFragment)
                        .commit();
            }


        Settings.currentMenu = id;
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setTitleByTheme(String title){
        if (ThemeManager.isTextDark()){
            setTitle(Html.fromHtml("<font color='#000000'>" + title + "</font>"));
        }else {
            setTitle(title);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }



    public void showLoading(){
        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Sedang meload data...");
        progress.show();
    }

    public void hideLoading(){
        progress.dismiss();
    }


    private void logOutClick(){
        new AlertDialog.Builder(this)
                .setTitle("Logout")
                .setMessage("Apakah anda yakin?")
                .setCancelable(true)
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        doLogout();
                    }
                })
                .show();

    }

    public void showAlertToken(){
        new AlertDialog.Builder(this)
                .setTitle("Session Kadaluarsa")
                .setMessage("Silahkan lakukan login kembali")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        doLogout();
                    }
                })
                .show();

    }


    public void doLogout(){
        Settings.clearConnected(this);

        // Clear 3 Alarm
        AlarmReceiver.cancelJadwalKuliah(this);
        AlarmReceiver.cancelJadwalUjian(this);
        AlarmReceiver.cancelJadwalMengajar(this);

        // Clear file
        MainData.newInstance();
        MainData.getInstance().save(this);

        Intent intent = new Intent(this, PilihFakultasActivity.class);
        startActivity(intent);
        finish();
    }

    public void showLoadSuccessToast(){
        Toast.makeText(this, "Data berhasil diperbaharui", Toast.LENGTH_SHORT).show();
    }
}
