package id.ac.unpas.situ.model;

import java.io.Serializable;

/**
 * Created by Sidiq on 3/23/2016.
 */
public class Pertanyaan implements Serializable {
    private String pertanyaan;
    private String nilai;

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }
}
