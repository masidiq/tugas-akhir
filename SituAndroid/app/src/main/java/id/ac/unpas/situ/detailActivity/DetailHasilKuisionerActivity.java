package id.ac.unpas.situ.detailActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;

import java.text.Format;
import java.text.SimpleDateFormat;

import id.ac.unpas.situ.MainData;
import id.ac.unpas.situ.R;
import id.ac.unpas.situ.helper.ThemeManager;
import id.ac.unpas.situ.model.HasilKuisioner;
import id.ac.unpas.situ.model.JadwalKuliah;
import id.ac.unpas.situ.model.Kehadiran;
import id.ac.unpas.situ.model.Pertanyaan;

public class DetailHasilKuisionerActivity extends AppCompatActivity {
    public final static String PARAMS_POSITION = "item_position";
    private HasilKuisioner hasilKuisioner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ThemeManager.setCurrentTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_hasil_kuisioner);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent myIntent = getIntent();
        int position = myIntent.getIntExtra(PARAMS_POSITION, 0);
        hasilKuisioner = MainData.getInstance().getHasilKuisionerList().get(position);

        String title = "Detail Hasil Kuisioner";
        if (ThemeManager.isTextDark()){
            setTitle(Html.fromHtml("<font color='#000000'>" + title + "</font>"));
        }else {
            setTitle(title);
        }

        setUiValue();
        generateTableDetail();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void generateTableDetail(){
        // Generate Table kehadiran
        LayoutInflater inflater = LayoutInflater.from(this);
        TableLayout tl = (TableLayout) findViewById(R.id.table);
        int i = 1;
        for (Pertanyaan pertanyaan : hasilKuisioner.getListPertanyaan()){
            View tr = inflater.inflate(R.layout.row_table_pertanyaan,null, false);
            ((TextView)tr.findViewById(R.id.txtNo)).setText(Integer.toString(i));
            ((TextView)tr.findViewById(R.id.txtPertanyaan)).setText(pertanyaan.getPertanyaan());
            ((TextView)tr.findViewById(R.id.txtNilai)).setText(pertanyaan.getNilai());
            if (i % 2 != 0)
                tr.setBackgroundColor(getResources().getColor(R.color.odd_row));
            else
                tr.setBackgroundColor(getResources().getColor(R.color.even_row));
            tl.addView(tr);
            i++;
        }
    }

    private void setUiValue(){
        ((TextView)findViewById(R.id.txtMataKuliah)).setText(hasilKuisioner.getMataKuliah());
        ((TextView)findViewById(R.id.txtSks)).setText(hasilKuisioner.getSks() + " sks");
        ((TextView)findViewById(R.id.txtSemester)).setText("Semester " + hasilKuisioner.getIdSemester());
        ((TextView)findViewById(R.id.txtKelas)).setText("Kelas " + hasilKuisioner.getKelas());
        ((TextView)findViewById(R.id.txtPeserta)).setText("Peserta : " + hasilKuisioner.getPeserta());
        ((TextView)findViewById(R.id.txtResponden)).setText("Responden : " + hasilKuisioner.getResponden());
        ((TextView)findViewById(R.id.txtNilai)).setText("Nilai IKD : " + hasilKuisioner.getNilai());
        ((TextView)findViewById(R.id.txtRataRata)).setText("Rata-rata : " + hasilKuisioner.getRataRata());
    }
}
