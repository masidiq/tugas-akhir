package id.ac.unpas.situ;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.net.HttpURLConnection;

import id.ac.unpas.situ.helper.ThemeManager;
import id.ac.unpas.situ.model.Profile;
import id.ac.unpas.situ.service.LoginService;
import id.ac.unpas.situ.utils.AlertHelper;
import id.ac.unpas.situ.utils.LoginRequestTask;
import id.ac.unpas.situ.utils.OnTaskListener;
import id.ac.unpas.situ.utils.RequestTask;
import id.ac.unpas.situ.utils.ResponseData;
import id.ac.unpas.situ.utils.Settings;


public class LoginActivity extends AppCompatActivity implements OnTaskListener {
    // UI references.

    private EditText edtUsername, edtPassword;
    private View mLoginFormView;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ThemeManager.setCurrentThemeNoActionBar(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        // Set up the login form.
        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtPassword.setTypeface(Typeface.DEFAULT);
        edtPassword.setTransformationMethod(new PasswordTransformationMethod());
        LinearLayout bg = (LinearLayout)findViewById(R.id.login_form);
        bg.setBackgroundColor(ThemeManager.getColorPrimary(this));

        edtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });


        Button btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
                //save();
            }
        });
        
        mLoginFormView = findViewById(R.id.login_form);

        TextView mLink = (TextView) findViewById(R.id.linkSitu);
        if (mLink != null) {
            mLink.setMovementMethod(LinkMovementMethod.getInstance());
        }

        TextView txtFakultas = (TextView)findViewById(R.id.txtFakultas);
        txtFakultas.setText(ThemeManager.getNamaFakultas());

        if (ThemeManager.isTextDark()){
            ImageView imgView = (ImageView) findViewById(R.id.banner);
            imgView.setImageResource(R.drawable.situ_banner_ekonomi);
            mLink.setLinkTextColor(R.color.black);
        }
    }

    private void attemptLogin() {

        // Reset errors.
        edtUsername.setError(null);
        edtPassword.setError(null);

        // Store values at the time of the login attempt.
        String username = edtUsername.getText().toString();
        String password = edtPassword.getText().toString();

        if (TextUtils.isEmpty(username)) {
            edtUsername.setError(getString(R.string.error_nrp_required));
            edtUsername.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            edtPassword.setError(getString(R.string.error_password_required));
            edtPassword.requestFocus();
            return;
        }


        LoginRequestTask task = new LoginRequestTask(this);
        task.execute(LoginService.endPoint, username, password);
    }

    public void showLoading(){
        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Harap tunggu sebentar...");
        progress.show();
    }

    public void hideLoading(){
        progress.hide();
    }

    @Override
    public void onTaskCompleted(ResponseData response) {
        hideLoading();
        Boolean isFail;
        String pesanError = "" ;
        if (response.getStatus() == HttpURLConnection.HTTP_OK){
            onLoginSuccess(response.getResponseString());
            return;
        }else if (response.getStatus() == HttpURLConnection.HTTP_UNAUTHORIZED){
            isFail = true;
            pesanError = response.parseErrorMessage();
        }else if (response.getStatus() == HttpURLConnection.HTTP_BAD_REQUEST){
            isFail = true;
            if (response.getResponseString().toLowerCase().contains("database") ||
                    response.getResponseString().toLowerCase().contains("denied for user")) {
                pesanError = "Silahkan hubungi SPTIK UNPAS";
                AlertHelper.ShowAlert(this, "Fakultas Belum Diaktivasi", pesanError);
                return;
            }
            else
                pesanError = response.getResponseString();
        }else {
            isFail = true;
            pesanError = response.getResponseString();
        }

        if (isFail) {
            AlertHelper.ShowAlert(this, "Login Gagal", pesanError);
            edtPassword.requestFocus();
        }
    }

    private void onLoginSuccess(String result){
        Profile profile = LoginService.parserJsonResult(result);
        if (profile == null){
            AlertHelper.ShowAlert(this, "Kesalahan pada sistem",result);
            return;
        }

        MainData.getInstance().setProfile(profile);
        MainData.getInstance().save(this);
        Settings.setConnected(this);
        goToMainActivity();
    }

    private void goToMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onTaskStarted() {
        showLoading();
    }
}

