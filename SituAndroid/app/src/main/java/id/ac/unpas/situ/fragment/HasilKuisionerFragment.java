package id.ac.unpas.situ.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.ac.unpas.situ.MainActivity;
import id.ac.unpas.situ.MainData;
import id.ac.unpas.situ.R;
import id.ac.unpas.situ.customAdapter.HasilKuisionerAdapter;
import id.ac.unpas.situ.customAdapter.NilaiAdapter;
import id.ac.unpas.situ.detailActivity.DetailHasilKuisionerActivity;
import id.ac.unpas.situ.detailActivity.DetailJadwalKuliahActivity;
import id.ac.unpas.situ.helper.EnumHari;
import id.ac.unpas.situ.model.HasilKuisioner;
import id.ac.unpas.situ.model.Nilai;
import id.ac.unpas.situ.service.HasilKuisionerService;
import id.ac.unpas.situ.service.NilaiService;
import id.ac.unpas.situ.utils.AlertHelper;
import id.ac.unpas.situ.utils.OnTaskListener;
import id.ac.unpas.situ.utils.RequestTask;
import id.ac.unpas.situ.utils.ResponseData;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HasilKuisionerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HasilKuisionerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HasilKuisionerFragment extends Fragment implements OnTaskListener {
    private OnFragmentInteractionListener mListener;
    private MainActivity activity;
    private ListView mList;
    private HasilKuisionerAdapter mAdapter;
    private List<HasilKuisioner> mainList;
    private TextView txtLastUpdate;


    public static HasilKuisionerFragment newInstance(String param1, String param2) {
        HasilKuisionerFragment fragment = new HasilKuisionerFragment();
        return fragment;
    }

    public HasilKuisionerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hasil_kuisioner, container, false);
        initUI(view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.activity = (MainActivity)activity;
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onTaskCompleted(ResponseData response) {
        this.activity.hideLoading();

        if (response.getStatus() == HttpURLConnection.HTTP_OK){
            mainList = HasilKuisionerService.parserJsonResult(response.getResponseString());
            if (mainList != null && mainList.size() > 0){
                MainData.getInstance().setHasilKuisionerLastUpdate(new Date());
                MainData.getInstance().setHasilKuisionerList(mainList);
                MainData.getInstance().save(this.activity);

                refreshListView();
                activity.showLoadSuccessToast();
            }
            return;
        }else if (response.getStatus() == HttpURLConnection.HTTP_UNAUTHORIZED){
            activity.showAlertToken();
        }else {
            AlertHelper.ShowAlert(activity, activity.getString(R.string.load_failed), response.getResponseString());
        }
    }

    @Override
    public void onTaskStarted() {
        this.activity.showLoading();
    }


    private void initUI(View view){
        //txtResponse = (TextView) view.findViewById(R.id.txtResponse);
        mainList = new ArrayList<>();
        mList = (ListView) view.findViewById(R.id.list);
        txtLastUpdate = (TextView)view.findViewById(R.id.txtLastUpdate);
        setValueToUI();

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(activity, DetailHasilKuisionerActivity.class);
                intent.putExtra(DetailHasilKuisionerActivity.PARAMS_POSITION, position);
                startActivity(intent);
            }

        });
    }

    public void requestToServer(){
        RequestTask task = new RequestTask(this);
        task.execute(HasilKuisionerService.endPoint);
    }

    public void setValueToUI(){
        if (MainData.getInstance().getHasilKuisionerList() == null){
            requestToServer();
            return;
        }
        mainList = MainData.getInstance().getHasilKuisionerList();
        if(!mainList.isEmpty()) {
            refreshListView();

        }
    }

    private void refreshListView(){
        mAdapter = new HasilKuisionerAdapter(getActivity(), mainList);
        mList.setAdapter(mAdapter);
        txtLastUpdate.setText(EnumHari.generateLastUpdate(MainData.getInstance().getHasilKuisionerLastUpdate()));
    }

}
