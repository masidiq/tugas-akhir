package id.ac.unpas.situ.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.ac.unpas.situ.MainActivity;
import id.ac.unpas.situ.MainData;
import id.ac.unpas.situ.R;
import id.ac.unpas.situ.customAdapter.JadwalMengajarAdapter;
import id.ac.unpas.situ.detailActivity.DetailJadwalKuliahActivity;
import id.ac.unpas.situ.detailActivity.DetailJadwalMengajarActivity;
import id.ac.unpas.situ.helper.EnumHari;
import id.ac.unpas.situ.model.JadwalMengajar;
import id.ac.unpas.situ.scheduler.AlarmReceiver;
import id.ac.unpas.situ.service.JadwalMengajarService;
import id.ac.unpas.situ.utils.AlertHelper;
import id.ac.unpas.situ.utils.OnTaskListener;
import id.ac.unpas.situ.utils.RequestTask;
import id.ac.unpas.situ.utils.ResponseData;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class JadwalMengajarFragment extends Fragment  implements OnTaskListener {
    private MainActivity activity;
    private StickyListHeadersListView mList;
    private JadwalMengajarAdapter mAdapter;
    List<JadwalMengajar> jdwl;
    private TextView txtLastUpdate;

    private OnFragmentInteractionListener mListener;

    public static JadwalMengajarFragment newInstance(String param1, String param2) {
        JadwalMengajarFragment fragment = new JadwalMengajarFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    public JadwalMengajarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jadwal_mengajar, container, false);
        // Init
        initUI(view);
        return view;
    }

    private void initUI(View view){
        txtLastUpdate = (TextView)view.findViewById(R.id.txtLastUpdate);
        jdwl = new ArrayList<>();
        mList = (StickyListHeadersListView) view.findViewById(R.id.list);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent intent = new Intent(activity, DetailJadwalMengajarActivity.class);
                intent.putExtra(DetailJadwalMengajarActivity.PARAMS_POSITION, position);
                startActivity(intent);
            }

        });
        setValueToUI();
    }
    public void requestToServer(){
        RequestTask task = new RequestTask(this);
        task.execute(JadwalMengajarService.endPoint);
    }

    public void setValueToUI(){
        if (MainData.getInstance().getJadwalMengajarList() == null){
            requestToServer();
            return;
        }
        jdwl = MainData.getInstance().getJadwalMengajarList();
        if(jdwl != null) {
            refreshListView();

        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.activity = (MainActivity)activity;
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onTaskCompleted(ResponseData response) {
        this.activity.hideLoading();

        if (response.getStatus() == HttpURLConnection.HTTP_OK){
            jdwl = JadwalMengajarService.parserJsonResult(response.getResponseString());
            if (jdwl != null && jdwl.size() > 0){
                //delete alarm
                AlarmReceiver.cancelJadwalMengajar(activity);

                MainData.getInstance().setJadwalMengajarLastUpdate(new Date());
                MainData.getInstance().setJadwalMengajarList(jdwl);
                MainData.getInstance().save(this.activity);
                AlarmReceiver.setScheduleJadwalMengajar(activity);

                refreshListView();
                activity.showLoadSuccessToast();
            }
            return;
        }else if (response.getStatus() == HttpURLConnection.HTTP_UNAUTHORIZED){
            activity.showAlertToken();
        }else {
            AlertHelper.ShowAlert(activity, activity.getString(R.string.load_failed), response.getResponseString());
        }
    }

    @Override
    public void onTaskStarted() {
        this.activity.showLoading();
    }

    private void refreshListView(){
        mAdapter = new JadwalMengajarAdapter(getActivity(), jdwl);
        mList.setAdapter(mAdapter);
        txtLastUpdate.setText(EnumHari.generateLastUpdate(MainData.getInstance().getJadwalMengajarLastUpdate()));
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
