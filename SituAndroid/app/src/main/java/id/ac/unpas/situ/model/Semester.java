package id.ac.unpas.situ.model;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by Sidiq on 3/13/2016.
 */
public class Semester implements Serializable {
    private String id;
    private String nama;
    private Boolean isActive;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        if (isActive == 1){
            this.isActive = true;
        }else {
            this.isActive = false;
        }
    }

    public static Comparator<Semester> IdComparator = new Comparator<Semester>() {
        public int compare(Semester s1, Semester s2) {
            String str1 = s1.getId().toUpperCase();
            String str2 = s2.getId().toUpperCase();

            //descending order
            return str2.compareTo(str1);
        }
    };
}
