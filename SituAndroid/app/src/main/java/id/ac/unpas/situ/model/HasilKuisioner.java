package id.ac.unpas.situ.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sidiq on 3/19/2016.
 */
public class HasilKuisioner implements Serializable{
    private String mataKuliah;
    private String kelas;
    private String prodi;
    private String idSemester;
    private String sks;
    private String peserta;
    private String responden;
    private String nilai;
    private String rataRata;
    private List<Pertanyaan> listPertanyaan;

    public HasilKuisioner() {
        listPertanyaan = new ArrayList<>();
    }

    public String getMataKuliah() {
        return mataKuliah;
    }

    public void setMataKuliah(String mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getProdi() {
        return prodi;
    }

    public void setProdi(String prodi) {
        this.prodi = prodi;
    }

    public String getIdSemester() {
        return idSemester;
    }

    public void setIdSemester(String idSemester) {
        this.idSemester = idSemester;
    }

    public String getSks() {
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }

    public String getPeserta() {
        return peserta;
    }

    public void setPeserta(String peserta) {
        this.peserta = peserta;
    }

    public String getResponden() {
        return responden;
    }

    public void setResponden(String responden) {
        this.responden = responden;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    public String getRataRata() {
        return rataRata;
    }

    public void setRataRata(String rataRata) {
        this.rataRata = rataRata;
    }

    public List<Pertanyaan> getListPertanyaan() {
        return listPertanyaan;
    }

    public void setListPertanyaan(List<Pertanyaan> listPertanyaan) {
        this.listPertanyaan = listPertanyaan;
    }
}
