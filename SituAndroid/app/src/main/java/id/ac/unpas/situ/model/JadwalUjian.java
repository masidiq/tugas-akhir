package id.ac.unpas.situ.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import id.ac.unpas.situ.helper.EnumHari;

/**
 * Created by Sidiq on 2/28/2016.
 */
public class JadwalUjian implements Serializable{
    private String mataKuliah;
    private Date tanggal;
    private String hari;
    private long idWaktu;
    private String jam;
    private String ruang;
    private String kodeMk;
    private String kelas;
    private String sks;
    private String dosen;
    private String presensi;

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public long getIdWaktu() {
        return idWaktu;
    }

    public void setIdWaktu(long idWaktu) {
        this.idWaktu = idWaktu;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getRuang() {
        return ruang;
    }

    public void setRuang(String ruang) {
        this.ruang = ruang;
    }

    public String getKodeMk() {
        return kodeMk;
    }

    public void setKodeMk(String kodeMk) {
        this.kodeMk = kodeMk;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getSks() {
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }

    public String getDosen() {
        return dosen;
    }

    public void setDosen(String dosen) {
        this.dosen = dosen;
    }

    public String getPresensi() {
        return presensi;
    }

    public void setPresensi(String presensi) {
        this.presensi = presensi;
    }

    public String getMataKuliah() {
        return mataKuliah;
    }

    public void setMataKuliah(String mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    public static Comparator<JadwalUjian> JamAsc = new Comparator<JadwalUjian>() {
        public int compare(JadwalUjian s1, JadwalUjian s2) {
            String str1 = Long.toString(s1.getIdWaktu()) + s1.getJam().toUpperCase();
            String str2 = Long.toString(s2.getIdWaktu()) + s2.getJam().toUpperCase();
            return str1.compareTo(str2);
        }
    };

    public void generateIdWaktu(){
       idWaktu = tanggal.getTime();
        Calendar c = Calendar.getInstance();
        c.setTime(tanggal);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        hari = EnumHari.getHari(dayOfWeek);
    }
}
